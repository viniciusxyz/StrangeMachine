#include "smpch.h"

#include "core/data/smHashMap.h"
#include "core/smCore.h"

#include "resource/file_format/smSMR.h"
#include "resource/smResource.h"
#include "resource/smShaderResource.h"

#undef SM_MODULE_NAME
#define SM_MODULE_NAME "RESOURCE MANAGER"

#define DMON_IMPL

#define DMON_MALLOC  SM_MALLOC
#define DMON_FREE    SM_FREE
#define DMON_REALLOC SM_REALLOC

#define DMON_ASSERT SM_ASSERT

#define DMON_LOG_ERROR   SM_LOG_ERROR
#define DMON_LOG_DEBUG   SM_LOG_TRACE
#define _DMON_LOG_ERRORF SM_LOG_ERROR
#define _DMON_LOG_DEBUGF SM_LOG_TRACE
#include "vendor/dmon/dmon.h"

typedef struct {

  SM_STRING root_folder;
  SM_STRING material_path;
  SM_STRING scene_path;

  sm_hashmap_str_m *map;

} resource_manager_s;

sm_resource_manager_type_e sm__resource_manager_get_file_type(const char *file);

/* globals */

resource_manager_s *RESOURCE_MANAGER = NULL;

char *expected_ext = "jpeg;jpg;png;mtl;mp3;ogg;shader;smscene;smmaterial"; /* supported resource types */

/* private functions */
sm_resource_manager_type_e sm__resource_manager_get_file_type(const char *file);
void sm__resource_manager_on_found(SM_STRING path);
void sm__resource_manager_on_delete(SM_STRING path);
void sm__resource_manager_on_reload(SM_STRING path);
void sm__resource_manager_watch_cb(dmon_watch_id watch_id, dmon_action action, const char *rootdir, const char *filepath,
                                   const char *oldfilepath, void *user);
void sm__resource_manager_dir_read(const char *folder);

bool sm_resource_manager_init(const char *root_folder)
{

  SM_ASSERT(RESOURCE_MANAGER == NULL && "resource already initialized");
  SM_ASSERT(root_folder);

  srand((u32)time(NULL));

  RESOURCE_MANAGER = SM_CALLOC(1, sizeof(resource_manager_s));
  SM_ASSERT(RESOURCE_MANAGER);

  RESOURCE_MANAGER->root_folder = SM_STRING_DUP(root_folder);

  size_t len = strlen(RESOURCE_MANAGER->root_folder);
  RESOURCE_MANAGER->material_path = SM_MALLOC(len + strlen("materials/") + 1);
  strcpy(RESOURCE_MANAGER->material_path, RESOURCE_MANAGER->root_folder);
  strcat(RESOURCE_MANAGER->material_path, "materials/");

  RESOURCE_MANAGER->scene_path = SM_MALLOC(len + strlen("scenes/") + 1);
  strcpy(RESOURCE_MANAGER->scene_path, RESOURCE_MANAGER->root_folder);
  strcat(RESOURCE_MANAGER->scene_path, "scenes/");

  RESOURCE_MANAGER->map = sm_hashmap_new_str( );
  if (!sm_hashmap_ctor_str(RESOURCE_MANAGER->map, 16, NULL, NULL)) {
    SM_LOG_ERROR("failed to create hashmap");
    return false;
  }

  /* Reads the folder and loads the resources */
  /* into the resource manager */
  sm__resource_manager_dir_read(RESOURCE_MANAGER->root_folder);

  dmon_init( );
  dmon_watch(RESOURCE_MANAGER->root_folder, sm__resource_manager_watch_cb, DMON_WATCHFLAGS_RECURSIVE, NULL);

  /* sm_texture_manager_init(16); */
  sm_shader_resource_init( );

  return true;
}

b8 sm__reource_manager_dtor_cb(const SM_STRING key, void *value, void *user_data)
{

  SM_UNUSED(user_data);

  sm_resource_s *res = (sm_resource_s *)value;
  SM_STRING_FREE(res->filename);

  SM_FREE(res);
  SM_STRING_FREE((SM_STRING)key);

  return true;
}

void sm_resource_manager_teardown(void)
{

  SM_ASSERT(RESOURCE_MANAGER);

  sm_shader_resource_teardown( );
  /* sm_texture_manager_teardown( ); */

  dmon_deinit( );

  sm_hashmap_for_each_str(RESOURCE_MANAGER->map, sm__reource_manager_dtor_cb, NULL);
  sm_hashmap_dtor_str(RESOURCE_MANAGER->map);

  SM_STRING_FREE(RESOURCE_MANAGER->scene_path);
  SM_STRING_FREE(RESOURCE_MANAGER->material_path);
  SM_STRING_FREE(RESOURCE_MANAGER->root_folder);

  SM_FREE(RESOURCE_MANAGER);
}

sm_resource_s *sm_resource_manager_get(const SM_STRING key)
{

  SM_ASSERT(key);

  sm_resource_s *res = NULL;
  /* SM_STRING res_name; */

  /* if (!sm_string_contains(key, RESOURCE_MANAGER->root_folder)) { */
  /*   res_name = sm_string_append(RESOURCE_MANAGER->root_folder, key); */
  /* } else { */
  /*   res_name = sm_string_reference(key); */
  /* } */

  sm_hashmap_lock_str(RESOURCE_MANAGER->map);
  res = sm_hashmap_get_str(RESOURCE_MANAGER->map, key);
  sm_hashmap_unlock_str(RESOURCE_MANAGER->map);

  /* sm_string_dtor(res_name); */

  if (res == NULL) {
    SM_LOG_WARN("[%s] resource not found", key);
    return NULL;
  }

  return res;
}

struct sm_resource_manager_uuid_match_s {
  u32 in;
  sm_resource_s *out;
};

b8 sm__resource_manager_match_uuid_cb(const SM_STRING key, void *value, void *user_data)
{

  SM_UNUSED(key);
  struct sm_resource_manager_uuid_match_s *match = (struct sm_resource_manager_uuid_match_s *)user_data;
  sm_resource_s *resource = (sm_resource_s *)value;

  if (match->in == resource->uuid) {
    match->out = resource;
    return false;
  }

  return true;
}

sm_resource_s *sm_resource_manager_get_by_uuid(u32 uuid)
{

  if (!uuid) return NULL;

  struct sm_resource_manager_uuid_match_s match = {.in = uuid, .out = NULL};

  sm_hashmap_lock_str(RESOURCE_MANAGER->map);
  sm_hashmap_for_each_str(RESOURCE_MANAGER->map, sm__resource_manager_match_uuid_cb, &match);
  sm_hashmap_unlock_str(RESOURCE_MANAGER->map);

  if (match.out == NULL) {
    SM_LOG_WARN("[%d] resource not found by uuid", uuid);
    return NULL;
  }

  return match.out;
}

/* sm_resource_manager_iter_s sm_resource_manager_iter_new(sm_resource_manager_type_e type, sm_resource_manager_status_e status) { */
/**/
/*   sm_resource_manager_iter_s iter; */
/*   iter.type = type; */
/*   iter.status = status; */
/*   iter.index = 0; */
/**/
/*   return iter; */
/* } */

sm_resource_manager_type_e sm__resource_manager_get_file_type(const SM_STRING filename)
{

  SM_ASSERT(filename);

  if (sm_filesystem_has_ext(filename, "jpg;jpeg;png")) return SM_RESOURCE_TYPE_TEXTURE;
  if (sm_filesystem_has_ext(filename, "mp3;ogg")) return SM_RESOURCE_TYPE_AUDIO;
  if (sm_filesystem_has_ext(filename, "shader")) return SM_RESOURCE_TYPE_SHADER;
  if (sm_filesystem_has_ext(filename, "smscene")) return SM_RESOURCE_TYPE_SCENE;
  if (sm_filesystem_has_ext(filename, "smmaterial")) return SM_RESOURCE_TYPE_MATERIAL;

  return SM_RESOURCE_TYPE_INVALID;
}

void sm__resource_manager_load_data(const SM_STRING file_path, sm_resource_s *resource)
{

  switch (resource->type) {
  case SM_RESOURCE_TYPE_MATERIAL: {
    smr_file_header_s header = {0};
    sm_file_handle_s fhandle;
    if (!sm_filesystem_open(file_path, SM_FILE_MODE_READ, true, &fhandle)) {
      SM_LOG_ERROR("[%s] failed to open file", file_path);
      return;
    }
    if (!smr_read_header(&header, &fhandle)) {
      SM_LOG_ERROR("[%s] failed to read header", file_path);
      return;
    }

    resource->uuid = header.uuid;

    sm_filesystem_close(&fhandle);
    break;
  }

  default: return;
  }
}

void sm__resource_manager_on_found(SM_STRING file_path)
{

  SM_ASSERT(file_path);

  if (!sm_filesystem_exists(file_path)) {
    SM_LOG_WARN("[%s] resource does not exist", file_path);
    return;
  }

  SM_LOG_TRACE("[%s] resource found", file_path);

  sm_resource_manager_type_e ft = sm__resource_manager_get_file_type(file_path);
  if (ft == SM_RESOURCE_TYPE_INVALID) {
    SM_LOG_WARN("[%s] resource not supported", file_path);
    return;
  }

  sm_resource_s *resource = SM_MALLOC(sizeof(sm_resource_s));
  resource->type = ft;
  resource->status = SM_RESOURCE_STATUS_FOUND;
  resource->handle = SM_INVALID_HANDLE;
  resource->uuid = 0u;
  resource->filename = SM_STRING_DUP(basename(file_path));

  sm__resource_manager_load_data(file_path, resource);

  sm_resource_s *r = sm_hashmap_put_str(RESOURCE_MANAGER->map, SM_STRING_DUP(file_path), resource);
  if (r) {
    SM_FREE(r);
    SM_LOG_WARN("[%s] resource already exists. Replacing it", file_path);
  }
}

void sm__resource_manager_on_delete(SM_STRING path)
{

  SM_ASSERT(path);

  sm_resource_s *res = sm_hashmap_get_str(RESOURCE_MANAGER->map, path);
  if (!res) {
    SM_LOG_WARN("[%s] resource not found in the map (NOT_FOUND)", path);
    return;
  }

  SM_LOG_TRACE("[%s] resource removed", path);
  res->status = SM_RESOURCE_STATUS_INVALID;
  res->type = SM_RESOURCE_TYPE_INVALID;
}

void sm__resource_manager_on_reload(SM_STRING path)
{

  SM_ASSERT(path);

  sm_resource_s *res = sm_hashmap_get_str(RESOURCE_MANAGER->map, path);
  if (!res) {
    SM_LOG_WARN("[%s] on reload: resource not found in the map (NOT_FOUND)", path);
    return;
  }

  SM_LOG_TRACE("[%s] resource reloaded", path);
  res->status |= SM_RESOURCE_STATUS_RELOADED;
}

void sm__resource_manager_watch_cb(dmon_watch_id watch_id, dmon_action action, const char *rootdir, const char *filepath,
                                   const char *oldfilepath, void *user)
{

  SM_ASSERT(rootdir);
  SM_ASSERT(filepath);
  SM_ASSERT(action < 5);

  SM_UNUSED(user);
  SM_UNUSED(watch_id);
  SM_UNUSED(oldfilepath);

  /* receive change events. type of event is stored in 'action' variable */

  if (!sm_filesystem_has_ext(filepath, expected_ext)) {
    SM_LOG_WARN("[%s] resource not supported", filepath);
    return;
  }

  static void (*handler[5])(SM_STRING key) = {

      [DMON_ACTION_CREATE] = sm__resource_manager_on_found,
      [DMON_ACTION_DELETE] = sm__resource_manager_on_delete,
      [DMON_ACTION_MODIFY] = sm__resource_manager_on_reload,
      [DMON_ACTION_MOVE] = NULL,
  };

  SM_STRING key = SM_MALLOC(strlen(rootdir) + strlen(filepath) + 1);
  strcpy(key, rootdir);
  strcat(key, filepath);

  sm_hashmap_lock_str(RESOURCE_MANAGER->map);
  handler[action](key);
  sm_hashmap_unlock_str(RESOURCE_MANAGER->map);

  SM_STRING_FREE(key);
}

SM_STRING sm_resource_manager_get_path(const SM_STRING name, sm_resource_manager_type_e type)
{

  SM_STRING file_path = NULL;

  switch (type) {
  case SM_RESOURCE_TYPE_MATERIAL: {
    file_path = SM_STRING_MALLOC(strlen(name) + strlen("assets/materials/") + strlen(".smmaterial") + 1);
    strcpy(file_path, "assets/materials/");
    strcat(file_path, name);
    strcat(file_path, ".smmaterial");
    break;
  }
  default: SM_LOG_ERROR("invalid type %d", type); return file_path;
  }

  return file_path;
}

void sm__resource_manager_dir_read(const char *folder)
{

  DIR *dir = opendir(folder);
  if (!dir) {
    return;
  }

  size_t len = strlen(folder);

  char root[256];
  strcpy(root, folder);

  char *buf = root + len;

  struct dirent *ent;
  while ((ent = readdir(dir)) != NULL) {

    /* check if it is a regular file */
    if (SM_MASK_CHK(ent->d_type, DT_REG)) {

      /* check if it is a supported file type */
      if (sm_filesystem_has_ext(ent->d_name, expected_ext)) {

        /* create the full path */
        strcpy(buf, ent->d_name);

        sm__resource_manager_on_found(root);
      }

    } else if (SM_MASK_CHK(ent->d_type, DT_DIR)) {

      /* check if it is a directory */
      if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0) {
        continue;
      }

      /* create the full path */
      strcpy(buf, ent->d_name);
      strcat(buf, "/");

      /* recurse */
      sm__resource_manager_dir_read(root);
    }
  }

  closedir(dir);
}

#undef SM_MODULE_NAME
