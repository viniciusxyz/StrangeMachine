#include "smpch.h"

#include "core/smBase.h"
#include "core/smLog.h"

#include "core/smAssert.h"
#include "core/smMem.h"
#include "core/smStackLayer.h"
#include "core/smTime.h"
#include "core/smTimer.h"
#include "core/smWindow.h"
#include "core/util/smBitMask.h"

#include "renderer/smMaterialManager.h"
#include "renderer/smRenderer.h"
#include "renderer/smShaderManager.h"

#include "cimgui/smCimgui.h"

#include "resource/smResource.h"

#include "smInput.h"

#undef SM_MODULE_NAME
#define SM_MODULE_NAME "APPLICATION"

#ifdef SM_DEBUG
static void sm_at_exit(void)
{
  sm__mem_print( );
}
#endif

typedef struct sm__application_s {

  struct sm_window_s *window;
  struct sm_stack_layer_s *stack;

  sm_cimgui_s *cimgui;

  b8 is_running;
  b8 is_minimized;

  struct sm_timer_s *timer;

  struct {
    const f32 avg_interval_sec;
    u32 num_frames;
    f64 accumulated_time;
    f32 current_fps;
  } fps2;

  f32 thickness;
  f64 delta;
  f64 fps;

} sm_application_s;

/* private functions */
SM_PRIVATE
void sm__application_cap_frame_rate(long *then, f32 *remainder);

SM_PRIVATE
void sm__application_status_gui(sm_application_s *app);

sm_application_s *sm_application_new(void)
{

  static b8 instanced = false;
  SM_CORE_ASSERT(!instanced && "application already instanced");

  sm_application_s *app = SM_CALLOC(1, sizeof(sm_application_s));
  SM_CORE_ASSERT(app);

  instanced = true;

  return app;
}

b8 sm_application_on_event(sm_event_s *event, void *user_data);
b8 sm_application_on_window_close(sm_event_s *event, void *user_data);
b8 sm_application_on_window_resize(sm_event_s *event, void *user_data);
void sm_application_push_overlay(sm_application_s *app, struct sm_layer_s *layer);

b8 sm_application_ctor(sm_application_s *app, const char *name)
{

  SM_CORE_ASSERT(app);

#ifdef SM_DEBUG
  atexit(sm_at_exit);
#endif

  /* device_init(OPENGL21); */

  /* log_set_level(LOG_DEBUG); */

  app->window = window_new( );
  if (!sm_window_ctor(app->window, name, 800, 600)) {
    SM_CORE_LOG_ERROR("failed to initialize window");
    return false;
  }

  sm_window_set_callback(app->window, sm_application_on_event, app);

  input_init( );
  sm_resource_manager_init("assets/");

  app->cimgui = sm_cimgui_new( );
  if (!sm_cimgui_ctor(app->cimgui, app->window)) {
    SM_CORE_LOG_ERROR("failed to initialize cimgui");
    return false;
  }

  struct sm_stack_layer_s *stack = sm_stack_layer_new( );
  if (!sm_stack_layer_ctor(stack)) {
    SM_CORE_LOG_ERROR("failed to initialize stack layer");
    return false;
  }
  app->stack = stack;

  sm_application_push_overlay(app, app->cimgui);

  sm_event_category_e mask = SM_CATEGORY_WINDOW;
  sm_event_set_print_mask(mask);

  app->timer = sm_timer_new( );
  sm_timer_ctor(app->timer);

  sm_shader_manager_init( );

  sm_texture_manager_init( );

  sm_material_manager_init( );

  sm_renderer_init( );

  app->delta = 0.0;
  app->is_running = true;
  app->thickness = 1.0;

  return true;
}

void sm_application_dtor(sm_application_s *app)
{

  SM_CORE_ASSERT(app);

  sm_renderer_teardown( );

  sm_material_manager_teardown( );

  sm_texture_manager_teardown( );

  sm_shader_manager_teardown( );

  sm_timer_dtor(app->timer);

  sm_stack_layer_dtor(app->stack);

  sm_cimgui_dtor(app->cimgui);

  sm_resource_manager_teardown( );

  input_tear_down( );

  sm_window_dtor(app->window);

  SM_FREE(app);
}

b8 sm_application_on_event(sm_event_s *event, void *user_data)
{

  sm_event_print(event);

  SM_CORE_ASSERT(event);
  SM_CORE_ASSERT(user_data);

  sm_application_s *app = (sm_application_s *)user_data;

  sm_event_dispatch(event, SM_EVENT_WINDOW_CLOSE, sm_application_on_window_close, app);
  sm_event_dispatch(event, SM_EVENT_WINDOW_RESIZE, sm_application_on_window_resize, NULL);

  if (SM_MASK_CHK(event->category, SM_CATEGORY_KEYBOARD | SM_CATEGORY_MOUSE)) {

    /* Check if the event was handled by the imgui layer, otherwise pass it to the input system */
    event->handled |= sm_layer_event(app->cimgui, event);
    if (!event->handled) event->handled |= input_on_event(event, NULL);

    return event->handled;
  }

  size_t stack_size = sm_stack_layer_get_size(app->stack);
  for (size_t i = stack_size; i > 0; --i) {
    struct sm_layer_s *layer = sm_stack_layer_get_layer(app->stack, i - 1);
    if (event->handled) break;
    sm_layer_event(layer, event);
  }

  return event->handled;
}

void sm_application_do(sm_application_s *app)
{

  SM_CORE_ASSERT(app);

  long then = SM_GET_TICKS( );
  f32 remainder = 0;
  i32 f = 12;

  while (app->is_running) {

    sm_timer_reset(app->timer);

    input_do( );
    sm_window_do(app->window);

    size_t stack_size = sm_stack_layer_get_size(app->stack);
    for (size_t i = 0; i < stack_size; ++i) {
      struct sm_layer_s *layer = sm_stack_layer_get_layer(app->stack, i);
      sm_layer_update(layer, (f32)app->delta * app->thickness);
    }

    static b8 open = true;
    if (input_scan_key_lock(sm_key_f)) open = !open;

    sm_cimgui_begin(app->cimgui);

    stack_size = sm_stack_layer_get_size(app->stack);
    for (size_t i = 0; i < stack_size; ++i) {
      struct sm_layer_s *layer = sm_stack_layer_get_layer(app->stack, i);
      sm_layer_gui(layer);
    }

    if (open) sm__application_status_gui(app);

    /* igSliderf32("dt", &app->thickness, -1.0, 1.0, "%.3f", 0); */

    sm_cimgui_end(app->cimgui);

    sm_window_swap_buffers(app->window);

    sm__application_cap_frame_rate(&then, &remainder);

    app->delta = sm_timer_get_elapsed(app->timer);

    if (--f <= 0) {
      app->fps = (1.0 / app->delta);
      f = 12;
    }

    if (errno) {
      SM_CORE_LOG_ERROR("errno: %d", errno);
      errno = 0;
    }
  }

#ifdef SM_DEBUG

  SM_CORE_ASSERT(app->is_running == false);

#endif
}

b8 sm_application_on_window_close(sm_event_s *event, void *user_data)
{

  SM_CORE_ASSERT(event);
  SM_CORE_ASSERT(user_data);

  sm_application_s *app = (sm_application_s *)user_data;

  app->is_running = false;

  return true;
}

b8 sm_application_on_window_resize(sm_event_s *event, void *user_data)
{

  SM_CORE_ASSERT(event);

  sm_renderer_on_resize(event->window.width, event->window.width);

  return true;
}

void sm_application_push_layer(sm_application_s *app, struct sm_layer_s *layer)
{

  SM_CORE_ASSERT(app);
  SM_CORE_ASSERT(layer);

  sm_stack_layer_push(app->stack, layer);
  sm_layer_attach(layer);
}

void sm_application_push_overlay(sm_application_s *app, struct sm_layer_s *layer)
{

  SM_CORE_ASSERT(app);
  SM_CORE_ASSERT(layer);

  sm_stack_layer_push_overlay(app->stack, layer);
  sm_layer_attach(layer);
}

SM_PRIVATE
void sm__application_cap_frame_rate(long *then, float *remainder)
{
  long wait, frameTime;
  wait = (long int)((1000.0f / 60.0f) + *remainder);

  *remainder -= fabsf(*remainder);

  frameTime = SM_GET_TICKS( ) - *then;

  wait -= frameTime;
  if (wait < 1) {
    wait = 1;
  }
  SM_DELAY((u32)wait);

  *remainder += 0.667f;

  *then = SM_GET_TICKS( );
}

SM_PRIVATE
void sm__application_status_gui(sm_application_s *app)
{
  i32 corner = (app != NULL) ? 1 : 0;

  ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_AlwaysAutoResize |
                                  ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing |
                                  ImGuiWindowFlags_NoNav;
  if (corner != -1) {
    const f32 PAD = 10.0f;
    const ImGuiViewport *viewport = igGetMainViewport( );
    ImVec2 work_pos = viewport->WorkPos; // Use work area to avoid menu-bar/task-bar, if any!
    ImVec2 work_size = viewport->WorkSize;
    ImVec2 window_pos, window_pos_pivot;
    window_pos.x = (corner & 1) ? (work_pos.x + work_size.x - PAD) : (work_pos.x + PAD);
    window_pos.y = (corner & 2) ? (work_pos.y + work_size.y - PAD) : (work_pos.y + PAD);
    window_pos_pivot.x = (corner & 1) ? 1.0f : 0.0f;
    window_pos_pivot.y = (corner & 2) ? 1.0f : 0.0f;
    igSetNextWindowPos(window_pos, ImGuiCond_Always, window_pos_pivot);
    window_flags |= ImGuiWindowFlags_NoMove;
  }

  igSetNextWindowBgAlpha(0.35f); // Transparent background
  if (igBegin("Application status overlay", NULL, window_flags)) {
    igText("average %.3f ms/frame (%.1f FPS)", 1000.0 / app->fps, app->fps);
    igText("Delta: %f", app->delta * (f64)app->thickness);
    igText("Vsync: %s", sm_window_is_vsync(app->window) ? "true" : "false");
    igText("Mem: %.2f KB", ((f64)(get_bytes_in_use( )) / ((f64)(1 << 10))));
  }
  igEnd( );
}

#undef SM_MODULE_NAME
