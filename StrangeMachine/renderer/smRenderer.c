#include "smpch.h"

#include "core/smCore.h"

#include "renderer/smMaterialManager.h"
#include "renderer/smRenderer.h"
#include "renderer/smShaderManager.h"
#include "renderer/smTextureManager.h"

#include "renderer/GL/smGL.h"

typedef enum {
  SM_SHADER_LOC_VERTEX_POSITION = 0, // Shader location: vertex attribute: position
  SM_SHADER_LOC_VERTEX_UV,           // Shader location: vertex attribute: texcoord01
  SM_SHADER_LOC_VERTEX_COLOR,        // Shader location: vertex attribute: color
  SM_SHADER_LOC_VERTEX_NORMAL,       // Shader location: vertex attribute: normal

  SM_SHADER_LOC_MATRIX_MODEL, // Shader location: matrix uniform: model (transform)
  SM_SHADER_LOC_MATRIX_PV,    // Shader location: matrix uniform: model-view-projection

  SM_SHADER_LOC_COLOR_DIFFUSE, // Shader location: Color diffuse: albedo

  SM_SHADER_LOC_MAP_DIFFUSE, // Shader location: sampler2d texture: albedo
  SM_SHADER_LOC_MAX,

} ShaderLocationIndex;

typedef struct sm__renderer_s {

  sm_shader_handler_s default_shader;
  i32 default_shader_locs[SM_SHADER_LOC_MAX];

  sm_texture_handler_s default_texture;
  sm_vec4 default_diffuse_color;

} sm_renderer_s;

sm_renderer_s RENDERER = {0};

void sm_renderer_init( )
{

  u8 *pixels = SM_MALLOC(sizeof(u8) * 4);
  memcpy(pixels, (u8[4]){211, 211, 211, 255}, sizeof(u8) * 4);

  RENDERER.default_texture = sm_texture_manager_new_from_mem(pixels, 1, 1, SM_PIXELFORMAT_UNCOMPRESSED_R8G8B8A8);
  sm_texture_manager_gpu_load_data(RENDERER.default_texture);

  if (RENDERER.default_texture.handle == SM_INVALID_HANDLE) SM_UNREACHABLE( );

  SM_LOG_INFO("[%d] Default texture loaded successfully", RENDERER.default_texture.handle);

  sm_shader_resource_handler_s shader_resource = sm_shader_resource_new("assets/shaders/renderer3D.shader");

  RENDERER.default_shader = sm_shader_manager_new(shader_resource);
  if (RENDERER.default_shader.handle == SM_INVALID_HANDLE) SM_UNREACHABLE( );

  glm_vec4_copy(sm_vec4_one( ).data, RENDERER.default_diffuse_color.data);

  RENDERER.default_shader_locs[SM_SHADER_LOC_VERTEX_POSITION] =
      sm_shader_manager_get_attribute_location(RENDERER.default_shader, "a_position");
  RENDERER.default_shader_locs[SM_SHADER_LOC_VERTEX_UV] =
      sm_shader_manager_get_attribute_location(RENDERER.default_shader, "a_uv");
  RENDERER.default_shader_locs[SM_SHADER_LOC_VERTEX_COLOR] =
      sm_shader_manager_get_attribute_location(RENDERER.default_shader, "a_color");
  RENDERER.default_shader_locs[SM_SHADER_LOC_VERTEX_NORMAL] =
      sm_shader_manager_get_attribute_location(RENDERER.default_shader, "a_normal");
  RENDERER.default_shader_locs[SM_SHADER_LOC_MATRIX_MODEL] =
      sm_shader_manager_get_uniform_location(RENDERER.default_shader, "u_model");
  RENDERER.default_shader_locs[SM_SHADER_LOC_MATRIX_PV] =
      sm_shader_manager_get_uniform_location(RENDERER.default_shader, "u_pv");
  RENDERER.default_shader_locs[SM_SHADER_LOC_COLOR_DIFFUSE] =
      sm_shader_manager_get_uniform_location(RENDERER.default_shader, "u_color_diffuse");
  RENDERER.default_shader_locs[SM_SHADER_LOC_MAP_DIFFUSE] =
      sm_shader_manager_get_uniform_location(RENDERER.default_shader, "u_tex0");

  SM_LOG_INFO("[%d] Default shader loaded successfully", RENDERER.default_shader.handle);
}

b8 sm_mesh_upload(sm_mesh_s *mesh, bool dyn)
{

  SM_ASSERT(mesh);

  if (mesh->vao > 0) {
    SM_LOG_WARN("Trying to re-upload a mesh");
    return false;
  }

  mesh->vao = sm_gl_vao_new( );
  sm_gl_vao_bind(mesh->vao);

  /* Generate positions buffer */
  mesh->vbos[0] = sm_gl_vbo_new(mesh->positions, (u32)(SM_ARRAY_LEN(mesh->positions) * sizeof(sm_vec3)), dyn);
  sm_gl_vertex_attribute_pointer((u32)RENDERER.default_shader_locs[SM_SHADER_LOC_VERTEX_POSITION], 3, SM_F32, false, 0, 0);
  sm_gl_vertex_attribute_enable((u32)RENDERER.default_shader_locs[SM_SHADER_LOC_VERTEX_POSITION]);

  /* Generate uvs buffer */
  mesh->vbos[1] = sm_gl_vbo_new(mesh->uvs, (u32)SM_ARRAY_LEN(mesh->uvs) * sizeof(sm_vec2), dyn);
  sm_gl_vertex_attribute_enable((u32)RENDERER.default_shader_locs[SM_SHADER_LOC_VERTEX_UV]);
  sm_gl_vertex_attribute_pointer((u32)RENDERER.default_shader_locs[SM_SHADER_LOC_VERTEX_UV], 2, SM_F32, false, 0, NULL);

  /* Generate colors buffer */
  mesh->vbos[2] = sm_gl_vbo_new(mesh->colors, (u32)(SM_ARRAY_LEN(mesh->colors) * sizeof(sm_vec4)), dyn);
  sm_gl_vertex_attribute_enable((u32)RENDERER.default_shader_locs[SM_SHADER_LOC_VERTEX_COLOR]);
  sm_gl_vertex_attribute_pointer((u32)RENDERER.default_shader_locs[SM_SHADER_LOC_VERTEX_COLOR], 4, SM_F32, false, 0, 0);

  /* Generate normals buffer */
  mesh->vbos[3] = sm_gl_vbo_new(mesh->normals, (u32)(SM_ARRAY_LEN(mesh->normals) * sizeof(sm_vec3)), dyn);
  sm_gl_vertex_attribute_enable((u32)RENDERER.default_shader_locs[SM_SHADER_LOC_VERTEX_NORMAL]);
  sm_gl_vertex_attribute_pointer((u32)RENDERER.default_shader_locs[SM_SHADER_LOC_VERTEX_NORMAL], 3, SM_F32, false, 0, 0);

  /* Generate indices buffer */
  mesh->ebo = sm_gl_ebo_new(mesh->indices, (u32)(sizeof(u32) * SM_ARRAY_LEN(mesh->indices)), dyn);

  sm_gl_vao_unbind( );

  /* just in case */
  sm_gl_vbo_unbind( );
  sm_gl_ebo_unbind( );

  if (mesh->vao > 0) {
    sm_mesh_component_set_dirty(mesh, false);

    SM_LOG_TRACE("mesh uploaded successfully to VRAM");
    return true;
  }

  return false;
}

b8 sm_mesh_update(sm_mesh_s *mesh)
{

  if (mesh->vao == 0) return sm_mesh_upload(mesh, false);

  /* glEnableVertexAttribArray(mesh->vao); */
  sm_mesh_component_set_dirty(mesh, false);
  SM_LOG_TRACE("mesh updated successfully to VRAM");

  return true;
}

void sm__default_renderer_cb(sm_scene_s *scene, u32 index, void *user_data)
{

  if (index == ROOT) return;

  sm_ecs_s *ecs = (sm_ecs_s *)user_data;

  sm_entity_s cam_entt = sm_scene_get_current_camera(scene);
  const sm_camera_s *camera = sm_ecs_component_get_data(ecs, cam_entt, SM_CAMERA_COMP);

  sm_mat4 view_matrix, projection_matrix, view_projection_matrix;
  sm_camera_component_get_projection(camera, projection_matrix.data);
  sm_camera_component_get_view(camera, view_matrix.data);

  glm_mat4_mul(projection_matrix.data, view_matrix.data, view_projection_matrix.data);

  sm_entity_s *entities = sm_scene_get_entity(scene, index);
  for (size_t i = 0; i < SM_ARRAY_LEN(entities); ++i) {

    sm_entity_s ett = entities[i];
    if (!SM_MASK_CHK(ett.archetype_index, SM_MESH_COMP)) continue;

    const sm_mesh_s *mesh = sm_ecs_component_get_data(ecs, ett, SM_MESH_COMP);
    sm_texture_handler_s texture = RENDERER.default_texture;
    sm_shader_handler_s shader = RENDERER.default_shader;
    sm_vec4 diffuse_color = RENDERER.default_diffuse_color;

    sm_material_handler_s material = sm_material_manager_new(mesh->material_uuid);
    if (material.handle != SM_INVALID_HANDLE) {
      sm_texture_handler_s material_diffuse_texture = sm_material_manager_get_diffuse_map(material);
      sm_shader_handler_s material_shader = sm_material_manager_get_shader(material);
      sm_vec4 material_diffuse_color = sm_material_manager_get_diffuse_color(material);

      if (material_diffuse_texture.handle != SM_INVALID_HANDLE) {
        texture = material_diffuse_texture;
        if (!sm_texture_manager_gpu_has_data(texture)) sm_texture_manager_gpu_load_data(texture);
      }
      if (material_shader.handle != SM_INVALID_HANDLE) shader = material_shader;
      if (!glm_vec4_eq((float *)material_diffuse_color.data, 0.0f)) {
        diffuse_color.r = material_diffuse_color.r;
        diffuse_color.g = material_diffuse_color.g;
        diffuse_color.b = material_diffuse_color.b;
        diffuse_color.a = material_diffuse_color.a;
      }
    }

    /* if (SM_MASK_CHK(ett.archetype_index, SM_MATERIAL_COMP)) { */
    /*   const sm_material_s *m = sm_ecs_component_get_data(ecs, ett, SM_MATERIAL_COMP); */
    /**/
    /*   if (m->diffuse_map.handle != SM_INVALID_HANDLE) { */
    /*     texture = m->diffuse_map; */
    /*     if (!sm_texture_manager_gpu_has_data(m->diffuse_map)) sm_texture_manager_gpu_load_data(m->diffuse_map); */
    /*   }; */
    /*   if (m->shader.handle != SM_INVALID_HANDLE) shader = m->shader; */
    /*   if (!glm_vec4_eq((float *)m->color.data, 0.0f)) { */
    /*     diffuse_color.r = m->color.r; */
    /*     diffuse_color.g = m->color.g; */
    /*     diffuse_color.b = m->color.b; */
    /*     diffuse_color.a = m->color.a; */
    /*   } */
    /**/
    /* } */

    if (sm_mesh_component_is_renderable(mesh)) {

      if (sm_mesh_component_is_dirty(mesh))
        if (!sm_mesh_update((sm_mesh_s *)mesh)) {
          SM_LOG_ERROR("failed to update mesh");
          return;
        }

      sm_mat4 model;
      sm_transform_to_mat4(sm_scene_get_global_transform(scene, index), model.data);

      sm_gl_vao_bind(mesh->vao);
      sm_shader_manager_bind(shader);

      i32 value = 0;
      sm_shader_manager_set_uniform_location(RENDERER.default_shader_locs[SM_SHADER_LOC_MAP_DIFFUSE], SM_I32, &value);
      sm_texture_manager_gpu_bind(texture, 0);

      sm_shader_manager_set_uniform_location(RENDERER.default_shader_locs[SM_SHADER_LOC_MATRIX_MODEL], SM_MAT4, model.float16);
      sm_shader_manager_set_uniform_location(RENDERER.default_shader_locs[SM_SHADER_LOC_MATRIX_PV], SM_MAT4,
                                             view_projection_matrix.float16);
      sm_shader_manager_set_uniform_location(RENDERER.default_shader_locs[SM_SHADER_LOC_COLOR_DIFFUSE], SM_VEC4,
                                             diffuse_color.data);

      sm_gl_draw_elements((i32)SM_ARRAY_LEN(mesh->indices), NULL);

      sm_texture_manager_gpu_bind(texture, 0);
      sm_shader_manager_unbind( );
      sm_gl_vao_unbind( );
    }
  }
}

void sm_renderer_on_resize(i32 width, i32 height)
{

  sm_gl_viewport(0, 0, width, height);
}

void sm_renderer_clear(sm_vec4 color)
{

  sm_gl_clear_color(color);
  sm_gl_clear(SM_DEPTH_BUFFER_BIT | SM_COLOR_BUFFER_BIT);
}

void sm_renderer_draw_scene(sm_scene_s *scene, sm_ecs_s *ecs)
{

  SM_ASSERT(scene);

  sm_gl_enable(SM_DEPTH_TEST);

  sm_scene_for_each(scene, 0, sm__default_renderer_cb, ecs);
}

void sm_renderer_teardown(void) { }
