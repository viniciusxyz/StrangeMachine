#include "smpch.h"

#include "vendor/glad/glad.h"

#include "core/smCore.h"
#include "core/util/smString.h"

#include "ecs/smComponents.h"

#undef SM_MODULE_NAME
#define SM_MODULE_NAME "COMPONENTS"

const sm_component_t SM_VELOCITY_COMP = 1ULL << 0;
const sm_component_t SM_FORCE_COMP = 1ULL << 1;
const sm_component_t SM_SPEED_COMP = 1ULL << 2;
const sm_component_t SM_MESH_COMP = 1ULL << 3;
/* const sm_component_t SM_MATERIAL_COMP = 1ULL << 4; */
const sm_component_t SM_CAMERA_COMP = 1ULL << 4;
const sm_component_t SM_ALL_COMP = SM_VELOCITY_COMP | SM_FORCE_COMP | SM_SPEED_COMP | SM_MESH_COMP | SM_CAMERA_COMP;

static int initialized = 0;
SM_ARRAY(sm_component_view_s) COMPONENTS = NULL;

void component_init(void)
{

  if (initialized++ > 0) {
    return;
  }

  SM_ASSERT(COMPONENTS == NULL && "Component system already initialized");

  sm_component_view_s vel_view = {.name = SM_STRING_DUP("velocity"),
                                  .size = sizeof(sm_velocity_s),
                                  .alligned = false,
                                  .id = SM_VELOCITY_COMP,
                                  .write = sm_vec3_write,
                                  .read = sm_vec3_read,
                                  .dtor = NULL};

  sm_component_view_s force_view = {.name = SM_STRING_DUP("force"),
                                    .size = sizeof(sm_force_s),
                                    .alligned = false,
                                    .id = SM_FORCE_COMP,
                                    .write = sm_vec3_write,
                                    .read = sm_vec3_read,
                                    .dtor = NULL};

  sm_component_view_s speed_view = {.name = SM_STRING_DUP("speed"),
                                    .size = sizeof(sm_speed_s),
                                    .alligned = false,
                                    .id = SM_SPEED_COMP,
                                    .write = sm_speed_write,
                                    .read = sm_speed_read,
                                    .dtor = NULL};

  sm_component_view_s mesh_view = {.name = SM_STRING_DUP("mesh"),
                                   .size = sizeof(sm_mesh_s),
                                   .alligned = false,
                                   .id = SM_MESH_COMP,
                                   .write = sm_mesh_write,
                                   .read = sm_mesh_read,
                                   .dtor = sm_mesh_dtor};

  /* sm_component_view_s material_view = {.name = sm_string_from("material"), */
  /*                                      .size = sizeof(sm_material_s), */
  /*                                      .alligned = false, */
  /*                                      .id = SM_MATERIAL_COMP, */
  /*                                      .write = sm_material_write, */
  /*                                      .read = sm_material_read, */
  /*                                      .dtor = NULL}; */

  sm_component_view_s camera_view = {.name = SM_STRING_DUP("camera"),
                                     .size = sizeof(sm_camera_s),
                                     .alligned = false,
                                     .id = SM_CAMERA_COMP,
                                     .write = sm_camera_write,
                                     .read = sm_camera_read,
                                     .dtor = NULL};

  SM_ARRAY_PUSH(COMPONENTS, vel_view);
  SM_ARRAY_PUSH(COMPONENTS, force_view);
  SM_ARRAY_PUSH(COMPONENTS, speed_view);
  SM_ARRAY_PUSH(COMPONENTS, mesh_view);
  /* SM_ARRAY_PUSH(COMPONENTS, material_view); */
  SM_ARRAY_PUSH(COMPONENTS, camera_view);
}

void component_register_component(sm_component_view_s *desc)
{

  SM_ASSERT(COMPONENTS != NULL && "Component system not initialized");
  SM_ASSERT(desc != NULL);
  SM_ASSERT(desc->id != 0);

#ifdef SM_DEBUG
  for (size_t i = 0; i < SM_ARRAY_LEN(COMPONENTS); i++) {
    SM_ASSERT(COMPONENTS[i].id != desc->id && "Component already registered");
  }
#endif

  SM_ARRAY_PUSH(COMPONENTS, *desc);
}

const sm_component_view_s *component_get_desc(sm_component_t id)
{

  SM_ASSERT(COMPONENTS != NULL && "Component system not initialized");

  for (size_t i = 0; i < SM_ARRAY_LEN(COMPONENTS); i++) {
    if (COMPONENTS[i].id == id) {
      return &COMPONENTS[i];
    }
  }

  return NULL;
}

void component_teardown(void)
{

  if (--initialized > 0) {
    return;
  }

  SM_LOG_TRACE("Component system teardown");

  SM_ASSERT(COMPONENTS != NULL && "Component system not initialized");

  for (size_t i = 0; i < SM_ARRAY_LEN(COMPONENTS); i++) {
    if (COMPONENTS[i].name) SM_STRING_FREE(COMPONENTS[i].name);
  }

  SM_ARRAY_DTOR(COMPONENTS);

  COMPONENTS = NULL;
}

b8 sm_mesh_write(const sm_file_handle_s *handle, const void *ptr)
{

  SM_ASSERT(ptr);

  sm_mesh_s *mesh = (sm_mesh_s *)ptr;
  sm_file_handle_s *fh = (sm_file_handle_s *)handle;
  SM_LOG_TRACE("Writing mesh...");

  size_t len = 0, size = 0;

  /* Write positions */
  len = SM_ARRAY_LEN(mesh->positions);
  size = sizeof(*mesh->positions);
  SM_LOG_TRACE("positions: len: %lu, size: %lu", len, size);
  if (!sm_filesystem_write_bytes(fh, &len, sizeof(size_t))) {
    SM_LOG_ERROR("[s] failed to write the length of positions");
    return false;
  }
  if (!sm_filesystem_write_bytes(fh, &size, sizeof(size_t))) {
    SM_LOG_ERROR("[s] failed to write the size of positions");
    return false;
  }
  if (!sm_filesystem_write_bytes(fh, mesh->positions, len * size)) {
    SM_LOG_ERROR("[s] failed to write positions");
    return false;
  }

  /* Write UVs */
  len = SM_ARRAY_LEN(mesh->uvs);
  size = sizeof(*mesh->uvs);
  SM_LOG_TRACE("uvs: len: %lu, size: %lu", len, size);
  if (!sm_filesystem_write_bytes(fh, &len, sizeof(size_t))) {
    SM_LOG_ERROR("[s] failed to write the length of uvs");
    return false;
  }
  if (!sm_filesystem_write_bytes(fh, &size, sizeof(size_t))) {
    SM_LOG_ERROR("[s] failed to write the size of uvs");
    return false;
  }
  if (!sm_filesystem_write_bytes(fh, mesh->uvs, len * size)) {
    SM_LOG_ERROR("[s] failed to write uvs");
    return false;
  }

  /* Write colors */
  len = SM_ARRAY_LEN(mesh->colors);
  size = sizeof(*mesh->colors);
  SM_LOG_TRACE("colors: len: %lu, size: %lu", len, size);
  if (!sm_filesystem_write_bytes(fh, &len, sizeof(size_t))) {
    SM_LOG_ERROR("[s] failed to write the length of colors");
    return false;
  }
  if (!sm_filesystem_write_bytes(fh, &size, sizeof(size_t))) {
    SM_LOG_ERROR("[s] failed to write the size of colors");
    return false;
  }
  if (!sm_filesystem_write_bytes(fh, mesh->colors, len * size)) {
    SM_LOG_ERROR("[s] failed to write colors");
    return false;
  }

  /* Write normals */
  len = SM_ARRAY_LEN(mesh->normals);
  size = sizeof(*mesh->normals);
  SM_LOG_TRACE("normals: len: %lu, size: %lu", len, size);
  if (!sm_filesystem_write_bytes(fh, &len, sizeof(size_t))) {
    SM_LOG_ERROR("[s] failed to write the length of normals");
    return false;
  }
  if (!sm_filesystem_write_bytes(fh, &size, sizeof(size_t))) {
    SM_LOG_ERROR("[s] failed to write the size of normals");
    return false;
  }
  if (!sm_filesystem_write_bytes(fh, mesh->normals, len * size)) {
    SM_LOG_ERROR("[s] failed to write normals");
    return false;
  }

  /* Write indices */
  len = SM_ARRAY_LEN(mesh->indices);
  size = sizeof(*mesh->indices);
  SM_LOG_TRACE("indices: len: %lu, size: %lu", len, size);
  if (!sm_filesystem_write_bytes(fh, &len, sizeof(size_t))) {
    SM_LOG_ERROR("[s] failed to write the length of indices");
    return false;
  }
  if (!sm_filesystem_write_bytes(fh, &size, sizeof(size_t))) {
    SM_LOG_ERROR("[s] failed to write the size of indices");
    return false;
  }
  if (!sm_filesystem_write_bytes(fh, mesh->indices, len * size)) {
    SM_LOG_ERROR("[s] failed to write indices");
    return false;
  }

  if (!sm_filesystem_write_bytes(fh, &mesh->flags, sizeof(mesh->flags))) {
    SM_LOG_ERROR("[s] failed to write flags");
    return false;
  }

  if (!sm_filesystem_write_bytes(fh, &mesh->material_uuid, sizeof(u32))) {
    SM_LOG_ERROR("[s] failed to write uuid");
    return false;
  }

  return true;
}

b8 sm_mesh_read(const sm_file_handle_s *handle, const void *ptr)
{

  SM_ASSERT(ptr);

  sm_mesh_s *mesh = (sm_mesh_s *)ptr;
  sm_file_handle_s *fh = (sm_file_handle_s *)handle;

  size_t len = 0, size = 0;

  /* Read positions */
  if (!sm_filesystem_read_bytes(fh, &len, sizeof(size_t))) {
    SM_LOG_ERROR("[s] failed to read the length of positions");
    return false;
  }
  if (!sm_filesystem_read_bytes(fh, &size, sizeof(size_t))) {
    SM_LOG_ERROR("[s] failed to read the size of positions");
    return false;
  }
  SM_ARRAY_SET_LEN(mesh->positions, len);
  if (!sm_filesystem_read_bytes(fh, mesh->positions, len * size)) {
    SM_LOG_ERROR("[s] failed to read vertices");
    return false;
  }

  /* Read UVs */
  if (!sm_filesystem_read_bytes(fh, &len, sizeof(size_t))) {
    SM_LOG_ERROR("[s] failed to read the length of uvs");
    return false;
  }
  if (!sm_filesystem_read_bytes(fh, &size, sizeof(size_t))) {
    SM_LOG_ERROR("[s] failed to read the size of uvs");
    return false;
  }
  SM_ARRAY_SET_LEN(mesh->uvs, len);
  if (!sm_filesystem_read_bytes(fh, mesh->uvs, len * size)) {
    SM_LOG_ERROR("[s] failed to read uvs");
    return false;
  }

  /* Read colors */
  if (!sm_filesystem_read_bytes(fh, &len, sizeof(size_t))) {
    SM_LOG_ERROR("[s] failed to read the length of colors");
    return false;
  }
  if (!sm_filesystem_read_bytes(fh, &size, sizeof(size_t))) {
    SM_LOG_ERROR("[s] failed to read the size of colors");
    return false;
  }
  SM_ALIGNED_ARRAY_NEW(mesh->colors, 16, len);
  if (!sm_filesystem_read_bytes(fh, mesh->colors, len * size)) {
    SM_LOG_ERROR("[s] failed to read colors");
    return false;
  }

  /* Read normals */
  if (!sm_filesystem_read_bytes(fh, &len, sizeof(size_t))) {
    SM_LOG_ERROR("[s] failed to read the length of normals");
    return false;
  }
  if (!sm_filesystem_read_bytes(fh, &size, sizeof(size_t))) {
    SM_LOG_ERROR("[s] failed to read the size of normals");
    return false;
  }
  SM_ARRAY_SET_LEN(mesh->normals, len);
  if (!sm_filesystem_read_bytes(fh, mesh->normals, len * size)) {
    SM_LOG_ERROR("[s] failed to read normals");
    return false;
  }

  /* Read indices */
  if (!sm_filesystem_read_bytes(fh, &len, sizeof(size_t))) {
    SM_LOG_ERROR("[s] failed to read the length of indices");
    return false;
  }
  if (!sm_filesystem_read_bytes(fh, &size, sizeof(size_t))) {
    SM_LOG_ERROR("[s] failed to read the size of indices");
    return false;
  }
  SM_ARRAY_SET_LEN(mesh->indices, len);
  if (!sm_filesystem_read_bytes(fh, mesh->indices, len * size)) {
    SM_LOG_ERROR("[s] failed to read indices");
    return false;
  }

  if (!sm_filesystem_read_bytes(fh, &mesh->flags, sizeof(mesh->flags))) {
    SM_LOG_ERROR("[s] failed to read flags");
    return false;
  }

  if (!sm_filesystem_read_bytes(fh, &mesh->material_uuid, sizeof(u32))) {
    SM_LOG_ERROR("[s] failed to read uuid");
    return false;
  }

  sm_mesh_component_set_dirty(mesh, true);

  return true;
}

void sm_mesh_component_copy(sm_mesh_s *dest, const sm_mesh_s *src)
{

  size_t size = SM_ARRAY_LEN(src->positions);
  size_t indices_size = SM_ARRAY_LEN(src->indices);

  SM_ARRAY_SET_LEN(dest->positions, size);
  SM_ARRAY_SET_LEN(dest->uvs, size);
  SM_ARRAY_SET_LEN(dest->normals, size);
  SM_ALIGNED_ARRAY_NEW(dest->colors, 16, size);
  SM_ARRAY_SET_LEN(dest->indices, indices_size);

  memcpy(dest->positions, src->positions, size * sizeof(sm_vec3));
  memcpy(dest->uvs, src->uvs, size * sizeof(sm_vec2));
  memcpy(dest->normals, src->normals, size * sizeof(sm_vec3));
  memcpy(dest->colors, src->colors, size * sizeof(sm_vec4));
  memcpy(dest->indices, src->indices, indices_size * sizeof(u32));

  dest->flags = src->flags;
}

void sm_mesh_dtor(void *ptr)
{

  SM_ASSERT(ptr);

  sm_mesh_s *mesh = (sm_mesh_s *)ptr;

  SM_ARRAY_DTOR(mesh->positions);
  SM_ARRAY_DTOR(mesh->uvs);
  SM_ALIGNED_ARRAY_DTOR(mesh->colors);
  SM_ARRAY_DTOR(mesh->normals);
  SM_ARRAY_DTOR(mesh->indices);
}

// b8 sm_material_write(const sm_file_handle_s *handle, const void *ptr) {
//
//   sm_material_s *material = (sm_material_s *)ptr;
//   SM_LOG_TRACE("Writing material...");
//   SM_LOG_TRACE("Material name: %s", material->name.str);
//
//   /* Writing the name length */
//   size_t name_len = sm_string_len(material->name);
//   if (!sm_filesystem_write_bytes(handle, &name_len, sizeof(size_t))) {
//     SM_LOG_ERROR("failed to write name len");
//     return false;
//   }
//
//   /* Writing the name */
//   if (!sm_filesystem_write_bytes(handle, material->name.str, name_len)) {
//     SM_LOG_ERROR("failed to write name");
//     return false;
//   }
//
//   return true;
// }

// b8 sm_material_read(const sm_file_handle_s *handle, const void *ptr) {
//
//   sm_material_s *material = (sm_material_s *)ptr;
//   SM_LOG_TRACE("Reading material...");
//
//   size_t name_len = 0;
//   if (!sm_filesystem_read_bytes(handle, &name_len, sizeof(size_t))) {
//     SM_LOG_ERROR("failed to read name len");
//     return false;
//   }
//   SM_LOG_TRACE("Name Length: %lu", name_len);
//
//   char buf[name_len + 1];
//   if (!sm_filesystem_read_bytes(handle, buf, name_len)) {
//     SM_LOG_ERROR(" failed to read the string");
//     return false;
//   }
//   buf[name_len] = '\0';
//   SM_LOG_TRACE("Name: %s", buf);
//   sm_string name = sm_string_from(buf);
//   material->name = sm_string_reference(name);
//   name.str = sm_string_dtor(name);
//
//   return true;
// }

b8 sm_speed_write(const sm_file_handle_s *handle, const void *ptr)
{

  SM_ASSERT(ptr);

  sm_speed_s *speed = (sm_speed_s *)ptr;
  SM_LOG_TRACE("Writing speed...");

  if (!sm_filesystem_write_bytes(handle, speed, sizeof(sm_speed_s))) {
    SM_LOG_ERROR("failed to write speed");
    return false;
  }

  return true;
}

b8 sm_speed_read(const sm_file_handle_s *handle, const void *ptr)
{

  SM_ASSERT(ptr);

  sm_speed_s *speed = (sm_speed_s *)ptr;
  SM_LOG_TRACE("Reading speed...");

  if (!sm_filesystem_read_bytes(handle, speed, sizeof(sm_speed_s))) {
    SM_LOG_ERROR("failed to read speed");
    return false;
  }

  return true;
}

b8 sm_vec3_write(const sm_file_handle_s *handle, const void *ptr)
{

  SM_ASSERT(ptr);

  sm_vec3 *vec3 = (sm_vec3 *)ptr;
  SM_LOG_TRACE("Writing vec3...");

  if (!sm_filesystem_write_bytes(handle, vec3, sizeof(sm_vec3))) {
    SM_LOG_ERROR("failed to write vec3");
    return false;
  }

  return true;
}

b8 sm_vec3_read(const sm_file_handle_s *handle, const void *ptr)
{

  SM_ASSERT(ptr);

  sm_vec3 *vec3 = (sm_vec3 *)ptr;
  SM_LOG_TRACE("Reading vec3...");

  if (!sm_filesystem_read_bytes(handle, vec3, sizeof(sm_vec3))) {
    SM_LOG_ERROR("failed to read vec3");
    return false;
  }

  return true;
}

b8 sm_camera_write(const sm_file_handle_s *handle, const void *ptr)
{

  SM_ASSERT(ptr);

  sm_camera_s *camera = (sm_camera_s *)ptr;
  SM_LOG_TRACE("Writing camera...");

  if (!sm_filesystem_write_bytes(handle, camera, sizeof(sm_camera_s))) {
    SM_LOG_ERROR("failed to write camera");
    return false;
  }

  return true;
}

b8 sm_camera_read(const sm_file_handle_s *handle, const void *ptr)
{

  SM_ASSERT(ptr);

  sm_camera_s *camera = (sm_camera_s *)ptr;
  SM_LOG_TRACE("Reading camera...");

  if (!sm_filesystem_read_bytes(handle, camera, sizeof(sm_camera_s))) {
    SM_LOG_ERROR("failed to read camera");
    return false;
  }

  return true;
}

#undef SM_MODULE_NAME
