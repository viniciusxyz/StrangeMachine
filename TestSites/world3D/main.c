#include <assert.h>
#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>

#define SM_DEBUG
#include "cimgui/smCimguiImpl.h"
#include "ecs/smComponents.h"
#include "ecs/smECS.h"
#include "math/smMath.h"
#include "renderer/smRenderer.h"
#include "scene/smScene.h"
#include "smInput.h"

#define REF(X) X

typedef struct {

  sm_scene_s *scene;
  sm_ecs_s *ecs;
  struct sm_layer_s *layer;

} lab_s;

void sm__camera_free_do(sm_system_iterator_s *iter, float dt)
{

  while (system_iter_next(iter)) {

    sm_camera_s *camera = (sm_camera_s *)iter->iter_data[0].data;

    sm_vec3 pos;

    if (input_scan_key(sm_key_s)) {
      glm_vec3_scale(camera->target.data, camera->move_speed * dt, pos.data);
      glm_vec3_add(camera->position.data, pos.data, camera->position.data);
    }

    if (input_scan_key(sm_key_w)) {
      glm_vec3_scale(camera->target.data, camera->move_speed * dt, pos.data);
      glm_vec3_sub(camera->position.data, pos.data, camera->position.data);
    }

    if (input_scan_key(sm_key_d)) {
      glm_vec3_scale(camera->right.data, camera->move_speed * dt, pos.data);
      glm_vec3_sub(camera->position.data, pos.data, camera->position.data);
    }

    if (input_scan_key(sm_key_a)) {
      glm_vec3_scale(camera->right.data, camera->move_speed * dt, pos.data);
      glm_vec3_add(camera->position.data, pos.data, camera->position.data);
    }

    if (input_scan_key(sm_key_space)) {
      glm_vec3_scale(camera->up.data, camera->move_speed * dt, pos.data);
      glm_vec3_add(camera->position.data, pos.data, camera->position.data);
    }
    if (input_scan_key(sm_key_lshift)) {
      glm_vec3_scale(camera->up.data, camera->move_speed * dt, pos.data);
      glm_vec3_sub(camera->position.data, pos.data, camera->position.data);
    }

    float offset_x = input_get_x_rel_mouse( );
    float offset_y = input_get_y_rel_mouse( );

    if (offset_x != 0 || offset_y != 0) {
      camera->angle.y += offset_x * camera->sensitive * dt;
      camera->angle.x -= offset_y * camera->sensitive * dt;

      if (camera->angle.x > 80.0f) camera->angle.x = 80.0f;
      if (camera->angle.x < -80.0f) camera->angle.x = -80.0f;

      if (camera->angle.y > 360.0f || camera->angle.y < -360.0f) camera->angle.y = 0.0f;
    }

    sm_vec3 front;
    front.x = cosf(glm_rad(camera->angle.y)) * cosf(glm_rad(camera->angle.x));
    front.y = sinf(glm_rad(camera->angle.x));
    front.z = sinf(glm_rad(camera->angle.y)) * cosf(glm_rad(camera->angle.x));

    glm_vec3_normalize_to(front.data, camera->target.data);

    sm_vec3 right;
    glm_vec3_cross(camera->target.data, (vec3){0.0f, 1.0f, 0.0f}, right.data);
    glm_vec3_normalize_to(right.data, camera->right.data);

    sm_vec3 up;
    glm_vec3_cross(camera->right.data, camera->target.data, up.data);
    glm_vec3_normalize_to(up.data, camera->up.data);
  }
}

void sm__mesh_move(sm_system_iterator_s *iter, float dt)
{

  (void)dt;

  while (system_iter_next(iter)) {

    sm_transform_s *trans = (sm_transform_s *)iter->iter_data[0].data;
    sm_speed_s *speed = (sm_speed_s *)iter->iter_data[1].data;

    trans->position.x += 10.0f * speed->speed * dt;
  }
}

typedef struct sm_render_user_data {
  sm_scene_s *scene;
  sm_ecs_s *ecs;
} sm_renderer_user_data;

void on_attach(void *user_data)
{

  assert(user_data);

  lab_s *lab = (lab_s *)user_data;

  sm_ecs_s *ecs = sm_ecs_new( );
  if (!sm_ecs_ctor(ecs, SM_ALL_COMP)) {
    printf("Failed to create ECS\n");
    return;
  }

  lab->ecs = ecs;

  const SM_STRING scene_file_path = "assets/scene/scene.smscene";
  lab->scene = sm_scene_open(scene_file_path, lab->ecs);
  if (!lab->scene) {
    printf("Failed to open scene\n");
    exit(EXIT_FAILURE);
  }

  sm_ecs_system_register(lab->ecs, SM_CAMERA_COMP, sm__camera_free_do, SM_SYSTEM_INCLUSIVE_FLAG);

  sm_entity_s ett = sm_ecs_entity_new(lab->ecs, SM_CAMERA_COMP);
  u32 idx = sm_scene_new_node(lab->scene);
  sm_scene_add_child(lab->scene, ROOT, idx);
  sm_scene_set_current_camera(lab->scene, ett);

  sm_camera_s camera = {
      /* .main = true, */
      .position = sm_vec3_new(0.0f, 0.0f, 1.0f),
      .target = sm_vec3_new(0.0f, 0.0f, 1.0f),
      .up = sm_vec3_new(0.0f, 1.0f, 0.0f),
      .move_speed = 12.0f,
      .sensitive = 15.0f,
      .target_distance = 10.0f,
      .fov = 75.0f,
      .angle = sm_vec3_new(0.0f, 0.0f, 0.0f),
      .aspect_ratio = 800.f / 600.f,
  };

  sm_ecs_component_set_data(lab->ecs, ett, SM_CAMERA_COMP, &camera);
  sm_scene_push_entity(lab->scene, idx, ett);
  const char *camera_name = "camera";
  sm_scene_set_name(lab->scene, idx, camera_name);

  if (1) return;

  /* sm_string cube = sm_string_from("Cube"); */
  /* u32 cube_idx = sm_scene_get_by_name(lab->scene, cube); */
  /* sm_string_dtor(cube); */
  /* sm_entity_s *etts = sm_scene_get_entity(lab->scene, cube_idx); */
  /**/
  /* sm_entity_s mesh_entity = {0}; */
  /* for (u32 i = 0; i < SM_ARRAY_LEN(etts); ++i) { */
  /*   if (SM_MASK_CHK_EQ(etts[i].archetype_index, SM_MESH_COMP)) { */
  /*     mesh_entity = etts[i]; */
  /*     break; */
  /*   } */
  /* } */
  /**/
  /* if (mesh_entity.handle == SM_INVALID_HANDLE) abort( ); */
  /**/
  /* const sm_mesh_s *mesh = sm_ecs_component_get_data(lab->ecs, mesh_entity, SM_MESH_COMP); */
  /**/
  /* sm_mesh_s new_mesh = {0}; */
  /**/
  /* sm_string file = sm_string_from("assets/texcube.png"); */
  /* sm_texture_handler_s texture = sm_texture_manager_new(file); */
  /* sm_texture_manager_gpu_load_data(texture); */
  /* sm_string_dtor(file); */
  /**/
  /* sm_material_s material = { */
  /*     .name = sm_string_from("My material"), .shader = {0}, .diffuse_map = texture, .color = sm_vec4_one( )}; */
  /**/
  /* sm_mesh_component_copy(&new_mesh, mesh); */
  /**/
  /* sm_entity_s new_entity = sm_ecs_entity_new(lab->ecs, SM_MESH_COMP | SM_MATERIAL_COMP); */
  /**/
  /* struct __attribute__((packed)) { */
  /*   sm_mesh_s mesh; */
  /*   sm_material_s material; */
  /* } data = {.mesh = new_mesh, .material = material}; */
  /**/
  /* sm_ecs_component_set_all_data(lab->ecs, new_entity, &data); */
  /**/
  /* u32 new_node = sm_scene_new_node(lab->scene); */
  /* sm_scene_add_child(lab->scene, ROOT, new_node); */
  /**/
  /* sm_string cube_name = sm_string_from("Cube2"); */
  /* sm_scene_set_name(lab->scene, new_node, cube_name); */
  /* sm_string_dtor(cube_name); */
  /* sm_scene_push_entity(lab->scene, new_node, new_entity); */
  /**/
  /* sm_transform_s transform = sm_transform_identity( ); */
  /* transform.position.x += 3.0f; */
  /**/
  /* sm_scene_set_local_transform(lab->scene, new_node, transform); */
}

void on_detach(void *user_data)
{

  assert(user_data);

  lab_s *lab = (lab_s *)user_data;

  /* sm_string path = sm_string_from("assets/scene/scene.smscene"); */
  /* sm_scene_save(lab->scene, path, lab->ecs); */
  /* sm_string_dtor(path); */

  /* DEVICE->texture_dtor(texture); */

  sm_ecs_dtor(lab->ecs);
  sm_scene_dtor(lab->scene);
}

void on_update(void *user_data, float dt)
{

  assert(user_data);
  (void)dt;

  lab_s *lab = (lab_s *)user_data;

  sm_scene_do(lab->scene, dt);
  sm_ecs_do(lab->ecs, dt);

  const SM_STRING suzanne = "Suzanne";
  u32 ett = sm_scene_get_by_name(lab->scene, suzanne);
  if (ett != INVALID_NODE) {

    sm_transform_s transform = sm_scene_get_local_transform(lab->scene, ett);
    transform.position.x += 4.f * dt;
    if (transform.position.x >= 10.f) transform.position.x = 0.0f;

    sm_scene_set_local_transform(lab->scene, ett, transform);
  }

  sm_renderer_clear(sm_vec4_new(128.f / 255.f, 128.f / 255.f, 128.0f / 255.f, 1.0f));
  sm_renderer_draw_scene(lab->scene, lab->ecs);
}

bool on_event(sm_event_s *event, void *user_data)
{

  assert(user_data);

  lab_s *lab = (lab_s *)user_data;
  (void)lab;

  switch (event->category) {
  case SM_CATEGORY_WINDOW:
    if (event->window.type == SM_EVENT_WINDOW_RESIZE) {
      sm_entity_s cam_ett = sm_scene_get_current_camera(lab->scene);
      const sm_camera_s *cam = sm_ecs_component_get_data(lab->ecs, cam_ett, SM_CAMERA_COMP);
      sm_camera_component_set_aspect_ratio((sm_camera_s *)cam, (float)event->window.width / (float)event->window.height);
      return true;
    }
  default: break;
  }
  return false;
}

void for_each_node(sm_scene_s *scene, u32 node_index, void *user_data)
{

  SM_UNUSED(user_data);
  const char *name = sm_scene_get_name(scene, node_index);
  /* sm_entity_s e = sm_scene_get_entity(scene, node_index); */
  igBulletText("%u, %s", node_index, name);
}

void on_gui(void *user_data)
{

  assert(user_data);

  lab_s *lab = (lab_s *)user_data;
  (void)lab;

  static bool show_window = true;

  if (show_window) igShowDemoWindow(&show_window);

  /* igShowDemoWindow(&show_window); */

  ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_AlwaysAutoResize |
                                  ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing |
                                  ImGuiWindowFlags_NoNav;
  igBegin("Nodes", &show_window, window_flags);

  if (igTreeNode_Str("Nodes")) {
    sm_scene_for_each(lab->scene, 0, for_each_node, NULL);
    igTreePop( );
  }

  igEnd( );
}

int main(void)
{

  SM_STRING file_path = SM_STRING_DUP("assets/trouble/cool.txt");
  SM_STRING name = basename(file_path);
  SM_STRING path = dirname(file_path);

  printf("file_path: %s\n", file_path);
  if (name) printf("name: %s\n", name);
  if (path) printf("path: %s\n", path);

  SM_STRING_FREE(file_path);

  struct sm__application_s *app = sm_application_new( );
  if (!sm_application_ctor(app, "lab")) {
    exit(EXIT_FAILURE);
  }

  lab_s *lab = calloc(1, sizeof(lab_s));

  struct sm_layer_s *layer = sm_layer_new( );
  if (!sm_layer_ctor(layer, "entity", lab, on_attach, on_detach, on_update, on_gui, on_event)) {
    exit(EXIT_FAILURE);
  }
  lab->layer = layer;

  sm_application_push_layer(app, layer);

  sm_application_do(app);

  sm_application_dtor(app);
  sm_layer_dtor(layer);
  free(lab);

  return EXIT_SUCCESS;
}
