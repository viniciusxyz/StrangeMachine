#include "smpch.h"

#include "event/smEvent.h"

#include "core/smAssert.h"
#include "core/smLayerPub.h"
#include "core/smMem.h"

typedef struct sm__layer_s {

  char *name;

  void *user_data;

  sm_layer_on_attach_f on_attach;
  sm_layer_on_detach_f on_detach;
  sm_layer_on_update_f on_update;
  sm_layer_on_gui_f on_gui;

  sm_layer_on_event_f on_event;

} sm_layer_s;

sm_layer_s *sm_layer_new(void)
{

  sm_layer_s *layer = SM_CALLOC(1, sizeof(sm_layer_s));
  SM_ASSERT(layer);

  return layer;
}

bool sm_layer_ctor(sm_layer_s *layer, char *name, void *user_data, sm_layer_on_attach_f on_attach,
                   sm_layer_on_detach_f on_detach, sm_layer_on_update_f on_update, sm_layer_on_gui_f on_gui,
                   sm_layer_on_event_f on_event)
{

  SM_ASSERT(layer);

  layer->name = strdup(name);
  layer->user_data = user_data;
  layer->on_attach = on_attach;
  layer->on_detach = on_detach;
  layer->on_update = on_update;
  layer->on_gui = on_gui;
  layer->on_event = on_event;

  return true;
}

void sm_layer_dtor(sm_layer_s *layer)
{

  SM_ASSERT(layer);

  free(layer->name);
  SM_FREE(layer);
}

void sm_layer_attach(sm_layer_s *layer)
{

  SM_ASSERT(layer);

  if (layer->on_attach) {
    layer->on_attach(layer->user_data);
  }
}

void sm_layer_detach(sm_layer_s *layer)
{

  SM_ASSERT(layer);

  if (layer->on_detach) {
    layer->on_detach(layer->user_data);
  }
}

void sm_layer_update(sm_layer_s *layer, float dt)
{

  SM_ASSERT(layer);

  if (layer->on_update) {
    layer->on_update(layer->user_data, dt);
  }
}

void sm_layer_gui(sm_layer_s *layer)
{

  SM_ASSERT(layer);

  if (layer->on_gui) {
    layer->on_gui(layer->user_data);
  }
}

bool sm_layer_event(sm_layer_s *layer, sm_event_s *event)
{

  SM_ASSERT(layer);

  if (layer->on_event) {
    return layer->on_event(event, layer->user_data);
  }

  return false;
}

void *sm_layer_get_user_data(sm_layer_s *layer)
{

  SM_ASSERT(layer);

  return layer->user_data;
}
