#ifndef SM_CIMGUI_H
#define SM_CIMGUI_H

#include "core/smLayer.h"
#include "core/smWindow.h"

#include "cimgui/smCimguiImpl.h"

typedef struct sm_layer_s sm_cimgui_s;

sm_cimgui_s *sm_cimgui_new(void);
b8 sm_cimgui_ctor(sm_cimgui_s *cimgui, struct sm_window_s *window);
void sm_cimgui_dtor(sm_cimgui_s *cimgui);

void sm_cimgui_begin(sm_cimgui_s *cimgui);
void sm_cimgui_end(sm_cimgui_s *cimgui);

#endif /* SM_CIMGUI_H */
