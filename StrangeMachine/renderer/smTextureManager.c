#include "smpch.h"

#include "core/smCore.h"

#include "renderer/GL/smGL.h"
#include "renderer/smTextureManager.h"
#include "resource/smResource.h"

#include "vendor/stbi/stb_image.h"

#undef SM_MODULE_NAME
#define SM_MODULE_NAME "TEXTURE RES."

void sm_texture_manager_unload_data(sm_texture_handler_s handler);

typedef struct sm__texture_s {
  SM_STRING name;
  u32 width, height;
  u32 channels;
  sm_pixel_format_e format;
  void *data;

  b8 has_gpu;
  u32 handle; /* OpenGL handle */

  atomic_int ref_count;

} sm_texture_s;

typedef struct sm__texture_manager_s {

  struct sm_handle_pool_s *handle_pool;
  SM_ARRAY(sm_texture_s) textures;

} sm_texture_manager_s;

/* Globals */
sm_texture_manager_s *TEXTURE_MANAGER = NULL;

b8 sm_texture_manager_init( )
{

  SM_ASSERT(TEXTURE_MANAGER == NULL && "TEXTURE_RESOURCE already initialized");

  TEXTURE_MANAGER = SM_CALLOC(1, sizeof(sm_texture_manager_s));
  SM_ASSERT(TEXTURE_MANAGER);

  struct sm_handle_pool_s *handle_pool = sm_handle_pool_new( );
  if (!sm_handle_pool_ctor(handle_pool, 16)) {
    SM_LOG_ERROR("failed to create texture handle pool");
    return false;
  }
  TEXTURE_MANAGER->handle_pool = handle_pool;

  SM_ARRAY_CTOR(TEXTURE_MANAGER->textures, 16);
  SM_ARRAY_SET_LEN(TEXTURE_MANAGER->textures, 16);
  memset(TEXTURE_MANAGER->textures, 0x0, sizeof(sm_texture_s) * 16);

  stbi_set_flip_vertically_on_load(true); /* tell stb_image.h to flip loaded texture's on the y-axis. */

  return true;
}

sm_texture_handler_s sm_texture_manager_new(SM_STRING file)
{

  SM_ASSERT(TEXTURE_MANAGER);

  sm_resource_s *resource = sm_resource_manager_get(file);
  if (!resource) {
    SM_LOG_WARN("[%s] resource not found", file);
    return (sm_texture_handler_s){SM_INVALID_HANDLE};
  }
  SM_ASSERT(SM_MASK_CHK(resource->type, SM_RESOURCE_TYPE_TEXTURE) && "resource is not a texture");

  if (resource->handle != SM_INVALID_HANDLE) {
    sm_texture_s *texture = &TEXTURE_MANAGER->textures[sm_handle_index(resource->handle)];
    atomic_fetch_add(&texture->ref_count, 1);
    return (sm_texture_handler_s){resource->handle};
  }

  if (sm_handle_full(TEXTURE_MANAGER->handle_pool)) {
    SM_LOG_ERROR("texture handle pool is full");
    return (sm_texture_handler_s){SM_INVALID_HANDLE};
  }

  u32 handle = sm_handle_new(TEXTURE_MANAGER->handle_pool);
  if (handle == SM_INVALID_HANDLE) {
    SM_LOG_ERROR("failed to create texture handle");
    return (sm_texture_handler_s){SM_INVALID_HANDLE};
  }
  /* TODO: mutex is required */
  resource->handle = handle;

  sm_texture_s *texture = &TEXTURE_MANAGER->textures[sm_handle_index(handle)];

  i32 width, height, channels, ok = 0;
  ok = stbi_info(file, &width, &height, &channels);
  if (!ok) {
    SM_LOG_ERROR("[%s] failed to load image from disk", file);
    sm_handle_del(TEXTURE_MANAGER->handle_pool, handle);
    return (sm_texture_handler_s){SM_INVALID_HANDLE};
  }
  SM_LOG_TRACE("[%s] (%dx%d:%d) image successfully loaded", file, width, height, channels);

  texture->name = SM_STRING_DUP(file);
  texture->width = (u32)width;
  texture->height = (u32)height;
  texture->channels = (u32)channels;
  texture->handle = 0;
  texture->data = NULL;
  atomic_store(&texture->ref_count, 1);

  if (channels == 1) texture->format = SM_PIXELFORMAT_UNCOMPRESSED_GRAYSCALE;
  else if (channels == 2) texture->format = SM_PIXELFORMAT_UNCOMPRESSED_GRAY_ALPHA;
  else if (channels == 3) texture->format = SM_PIXELFORMAT_UNCOMPRESSED_R8G8B8;
  else if (channels == 4) texture->format = SM_PIXELFORMAT_UNCOMPRESSED_R8G8B8A8;

  resource->handle = handle;

  return (sm_texture_handler_s){handle};
}

sm_texture_handler_s sm_texture_manager_new_from_mem(const void *data, u32 width, u32 height, sm_pixel_format_e format)
{

  u32 handle = sm_handle_new(TEXTURE_MANAGER->handle_pool);
  if (handle == SM_INVALID_HANDLE) {
    SM_LOG_ERROR("failed to create texture handle");
    return (sm_texture_handler_s){SM_INVALID_HANDLE};
  }

  sm_texture_s *texture = &TEXTURE_MANAGER->textures[sm_handle_index(handle)];

  texture->name = SM_STRING_DUP("default");
  texture->width = width;
  texture->height = height;
  texture->handle = 0;
  texture->format = format;
  SM_ASSERT(!texture->data);

  /* u32 size = sm_gl_get_pixel_data_size(width, height, format); */

  SM_ASSERT(data);
  texture->data = (void *)data;

  atomic_store(&texture->ref_count, 1);
  return (sm_texture_handler_s){handle};
}

b8 sm_texture_manager_load_data(sm_texture_handler_s handler)
{

  SM_ASSERT(TEXTURE_MANAGER);
  SM_ASSERT(sm_handle_valid(TEXTURE_MANAGER->handle_pool, handler.handle));

  u32 index = sm_handle_index(handler.handle);
  SM_ASSERT(index < SM_ARRAY_LEN(TEXTURE_MANAGER->textures));

  sm_texture_s *texture = &TEXTURE_MANAGER->textures[index];

  if (texture->data) {
    SM_LOG_WARN("[%s] texture already has cpu data", texture->name);
    return false;
  }

  i32 width, height, channels;
  u8 *data = stbi_load(texture->name, &width, &height, &channels, 0);
  if (data == NULL) {
    SM_LOG_ERROR("[%s] failed to load texture", texture->name);
    return false;
  }

  texture->width = (u32)width;
  texture->height = (u32)height;
  texture->channels = (u32)channels;

  if (channels == 1) texture->format = SM_PIXELFORMAT_UNCOMPRESSED_GRAYSCALE;
  else if (channels == 2) texture->format = SM_PIXELFORMAT_UNCOMPRESSED_GRAY_ALPHA;
  else if (channels == 3) texture->format = SM_PIXELFORMAT_UNCOMPRESSED_R8G8B8;
  else if (channels == 4) texture->format = SM_PIXELFORMAT_UNCOMPRESSED_R8G8B8A8;

  texture->data = data;

  SM_LOG_TRACE("[%s] texture successfully loaded from disk to main memory", texture->name);
  return true;
}

/*
 * Unload texture from RAM memory
 * but keep the handle in the handle pool
 */
void sm_texture_manager_unload_data(sm_texture_handler_s handler)
{

  SM_ASSERT(TEXTURE_MANAGER);
  SM_ASSERT(sm_handle_valid(TEXTURE_MANAGER->handle_pool, handler.handle));

  u32 index = sm_handle_index(handler.handle);
  SM_ASSERT(index < SM_ARRAY_LEN(TEXTURE_MANAGER->textures));

  sm_texture_s *texture = &TEXTURE_MANAGER->textures[index];
  SM_FREE(texture->data);
  texture->data = NULL;
}

/*
 * Unload texture from RAM
 * and remove the handle from the handle pool
 */
void sm_texture_manager_dtor(sm_texture_handler_s handler)
{

  SM_ASSERT(TEXTURE_MANAGER);
  SM_ASSERT(sm_handle_valid(TEXTURE_MANAGER->handle_pool, handler.handle));

  u32 index = sm_handle_index(handler.handle);
  SM_ASSERT(index < SM_ARRAY_LEN(TEXTURE_MANAGER->textures));

  sm_texture_s *texture = &TEXTURE_MANAGER->textures[index];

  /* Decrement reference count */
  atomic_fetch_sub(&texture->ref_count, 1);
  if (atomic_load(&texture->ref_count) > 0) return;

  SM_STRING_FREE(texture->name);
  texture->name = NULL;
  texture->channels = 0;
  texture->height = 0;
  texture->width = 0;

  sm_texture_manager_unload_data(handler);
  sm_handle_del(TEXTURE_MANAGER->handle_pool, handler.handle);
}

sm_texture_handler_s sm_texture_manager_reference(sm_texture_handler_s handler)
{

  SM_ASSERT(TEXTURE_MANAGER);
  SM_ASSERT(sm_handle_valid(TEXTURE_MANAGER->handle_pool, handler.handle));

  u32 index = sm_handle_index(handler.handle);
  SM_ASSERT(index < SM_ARRAY_LEN(TEXTURE_MANAGER->textures));

  sm_texture_s *texture = &TEXTURE_MANAGER->textures[index];
  atomic_fetch_add(&texture->ref_count, 1);
  return handler;
}

u32 sm_texture_manager_get_width(sm_texture_handler_s handler)
{

  SM_ASSERT(TEXTURE_MANAGER);
  SM_ASSERT(sm_handle_valid(TEXTURE_MANAGER->handle_pool, handler.handle));

  u32 index = sm_handle_index(handler.handle);
  SM_ASSERT(index < SM_ARRAY_LEN(TEXTURE_MANAGER->textures));

  sm_texture_s *texture = &TEXTURE_MANAGER->textures[index];
  return texture->width;
}

u32 sm_texture_manager_get_height(sm_texture_handler_s handler)
{

  SM_ASSERT(TEXTURE_MANAGER);
  SM_ASSERT(sm_handle_valid(TEXTURE_MANAGER->handle_pool, handler.handle));

  u32 index = sm_handle_index(handler.handle);
  SM_ASSERT(index < SM_ARRAY_LEN(TEXTURE_MANAGER->textures));

  sm_texture_s *texture = &TEXTURE_MANAGER->textures[index];
  return texture->height;
}

u32 sm_texture_manager_get_channels(sm_texture_handler_s handler)
{

  SM_ASSERT(TEXTURE_MANAGER);
  SM_ASSERT(sm_handle_valid(TEXTURE_MANAGER->handle_pool, handler.handle));

  u32 index = sm_handle_index(handler.handle);
  SM_ASSERT(index < SM_ARRAY_LEN(TEXTURE_MANAGER->textures));

  sm_texture_s *texture = &TEXTURE_MANAGER->textures[index];
  return texture->channels;
}

sm_pixel_format_e sm_texture_manager_get_pixel_format(sm_texture_handler_s handler)
{

  SM_ASSERT(TEXTURE_MANAGER);
  SM_ASSERT(sm_handle_valid(TEXTURE_MANAGER->handle_pool, handler.handle));

  u32 index = sm_handle_index(handler.handle);
  SM_ASSERT(index < SM_ARRAY_LEN(TEXTURE_MANAGER->textures));

  sm_texture_s *texture = &TEXTURE_MANAGER->textures[index];
  return texture->format;
}

SM_STRING sm_texture_manager_get_name(sm_texture_handler_s handler)
{

  SM_ASSERT(TEXTURE_MANAGER);
  SM_ASSERT(sm_handle_valid(TEXTURE_MANAGER->handle_pool, handler.handle));

  u32 index = sm_handle_index(handler.handle);
  SM_ASSERT(index < SM_ARRAY_LEN(TEXTURE_MANAGER->textures));

  sm_texture_s *texture = &TEXTURE_MANAGER->textures[index];
  return texture->name;
}

const void *sm_texture_manager_cpu_get_data(sm_texture_handler_s handler)
{

  SM_ASSERT(TEXTURE_MANAGER);
  SM_ASSERT(sm_handle_valid(TEXTURE_MANAGER->handle_pool, handler.handle));

  u32 index = sm_handle_index(handler.handle);
  SM_ASSERT(index < SM_ARRAY_LEN(TEXTURE_MANAGER->textures));

  sm_texture_s *texture = &TEXTURE_MANAGER->textures[index];
  return texture->data;
}

b8 sm_texture_manager_cpu_has_data(sm_texture_handler_s handler)
{

  return sm_texture_manager_cpu_get_data(handler) != NULL;
}

b8 sm_texture_manager_gpu_load_data(sm_texture_handler_s handler)
{

  SM_ASSERT(TEXTURE_MANAGER);
  SM_ASSERT(sm_handle_valid(TEXTURE_MANAGER->handle_pool, handler.handle));

  u32 index = sm_handle_index(handler.handle);
  SM_ASSERT(index < SM_ARRAY_LEN(TEXTURE_MANAGER->textures));

  sm_texture_s *texture = &TEXTURE_MANAGER->textures[index];

  if (texture->data == NULL) sm_texture_manager_load_data(handler);

  texture->handle = sm_gl_texture_ctor(handler);
  SM_ASSERT(texture->handle != 0);

  /* sm_texture_manager_unload_data(handler); */

  return true;
}

void sm_texture_manager_gpu_unload_data(sm_texture_handler_s handler)
{

  SM_ASSERT(TEXTURE_MANAGER);
  SM_ASSERT(sm_handle_valid(TEXTURE_MANAGER->handle_pool, handler.handle));

  u32 index = sm_handle_index(handler.handle);
  SM_ASSERT(index < SM_ARRAY_LEN(TEXTURE_MANAGER->textures));

  sm_texture_s *texture = &TEXTURE_MANAGER->textures[index];

  if (!texture->handle) SM_LOG_WARN("texture [%s] has no gpu data", texture->name);

  sm_gl_texture_dtor(texture->handle);
}

void sm_texture_manager_gpu_bind(sm_texture_handler_s handler, u32 tex_index)
{

  SM_ASSERT(TEXTURE_MANAGER);
  SM_ASSERT(sm_handle_valid(TEXTURE_MANAGER->handle_pool, handler.handle));

  u32 index = sm_handle_index(handler.handle);
  SM_ASSERT(index < SM_ARRAY_LEN(TEXTURE_MANAGER->textures));

  sm_texture_s *texture = &TEXTURE_MANAGER->textures[index];

  if (!texture->handle) SM_LOG_WARN("texture [%s] has no gpu data", texture->name);

  sm_gl_texture_bind(texture->handle, tex_index);
}

void sm_texture_manager_gpu_unbind(sm_texture_handler_s handler, u32 tex_index)
{

  SM_ASSERT(TEXTURE_MANAGER);
  SM_ASSERT(sm_handle_valid(TEXTURE_MANAGER->handle_pool, handler.handle));

  u32 index = sm_handle_index(handler.handle);
  SM_ASSERT(index < SM_ARRAY_LEN(TEXTURE_MANAGER->textures));

  sm_texture_s *texture = &TEXTURE_MANAGER->textures[index];

  if (!texture->handle) SM_LOG_WARN("texture [%s] has no gpu data", texture->name);

  sm_gl_texture_unbind(texture->handle, tex_index);
}

b8 sm_texture_manager_gpu_has_data(sm_texture_handler_s handler)
{

  SM_ASSERT(TEXTURE_MANAGER);
  SM_ASSERT(sm_handle_valid(TEXTURE_MANAGER->handle_pool, handler.handle));

  u32 index = sm_handle_index(handler.handle);
  SM_ASSERT(index < SM_ARRAY_LEN(TEXTURE_MANAGER->textures));

  sm_texture_s *texture = &TEXTURE_MANAGER->textures[index];

  return texture->handle != 0;
}

void sm_texture_manager_teardown(void)
{

  SM_ASSERT(TEXTURE_MANAGER);

  for (size_t i = 0; i < SM_ARRAY_LEN(TEXTURE_MANAGER->textures); ++i) {
    sm_texture_s *texture = &TEXTURE_MANAGER->textures[i];
    if (texture) {
      if (texture->name) SM_STRING_FREE(texture->name);
      if (texture->data) {
        SM_FREE(texture->data);
        texture->data = NULL;
      }
    }
  }

  SM_ARRAY_DTOR(TEXTURE_MANAGER->textures);
  sm_handle_pool_dtor(TEXTURE_MANAGER->handle_pool);
  SM_FREE(TEXTURE_MANAGER);
}

#undef SM_MODULE_NAME
