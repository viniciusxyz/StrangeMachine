#include "smpch.h"

#include "core/smAssert.h"
#include "core/smMem.h"

#include "core/util/smFilesystem.h"
#include "core/util/smString.h"

#include "core/data/smArray.h"

#undef SM_MODULE_NAME
#define SM_MODULE_NAME "UTIL_FILE"

b8 sm_filesystem_exists(SM_STRING file_path)
{

  return sm_filesystem_exists_c_str(file_path);
}

b8 sm_filesystem_exists_c_str(const char *file_path)
{

  SM_CORE_ASSERT(file_path);

#ifdef _MSC_VER
  struct _stat sb;
  return _stat(file, &sb) == 0 && S_ISREG(sb.st_mode);
#else
  struct stat sb;
  return stat(file_path, &sb) == 0 && S_ISREG(sb.st_mode);
#endif
}

b8 sm_filesystem_open(const SM_STRING file_path, file_modes_e mode, b8 binary, sm_file_handle_s *out_handle)
{

  SM_ASSERT(file_path);
  SM_ASSERT(out_handle);

  out_handle->is_valid = false;
  out_handle->handle = 0;
  const char *mode_str;

  if ((mode & SM_FILE_MODE_READ) != 0 && (mode & SM_FILE_MODE_WRITE) != 0) {
    mode_str = binary ? "w+b" : "w+";
  } else if ((mode & SM_FILE_MODE_READ) != 0 && (mode & SM_FILE_MODE_WRITE) == 0) {
    mode_str = binary ? "rb" : "r";
  } else if ((mode & SM_FILE_MODE_READ) == 0 && (mode & SM_FILE_MODE_WRITE) != 0) {
    mode_str = binary ? "wb" : "w";
  } else {
    SM_LOG_ERROR("[%s] invalid mode passed while trying to open file", file_path);
    return false;
  }

  FILE *file = fopen(file_path, mode_str);
  if (!file) {
    SM_LOG_ERROR("[%s] error opening file", file_path);
    return false;
  }

  out_handle->handle = file;
  out_handle->is_valid = true;

  return true;
}

b8 sm_filesystem_close(sm_file_handle_s *handle)
{

  SM_CORE_ASSERT(handle);
  SM_CORE_ASSERT(handle->handle);

  int re = fclose((FILE *)handle->handle);
  handle->handle = 0;
  handle->is_valid = false;

  return re == 0;
}

u64 sm_filesystem_size(const sm_file_handle_s *handle)
{

  SM_CORE_ASSERT(handle);

  if (handle->handle) {

    fseek((FILE *)handle->handle, 0, SEEK_END);
    u64 size = (u64)ftell((FILE *)handle->handle);

    rewind((FILE *)handle->handle);
    return size;
  }

  return 0;
}

SM_STRING sm_filesystem_read_all_text(const sm_file_handle_s *handle)
{

  SM_CORE_ASSERT(handle);
  SM_CORE_ASSERT(handle->handle);

  // File size
  u64 size = sm_filesystem_size(handle);
  if (!size) return NULL;

  SM_STRING string = SM_STRING_MALLOC(size + 1);

  u64 bytes_read = fread(string, sizeof(i8), size, (FILE *)handle->handle);

  if (bytes_read < size) string = SM_STRING_REALLOC(string, bytes_read + 1);

  string[bytes_read] = '\0';

  return string;
}

SM_STRING sm_filesystem_get_ext(const SM_STRING filename)
{

  const SM_STRING dot = strrchr(filename, '.');

  if (!dot || dot == filename) return NULL;

  return SM_STRING_DUP(dot + 1);
}

b8 sm_filesystem_has_ext(const SM_STRING filename, const SM_STRING suffix)
{

  SM_CORE_ASSERT(filename);
  SM_CORE_ASSERT(suffix);

  SM_STRING ext = sm_filesystem_get_ext(filename);

  if (!ext) return false;

  b8 result = false;
  SM_ARRAY(SM_STRING) split = strsplit(suffix, ';');
  for (size_t i = 0; i < SM_ARRAY_LEN(split); ++i) {

    if (!result && (strcmp(split[i], ext) == 0)) result = true;

    SM_STRING_FREE(split[i]);
  }

  SM_STRING_FREE(ext);
  SM_ARRAY_DTOR(split);

  return result;
}

b8 sm_filesystem_read_line(const sm_file_handle_s *handle, SM_STRING string_buf, i32 string_buf_size)
{

  SM_CORE_ASSERT(handle);

  return fgets(string_buf, string_buf_size, (FILE *)handle->handle);
}

b8 filesystem_write_line(sm_file_handle_s *handle, SM_STRING string)
{

  SM_CORE_ASSERT(handle);

  i32 result = fputs(string, (FILE *)handle->handle);

  if (result != EOF) result = fputc('\n', (FILE *)handle->handle);

  /* Make sure to flush the stream so it is written to the file immediately. */
  /* This prevents data loss in the event of a crash. */
  fflush((FILE *)handle->handle);

  return result != EOF;
}

b8 sm_filesystem_write_bytes(const sm_file_handle_s *handle, const void *data, u64 size)
{

  SM_CORE_ASSERT(handle);
  SM_CORE_ASSERT(data);

  u64 result = fwrite(data, 1, size, (FILE *)handle->handle);

  /* Make sure to flush the stream so it is written to the file immediately. */
  /* This prevents data loss in the event of a crash. */
  fflush((FILE *)handle->handle);

  return result != 0;
}

b8 sm_filesystem_read_bytes(const sm_file_handle_s *handle, void *data, u64 size)
{

  SM_CORE_ASSERT(handle);
  SM_CORE_ASSERT(data);

  u64 result = fread(data, 1, size, (FILE *)handle->handle);

  return result != 0;
}

#undef SM_MODULE_NAME
