#include "smConversorUtil.h"
#include <stdio.h>

void conversor_print_material_info(const struct aiScene *scene) {

  for (size_t i = 0; i < scene->mNumMaterials; ++i) {

    struct aiMaterial *mat = scene->mMaterials[i];

    struct aiString name;
    aiReturn ret = aiGetMaterialString(mat, AI_MATKEY_NAME, &name);
    /* if (ret == AI_SUCCESS) printf("Material name: %s\n", name.data); */

    printf("Material [%zu] %s\n", i, (ret == AI_SUCCESS) ? name.data : "no name");

    unsigned int count = 0;
    if ((count = aiGetMaterialTextureCount(mat, aiTextureType_DIFFUSE))) printf("\tDIFFUSE           %d\n", count);
    if ((count = aiGetMaterialTextureCount(mat, aiTextureType_DIFFUSE_ROUGHNESS))) printf("\tDIFFUSE_ROUGHNESS %d\n", count);
    if ((count = aiGetMaterialTextureCount(mat, aiTextureType_AMBIENT))) printf("\tAMBIENT           %d\n", count);
    if ((count = aiGetMaterialTextureCount(mat, aiTextureType_AMBIENT_OCCLUSION))) printf("\tAMBIENT_OCCLUSION %d\n", count);
    if ((count = aiGetMaterialTextureCount(mat, aiTextureType_BASE_COLOR))) printf("\tBASE_COLOR        %d\n", count);
    if ((count = aiGetMaterialTextureCount(mat, aiTextureType_EMISSION_COLOR))) printf("\tEMISSION_COLOR    %d\n", count);
    if ((count = aiGetMaterialTextureCount(mat, aiTextureType_CLEARCOAT))) printf("\tCLEARCOAT         %d\n", count);
    if ((count = aiGetMaterialTextureCount(mat, aiTextureType_DISPLACEMENT))) printf("\tDISPLACEMENT      %d\n", count);
    if ((count = aiGetMaterialTextureCount(mat, aiTextureType_EMISSIVE))) printf("\tEMISSIVE          %d\n", count);
    if ((count = aiGetMaterialTextureCount(mat, aiTextureType_HEIGHT))) printf("\tHEIGHT            %d\n", count);
    if ((count = aiGetMaterialTextureCount(mat, aiTextureType_LIGHTMAP))) printf("\tLIGHTMAP          %d\n", count);
    if ((count = aiGetMaterialTextureCount(mat, aiTextureType_METALNESS))) printf("\tMETALNESS         %d\n", count);
    if ((count = aiGetMaterialTextureCount(mat, aiTextureType_NONE))) printf("\tNONE              %d\n", count);
    if ((count = aiGetMaterialTextureCount(mat, aiTextureType_NORMAL_CAMERA))) printf("\tNORMAL_CAMERA     %d\n", count);
    if ((count = aiGetMaterialTextureCount(mat, aiTextureType_NORMALS))) printf("\tNORMALS           %d\n", count);
    if ((count = aiGetMaterialTextureCount(mat, aiTextureType_OPACITY))) printf("\tOPACITY           %d\n", count);
    if ((count = aiGetMaterialTextureCount(mat, aiTextureType_REFLECTION))) printf("\tREFLECTION        %d\n", count);
    if ((count = aiGetMaterialTextureCount(mat, aiTextureType_SHEEN))) printf("\tSHEEN             %d\n", count);
    if ((count = aiGetMaterialTextureCount(mat, aiTextureType_SHININESS))) printf("\tSHININESS         %d\n", count);
    if ((count = aiGetMaterialTextureCount(mat, aiTextureType_SPECULAR))) printf("\tSPECULAR          %d\n", count);
    if ((count = aiGetMaterialTextureCount(mat, aiTextureType_TRANSMISSION))) printf("\tTRANSMISSION      %d\n", count);
    if ((count = aiGetMaterialTextureCount(mat, aiTextureType_UNKNOWN))) printf("\tUNKNOWN           %d\n", count);

    struct aiColor4D color;
    if (aiGetMaterialColor(mat, AI_MATKEY_COLOR_AMBIENT, &color) == AI_SUCCESS)
      printf("\tCOLOR_AMBIENT     (%f, %f, %f, %f)\n", color.r, color.g, color.b, color.a);
    if (aiGetMaterialColor(mat, AI_MATKEY_COLOR_DIFFUSE, &color) == AI_SUCCESS)
      printf("\tCOLOR_DIFFUSE     (%f, %f, %f, %f)\n", color.r, color.g, color.b, color.a);
    if (aiGetMaterialColor(mat, AI_MATKEY_COLOR_EMISSIVE, &color) == AI_SUCCESS)
      printf("\tCOLOR_EMISSIVE    (%f, %f, %f, %f)\n", color.r, color.g, color.b, color.a);
    if (aiGetMaterialColor(mat, AI_MATKEY_COLOR_REFLECTIVE, &color) == AI_SUCCESS)
      printf("\tCOLOR_REFLECTIVE  (%f, %f, %f, %f)\n", color.r, color.g, color.b, color.a);
    if (aiGetMaterialColor(mat, AI_MATKEY_COLOR_SPECULAR, &color) == AI_SUCCESS)
      printf("\tCOLOR_SPECULAR    (%f, %f, %f, %f)\n", color.r, color.g, color.b, color.a);
    if (aiGetMaterialColor(mat, AI_MATKEY_COLOR_TRANSPARENT, &color) == AI_SUCCESS)
      printf("\tCOLOR_TRANSPARENT (%f, %f, %f, %f)\n", color.r, color.g, color.b, color.a);

    int property = 0;
    if (aiGetMaterialInteger(mat, AI_MATKEY_TWOSIDED, &property) == AI_SUCCESS)
      printf("\tTWOSIDED          %s\n", property ? "true" : "false");
    if (aiGetMaterialInteger(mat, AI_MATKEY_BLEND_FUNC, &property) == AI_SUCCESS)
      printf("\tBLEND_FUNC        %s\n", property ? "true" : "false");

    float property2 = 0.0f;
    if (aiGetMaterialFloat(mat, AI_MATKEY_SHININESS, &property2) == AI_SUCCESS) printf("\tSHININESS         %f\n", property2);
    if (aiGetMaterialFloat(mat, AI_MATKEY_SHININESS_STRENGTH, &property2) == AI_SUCCESS)
      printf("\tSHININESS_STRENG  %f\n", property2);
    if (aiGetMaterialFloat(mat, AI_MATKEY_OPACITY, &property2) == AI_SUCCESS) printf("\tOPACITY           %f\n", property2);
  }
}
