#ifndef SM_ECS_H
#define SM_ECS_H

#include "smpch.h"

#include "ecs/smComponents.h"
#include "ecs/smEntity.h"
#include "ecs/smSystem.h"

/* ECS allocates memory in "chuncks". A chunck always contains entities of a single archetype */
typedef struct sm__chunk_s {

  struct sm_handle_pool_s *pool;

  b8 alligned;
  size_t length; /* number of components in the pool */
  size_t size;   /* total size in bytes  */

  SM_ARRAY(sm_component_view_s) view;

  void *data;

} sm_chunk_s;

typedef struct sm__ecs_s {

  /* All registered components in the scene */
  const sm_component_t registered_components;

  sm_hashmap_u64_m *map_archetype;
  SM_ARRAY(sm_system_s) systems;

} sm_ecs_s;

sm_ecs_s *sm_ecs_new(void);

b8 sm_ecs_ctor(sm_ecs_s *ecs, sm_component_t comp);
void sm_ecs_dtor(sm_ecs_s *ecs);

sm_entity_s sm_ecs_entity_new(sm_ecs_s *ecs, sm_component_t archetype);
void sm_ecs_component_set_data(sm_ecs_s *ecs, sm_entity_s entity, sm_component_t components, void *data);
void sm_ecs_component_set_all_data(sm_ecs_s *ecs, sm_entity_s entity, void *data);
const void *sm_ecs_component_get_data(sm_ecs_s *ecs, sm_entity_s entity, sm_component_t component);
void sm_ecs_system_register(sm_ecs_s *ecs, sm_component_t comp, system_f system, u32 flags);

void sm_ecs_do(sm_ecs_s *scene, f32 dt);

/* Utility functions */
SM_STRING sm_ecs_entity_to_string(sm_component_t components);

#endif /* SM_ECS_H */
