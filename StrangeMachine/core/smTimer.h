#ifndef SM_CORE_TIMER_H
#define SM_CORE_TIMER_H

struct sm_timer_s;

struct sm_timer_s *sm_timer_new(void);

void sm_timer_ctor(struct sm_timer_s *t);
void sm_timer_dtor(struct sm_timer_s *t);
void sm_timer_reset(struct sm_timer_s *t);
double sm_timer_get_elapsed(struct sm_timer_s *t);

#endif /* SM_CORE_TIMER_H */
