#ifndef SM_MATERIAL_MANAGER_H
#define SM_MATERIAL_MANAGER_H

#include "smpch.h"

#include "core/smCore.h"

#include "math/smMath.h"

#include "resource/smResource.h"

#include "renderer/smMaterialManagerPub.h"
#include "renderer/smShaderManager.h"
#include "renderer/smTextureManager.h"

typedef struct sm__material_handler_s {
  u32 handle;

} sm_material_handler_s;

b8 sm_material_manager_init(void);
void sm_material_manager_teardown(void);

sm_material_handler_s sm_material_manager_new(u32 uuid);

void sm_material_manager_set_name(sm_material_handler_s handler, SM_STRING name);
void sm_material_manager_set_diffuse_color(sm_material_handler_s handler, sm_vec4 diffuse_color);
void sm_material_manager_set_diffuse_map(sm_material_handler_s handler, sm_texture_handler_s diffuse_map);
void sm_material_manager_set_shader(sm_material_handler_s handler, sm_shader_handler_s shader);

SM_STRING sm_material_manager_get_name(sm_material_handler_s handler);
u32 sm_material_manager_get_uuid(sm_material_handler_s handler);
sm_vec4 sm_material_manager_get_diffuse_color(sm_material_handler_s handler);
sm_texture_handler_s sm_material_manager_get_diffuse_map(sm_material_handler_s handler);
sm_shader_handler_s sm_material_manager_get_shader(sm_material_handler_s handler);

b8 sm_material_manager_write(sm_material_s *material);

#endif /* SM_MATERIAL_MANAGER_H */
