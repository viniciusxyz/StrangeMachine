#include "smpch.h"

#include "core/smCore.h"

#include "scene/smScene.h"

#include "renderer/smTextureManager.h"

#include "resource/file_format/smSMR.h"

#undef SM_MODULE_NAME
#define SM_MODULE_NAME "SCENE"

typedef struct sm__node_s {

  SM_STRING name;
  SM_ARRAY(sm_entity_s) entities;
  sm_transform_s local_transform;

  u32 parent;
  SM_ARRAY(u32) children;

} sm_node_s;

static inline sm_node_s sm_node_new_invalid(void)
{

  sm_node_s node = {
      .name = NULL,
      .entities = NULL,
      .local_transform = sm_transform_identity( ),
      .parent = INVALID_NODE,
      .children = NULL,
  };

  return node;
}

typedef struct sm__scene_s {

  sm_entity_s current_camera;
  SM_ARRAY(sm_node_s) nodes;

} sm_scene_s;

sm_scene_s *sm_scene_new(void)
{
  sm_scene_s *scene = SM_CALLOC(1, sizeof(sm_scene_s));
  SM_ASSERT(scene);

  return scene;
}

b8 sm_scene_ctor(sm_scene_s *scene)
{

  SM_ASSERT(scene);

  return true;
}

void sm_scene_dtor(sm_scene_s *scene)
{

  SM_ASSERT(scene);

  for (size_t i = 0; i < SM_ARRAY_LEN(scene->nodes); ++i) {
    if (scene->nodes[i].name) SM_STRING_FREE(scene->nodes[i].name);
    SM_ARRAY_DTOR(scene->nodes[i].children);
    SM_ARRAY_DTOR(scene->nodes[i].entities);
  }
  SM_ARRAY_DTOR(scene->nodes);

  SM_FREE(scene);
}

void sm_scene_do(sm_scene_s *scene, f32 dt)
{

  SM_UNUSED(dt);

  SM_ASSERT(scene);
}

struct sm__save_data {
  sm_ecs_s *ecs;
  sm_file_handle_s *file;
  SM_STRING file_path;
};

void sm_scene_set_parent(sm_scene_s *scene, u32 node_index, u32 node_parent)
{
  SM_ASSERT(scene);
  SM_ASSERT(node_index < SM_ARRAY_LEN(scene->nodes));

  scene->nodes[node_index].parent = node_parent;
}

u32 sm_scene_get_parent(sm_scene_s *scene, u32 node_index)
{
  SM_ASSERT(scene);
  SM_ASSERT(node_index < SM_ARRAY_LEN(scene->nodes));

  return scene->nodes[node_index].parent;
}

void sm_scene_set_name(sm_scene_s *scene, u32 node_index, const SM_STRING name)
{
  SM_ASSERT(scene);
  SM_ASSERT(node_index < SM_ARRAY_LEN(scene->nodes));

  if (scene->nodes[node_index].name) {
    SM_STRING_FREE(scene->nodes[node_index].name);
  }

  scene->nodes[node_index].name = SM_STRING_DUP(name);
}

void sm_scene_set_local_transform(sm_scene_s *scene, u32 node_index, sm_transform_s local_transform)
{
  SM_ASSERT(scene);
  SM_ASSERT(node_index < SM_ARRAY_LEN(scene->nodes));

  scene->nodes[node_index].local_transform = local_transform;
}

const SM_STRING sm_scene_get_name(sm_scene_s *scene, u32 node_index)
{
  SM_ASSERT(scene);

  if (node_index >= SM_ARRAY_LEN(scene->nodes)) {
    SM_LOG_ERROR("index %d out of bounds", node_index);
    return NULL;
  }

  return scene->nodes[node_index].name;
}

void sm_scene_set_current_camera(sm_scene_s *scene, sm_entity_s handle)
{

  SM_ASSERT(scene);

  if (!SM_MASK_CHK(handle.archetype_index, SM_CAMERA_COMP)) {
    SM_LOG_ERROR("entity has no SM_CAMERA_COMP component");
    SM_UNREACHABLE( );
  }

  scene->current_camera = handle;
}

sm_entity_s sm_scene_get_current_camera(sm_scene_s *scene)
{

  SM_ASSERT(scene);

  if (scene->current_camera.handle == 0 || scene->current_camera.archetype_index == 0) {
    SM_LOG_ERROR("scene has no camera set");
    SM_UNREACHABLE( );
  }

  return scene->current_camera;
}

b8 sm_scene_has_parent(sm_scene_s *scene, u32 node_index)
{
  SM_ASSERT(scene);
  SM_ASSERT(node_index < SM_ARRAY_LEN(scene->nodes));

  return scene->nodes[node_index].parent != INVALID_NODE;
}

void sm_scene_remove_child(sm_scene_s *scene, u32 node_index, u32 node_child)
{
  SM_ASSERT(scene);
  SM_ASSERT(node_index < SM_ARRAY_LEN(scene->nodes));

  for (size_t i = 0; i < SM_ARRAY_LEN(scene->nodes[node_index].children); i++) {
    if (scene->nodes[node_index].children[i] == node_child) {
      SM_ARRAY_DEL(scene->nodes[node_index].children, i, 1);
      return;
    }
  }
}

u32 sm_scene_add_child(sm_scene_s *scene, u32 node_index, u32 node_child)
{

  SM_ASSERT(scene);
  SM_ASSERT(node_index < SM_ARRAY_LEN(scene->nodes));

  if (node_child == ROOT) {
    SM_LOG_ERROR("cannot add root as child");
    return INVALID_NODE;
  }

  if (node_child == node_index) {
    SM_LOG_ERROR("cannot add self as child");
    return INVALID_NODE;
  }

  for (size_t i = 0; i < SM_ARRAY_LEN(scene->nodes[node_child].children); ++i) {
    if (scene->nodes[node_child].children[i] == node_index) {
      SM_LOG_ERROR("cyclic dependency of %d and %d", node_index, node_child);
      return INVALID_NODE;
    }
  }

  u32 parent = scene->nodes[node_index].parent;
  while (parent != ROOT && parent != NO_PARENT && parent != INVALID_NODE) {
    if (node_child == parent) {
      SM_LOG_ERROR("cyclic dependency of %d and %d", node_index, node_child);
      return INVALID_NODE;
    }
    parent = scene->nodes[parent].parent;
  }

  if (scene->nodes[node_index].parent == INVALID_NODE) {
    SM_LOG_WARN("node %d has no parent", node_index);
    sm_scene_add_child(scene, ROOT, node_index);
  }

  if (sm_scene_has_parent(scene, node_child)) {
    sm_scene_remove_child(scene, scene->nodes[node_child].parent, node_child);
  }

  SM_ARRAY_PUSH(scene->nodes[node_index].children, node_child);
  sm_scene_set_parent(scene, node_child, node_index);

  return node_child;
}

u32 sm_scene_new_node(sm_scene_s *scene)
{
  SM_ASSERT(scene);

  for (size_t i = 0; i < SM_ARRAY_LEN(scene->nodes); i++) {
    if (scene->nodes[i].parent == INVALID_NODE) {
      return (u32)i;
    }
  }

  /* if we get here, there are no invalid nodes */
  sm_node_s n = sm_node_new_invalid( );
  SM_ARRAY_PUSH(scene->nodes, n);
  return (u32)SM_ARRAY_LEN(scene->nodes) - 1;
}

b8 sm_scene_push_entity(sm_scene_s *scene, u32 node_index, sm_entity_s entity)
{
  SM_ASSERT(scene);

  if (node_index >= SM_ARRAY_LEN(scene->nodes)) {
    SM_LOG_ERROR("index %d out of bounds", node_index);
    return false;
  }

  if (scene->nodes[node_index].parent == INVALID_NODE) {
    SM_LOG_ERROR("node %d has no parent", node_index);
    return false;
  }

  SM_ARRAY_PUSH(scene->nodes[node_index].entities, entity);

  return true;
}

u32 sm_scene_get_by_name(sm_scene_s *scene, const SM_STRING name)
{

  SM_ASSERT(scene);
  if (!name) return INVALID_NODE;

  for (size_t i = 0; i < SM_ARRAY_LEN(scene->nodes); i++) {
    if (scene->nodes[i].name && strcmp(scene->nodes[i].name, name)) {
      return (u32)i;
    }
  }

  return INVALID_NODE;
}

u32 sm_scene_get_by_entity(sm_scene_s *scene, sm_entity_s entity)
{
  SM_ASSERT(scene);

  for (size_t i = 0; i < SM_ARRAY_LEN(scene->nodes); i++) {
    sm_entity_s *etts = scene->nodes[i].entities;

    for (size_t j = 0; j < SM_ARRAY_LEN(etts); ++j) {

      sm_entity_s e = etts[j];
      if (e.handle == entity.handle && e.archetype_index == entity.archetype_index) return (u32)i;
    }
  }

  return INVALID_NODE;
}

u32 sm_scene_get_root(sm_scene_s *scene)
{

  SM_ASSERT(scene);

  if (!SM_ARRAY_LEN(scene->nodes)) {

    SM_ARRAY_SET_LEN(scene->nodes, 1);
    scene->nodes[0] = sm_node_new_invalid( );
    /* set the first node as the root node */
    scene->nodes[0].parent = NO_PARENT;
    scene->nodes[0].name = SM_STRING_DUP("ROOT");
  }

  return ROOT;
}

sm_entity_s *sm_scene_get_entity(sm_scene_s *scene, u32 node_index)
{
  SM_ASSERT(scene);

  if (node_index >= SM_ARRAY_LEN(scene->nodes)) {
    SM_LOG_ERROR("index %d out of bounds", node_index);
    return NULL;
  }

  return scene->nodes[node_index].entities;
}

void sm_scene_for_each(sm_scene_s *scene, u32 node_index, void (*callback)(sm_scene_s *scene, u32 node_index, void *user_data),
                       void *user_data)
{

  SM_ASSERT(scene);
  SM_ASSERT(node_index < SM_ARRAY_LEN(scene->nodes));

  /* static int depth = -1; */
  /* depth++; */

  /* for (int i = 0; i < depth; i++) { */
  /*   printf("| "); */
  /* } */
  callback(scene, node_index, user_data);

  for (u32 i = 0; i < SM_ARRAY_LEN(scene->nodes[node_index].children); i++) {
    sm_scene_for_each(scene, scene->nodes[node_index].children[i], callback, user_data);
  }
  /* depth--; */
}

void sm__scene_write_cb(sm_scene_s *scene, u32 node_index, void *user_data)
{

  struct sm__save_data *data = (struct sm__save_data *)user_data;
  sm_entity_s *entities = sm_scene_get_entity(scene, node_index);

  /* Writing index */
  if (!sm_filesystem_write_bytes(data->file, &node_index, sizeof(node_index))) {
    SM_LOG_ERROR("failed to write index");
    return;
  }

  /* Writing parent */
  u32 parent = sm_scene_get_parent(scene, node_index);
  if (!sm_filesystem_write_bytes(data->file, &parent, sizeof(u32))) {
    SM_LOG_ERROR("failed to write parent");
    return;
  }

  /* Writing the name length */
  const SM_STRING name = sm_scene_get_name(scene, node_index);
  size_t name_len = strlen(name);
  if (!sm_filesystem_write_bytes(data->file, &name_len, sizeof(size_t))) {
    SM_LOG_ERROR("failed to write name len");
    return;
  }

  /* Writing the name */
  if (!sm_filesystem_write_bytes(data->file, name, name_len)) {
    SM_LOG_ERROR("failed to write name");
    return;
  }

  /* Writing the local transformation */
  sm_transform_s local_transform = sm_scene_get_local_transform(scene, node_index);
  if (!sm_filesystem_write_bytes(data->file, &local_transform, sizeof(sm_transform_s))) {
    SM_LOG_ERROR("failed to write local transform");
    return;
  }

  /* Writing the entities length */
  size_t entities_len = SM_ARRAY_LEN(entities);
  if (!sm_filesystem_write_bytes(data->file, &entities_len, sizeof(size_t))) {
    SM_LOG_ERROR("failed to write entities len");
    return;
  }

  /* Writing the entities */
  for (size_t i = 0; i < entities_len; ++i) {

    sm_entity_s entity = entities[i];

    sm_chunk_s *chunk = sm_hashmap_get_u64(data->ecs->map_archetype, entity.archetype_index);
    if (chunk == NULL) {
      SM_LOG_WARN("no chunk for archetype %lu", entity.archetype_index);
      return;
    }

    SM_ASSERT(chunk->pool);

    u32 handle_idx = sm_handle_index(entity.handle);
    SM_ASSERT(handle_idx < chunk->length);
    /* sm_string str = sm_ecs_entity_to_string(entity.archetype_index); */
    /* sm_string_dtor(str); */

    /* Writing archtype */
    if (!sm_filesystem_write_bytes(data->file, &entity.archetype_index, sizeof(sm_component_t))) {
      SM_LOG_ERROR("failed to write archetype index");
      return;
    }

    for (u32 v = 0; v < SM_ARRAY_LEN(chunk->view); ++v) {
      sm_component_view_s *view = &chunk->view[v];
      void *chdata = (u8 *)chunk->data + (handle_idx * chunk->size) + view->offset;
      if (view->write) view->write(data->file, chdata);

      /* if (SM_MASK_CHK_EQ(view->id, SM_MATERIAL_COMP)) { */
      /*   sm_material_s *material = chdata; */
      /*   if (!sm_scene_material_save(data->file_path, material)) SM_LOG_ERROR("failed to save material file"); */
      /* } */
    }
  }
}

sm_transform_s sm_scene_get_global_transform(sm_scene_s *scene, u32 node_index)
{

  SM_ASSERT(scene);

  sm_transform_s result = sm_scene_get_local_transform(scene, node_index);

  for (u32 p = sm_scene_get_parent(scene, node_index); p > 0; p = sm_scene_get_parent(scene, p)) {

    result = sm_transform_combine(sm_scene_get_local_transform(scene, p), result);
  }

  return result;
}

sm_transform_s sm_scene_get_local_transform(sm_scene_s *scene, u32 node_index)
{
  SM_ASSERT(scene);

  return scene->nodes[node_index].local_transform;
}

b8 sm_scene_save(sm_scene_s *scene, SM_STRING path, sm_ecs_s *ecs)
{

  SM_ASSERT(scene);

  sm_file_handle_s fhandle;
  smr_file_header_s header = smr_file_header_new(SMR_RESOURCE_SCENE, 0);

  if (!sm_filesystem_open(path, SM_FILE_MODE_WRITE, true, &fhandle)) {
    SM_LOG_ERROR("[%s] failed to open/create file", path);
    return false;
  }

  if (!smr_write_header(&header, &fhandle)) {
    SM_LOG_ERROR("[%s] failed to write header", path);
    return false;
  }
  /*
   * +--------------+
   * | String       |
   * +--------------+
   * size_t string_length
   * char* string
   */

  /*
   * +--------------+
   * | Entity        |
   * +--------------+
   * u64 archetype
   * void* data
   */

  /*
   * +--------------+
   * | Node         |
   * +--------------+
   * u32 index;
   * u32 parent;
   *
   * String name;
   * sm_transform_s local_transform;
   * size_t entities_lenght
   * ARRAY(Entity) entities;
   */

  /*
   * +------------+
   * | header     |
   * +------------+
   *  size_t nodes_length
   *  ARRAY(Node) nodes
   */

  size_t nodes_len = SM_ARRAY_LEN(scene->nodes);
  if (!sm_filesystem_write_bytes(&fhandle, &nodes_len, sizeof(size_t))) {
    SM_LOG_ERROR("failed to write node length");
    return false;
  }

  SM_STRING path_alloc = SM_STRING_DUP(path);
  SM_STRING dir = SM_MALLOC(strlen(dirname(path_alloc)) + 2);
  strcat(dir, "/");

  struct sm__save_data data = {ecs, &fhandle, dir};
  sm_scene_for_each(scene, 0, sm__scene_write_cb, &data);

  SM_STRING_FREE(dir);

  sm_filesystem_close(&fhandle);

  return true;
}

sm_scene_s *sm_scene_open(const SM_STRING file_path, sm_ecs_s *ecs)
{

  sm_scene_s *scene = sm_scene_new( );
  if (!sm_scene_ctor(scene)) {
    SM_LOG_ERROR("failed to create scene");
    return NULL;
  }

  sm_file_handle_s fhandle;
  if (!sm_filesystem_open(file_path, SM_FILE_MODE_READ, true, &fhandle)) {
    SM_LOG_ERROR("[%s] failed to open file", file_path);
    return NULL;
  }

  smr_file_header_s header;
  if (!smr_read_header(&header, &fhandle)) {
    SM_LOG_ERROR("[%s] failed to read header", file_path);
    return NULL;
  }

  size_t nodes_len = 0;
  if (!sm_filesystem_read_bytes(&fhandle, &nodes_len, sizeof(size_t))) {
    SM_LOG_ERROR("failed to read parent");
    return NULL;
  }

  SM_ARRAY_SET_LEN(scene->nodes, nodes_len);

  for (size_t i = 0; i < nodes_len; ++i) {

    scene->nodes[i] = sm_node_new_invalid( );

    u32 index = 0;
    if (!sm_filesystem_read_bytes(&fhandle, &index, sizeof(u32))) {

      /* if ((feof((FILE *)fhandle.handle))) { */
      /*   break; */
      /* } */

      SM_LOG_ERROR("failed to read index");
      return NULL;
    }

    u32 parent = 0;
    if (!sm_filesystem_read_bytes(&fhandle, &parent, sizeof(u32))) {
      SM_LOG_ERROR("failed to read parent");
      return NULL;
    }

    if (parent != NO_PARENT) sm_scene_add_child(scene, parent, index);
    else sm_scene_set_parent(scene, index, NO_PARENT);

    size_t name_len = 0;
    if (!sm_filesystem_read_bytes(&fhandle, &name_len, sizeof(size_t))) {
      SM_LOG_ERROR("failed to read name len");
      return NULL;
    }

    char buf[name_len + 1];
    if (!sm_filesystem_read_bytes(&fhandle, buf, name_len)) {
      SM_LOG_ERROR(" failed to read the string");
      return NULL;
    }
    buf[name_len] = '\0';
    sm_scene_set_name(scene, index, buf);

    sm_transform_s local_transform;
    if (!sm_filesystem_read_bytes(&fhandle, &local_transform, sizeof(sm_transform_s))) {
      SM_LOG_ERROR("failed to read local transform");
      return NULL;
    }
    sm_scene_set_local_transform(scene, index, local_transform);

    size_t entities_len = 0;
    if (!sm_filesystem_read_bytes(&fhandle, &entities_len, sizeof(size_t))) {
      SM_LOG_ERROR("failed to read local transform");
      return NULL;
    }

    for (size_t j = 0; j < entities_len; ++j) {

      sm_component_t archetype = 0;
      if (!sm_filesystem_read_bytes(&fhandle, &archetype, sizeof(sm_component_t))) {
        SM_LOG_ERROR("failed to read archetype index");
        return NULL;
      }

      sm_entity_s entity = sm_ecs_entity_new(ecs, archetype);
      if (entity.handle == SM_INVALID_HANDLE) {
        SM_LOG_ERROR("failed to create entity");
        return NULL;
      }
      sm_scene_push_entity(scene, index, entity);

      sm_chunk_s *chunk = sm_hashmap_get_u64(ecs->map_archetype, entity.archetype_index);
      if (chunk == NULL) {
        SM_LOG_WARN("no chunk for archetype %lu", entity.archetype_index);
        return NULL;
      }

      u32 handle_idx = sm_handle_index(entity.handle);

      for (u32 k = 0; k < SM_ARRAY_LEN(chunk->view); ++k) {
        sm_component_view_s *v = &chunk->view[k];
        if (v->read) {
          void *chdata = (u8 *)chunk->data + (handle_idx * chunk->size) + v->offset;
          memset(chdata, 0x0, v->size);
          if (!v->read(&fhandle, chdata)) SM_LOG_ERROR("Error while reading: %s", buf);
        }
      }
    }
  }

  sm_filesystem_close(&fhandle);

  return scene;
}

#undef SM_MODULE_NAME
