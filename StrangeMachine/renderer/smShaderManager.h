#ifndef SM_RENDERER_SHADER_RESOURCE_H
#define SM_RENDERER_SHADER_RESOURCE_H

#include "smpch.h"

#include "renderer/GL/smGL.h"
#include "resource/smShaderResource.h"

b8 sm_shader_manager_init(void);
void sm_shader_manager_teardown(void);

typedef struct sm__shader_manager_handler_s {
  u32 handle;
} sm_shader_handler_s;

sm_shader_handler_s sm_shader_manager_new(sm_shader_resource_handler_s shader_resource);
void sm_shader_manager_dtor(sm_shader_handler_s handler);
void sm_shader_manager_bind(sm_shader_handler_s handler);
sm_shader_handler_s sm_shader_manager_reference(sm_shader_handler_s handler);
void sm_shader_manager_unbind( );
i32 sm_shader_manager_get_uniform_location(sm_shader_handler_s handler, char *uniform);
void sm_shader_manager_set_uniform(sm_shader_handler_s handler, SM_STRING name, sm_type_e type, void *value);
void sm_shader_manager_set_uniform_location(i32 location, sm_type_e type, void *value);
i32 sm_shader_manager_get_attribute_location(sm_shader_handler_s handler, char *attribute);

#endif /* SM_RENDERER_SHADER_RESOURCE_H */
