#ifndef SM_CIMGUI_IMPL_H
#define SM_CIMGUI_IMPL_H

#define CIMGUI_DEFINE_ENUMS_AND_STRUCTS
#include "vendor/cimgui/cimgui.h"
#define CIMGUI_USE_SDL
#define CIMGUI_USE_OPENGL2
#include "vendor/cimgui/generator/output/cimgui_impl.h"

#ifdef IMGUI_HAS_IMSTR
  #define igBegin       igBegin_Str
  #define igSliderFloat igSliderFloat_Str
  #define igCheckbox    igCheckbox_Str
  #define igColorEdit3  igColorEdit3_Str
  #define igButton      igButton_Str
#endif

#endif /* SM_CIMGUI_IMPL_H */
