#ifndef SM_SCENE_H
#define SM_SCENE_H

#include "smpch.h"

#include "ecs/smECS.h"

typedef struct sm__scene_s sm_scene_s;

#define ROOT         0u          /* 0 */
#define INVALID_NODE 0xFFFFFFFFu /* -1 */
#define NO_PARENT    0xFFFFFFFEu /* -2 */

sm_scene_s *sm_scene_new(void);
b8 sm_scene_ctor(sm_scene_s *scene);
void sm_scene_dtor(sm_scene_s *scene);

void sm_scene_do(sm_scene_s *scene, f32 dt);

void sm_scene_set_parent(sm_scene_s *scene, u32 node_index, u32 parent);
u32 sm_scene_get_parent(sm_scene_s *scene, u32 node_index);
void sm_scene_set_name(sm_scene_s *scene, u32 node_index, const SM_STRING name);
void sm_scene_set_local_transform(sm_scene_s *scene, u32 node_index, sm_transform_s local_transform);
b8 sm_scene_has_parent(sm_scene_s *scene, u32 node_index);
void sm_scene_remove_child(sm_scene_s *scene, u32 node_index, u32 node_child);
u32 sm_scene_add_child(sm_scene_s *scene, u32 node_index, u32 node_child);
u32 sm_scene_new_node(sm_scene_s *scene);
b8 sm_scene_push_entity(sm_scene_s *scene, u32 node_index, sm_entity_s entity);
u32 sm_scene_get_by_name(sm_scene_s *scene, const SM_STRING name);
u32 sm_scene_get_by_entity(sm_scene_s *scene, sm_entity_s entity);
u32 sm_scene_get_root(sm_scene_s *scene);
sm_entity_s *sm_scene_get_entity(sm_scene_s *scene, u32 node_index);
void sm_scene_for_each(sm_scene_s *scene, u32 node_index, void (*callback)(sm_scene_s *scene, u32 node_index, void *user_data),
                       void *user_data);
const SM_STRING sm_scene_get_name(sm_scene_s *scene, u32 node_index);
void sm_scene_set_current_camera(sm_scene_s *scene, sm_entity_s entity);
sm_entity_s sm_scene_get_current_camera(sm_scene_s *scene);

/* b8 sm_scene_material_save(sm_string dir, const sm_material_s *material); */
/* b8 sm_scene_material_open(sm_string dir, sm_material_s *material); */

b8 sm_scene_save(sm_scene_s *scene, SM_STRING path, sm_ecs_s *ecs);
sm_scene_s *sm_scene_open(const SM_STRING path, sm_ecs_s *ecs);

sm_transform_s sm_scene_get_local_transform(sm_scene_s *scene, u32 node_index);
sm_transform_s sm_scene_get_global_transform(sm_scene_s *scene, u32 node_index);

#endif /* SM_SCENE_H */
