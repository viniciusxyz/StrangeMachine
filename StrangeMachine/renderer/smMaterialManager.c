#include "smpch.h"

#include "core/smCore.h"

#include "resource/file_format/smSMR.h"
#include "resource/smResource.h"

#include "renderer/smMaterialManager.h"
#include "renderer/smMaterialManagerPub.h"
#include "renderer/smShaderManager.h"
#include "renderer/smTextureManager.h"

typedef struct sm__material_manager_s {

  struct sm_handle_pool_s *handle_pool;
  SM_ARRAY(sm_material_s) materials;

} sm_material_manager_s;

b8 sm_material_manager_read(sm_material_s *material, sm_resource_s *resource);

sm_material_manager_s *MATERIAL_MANAGER = NULL;

b8 sm_material_manager_init(void)
{

  MATERIAL_MANAGER = SM_CALLOC(1, sizeof(sm_material_manager_s));
  SM_ASSERT(MATERIAL_MANAGER);

  struct sm_handle_pool_s *handle_pool = sm_handle_pool_new( );
  if (!sm_handle_pool_ctor(handle_pool, 16)) {
    SM_LOG_ERROR("failed to create material handle pool");
    return false;
  }
  MATERIAL_MANAGER->handle_pool = handle_pool;

  MATERIAL_MANAGER->materials = SM_ALIGNED_ALLOC(16, sizeof(sm_material_s) * 16);
  memset(MATERIAL_MANAGER->materials, 0x0, sizeof(sm_material_s) * 16);

  return true;
}

sm_material_handler_s sm_material_manager_new(u32 uuid)
{

  sm_resource_s *resource = sm_resource_manager_get_by_uuid(uuid);
  if (!resource) {
    SM_LOG_ERROR("material does not exists uuid(%d)", uuid);
    return (sm_material_handler_s){SM_INVALID_HANDLE};
  }

  SM_ASSERT(SM_MASK_CHK(resource->type, SM_RESOURCE_TYPE_MATERIAL) && "resource is not a material");

  if (resource->handle != SM_INVALID_HANDLE) {
    sm_material_s *material = &MATERIAL_MANAGER->materials[sm_handle_index(resource->handle)];
    atomic_fetch_add(&material->ref_count, 1);
    return (sm_material_handler_s){resource->handle};
  }

  if (sm_handle_full(MATERIAL_MANAGER->handle_pool)) {
    SM_LOG_ERROR("material handle pool is full");
    return (sm_material_handler_s){SM_INVALID_HANDLE};
  }

  u32 handle = sm_handle_new(MATERIAL_MANAGER->handle_pool);
  if (handle == SM_INVALID_HANDLE) {
    SM_LOG_ERROR("failed to create material handle");
    return (sm_material_handler_s){SM_INVALID_HANDLE};
  }

  /* TODO: mutex is required */
  resource->handle = handle;

  sm_material_s *material = &MATERIAL_MANAGER->materials[sm_handle_index(handle)];
  material->uuid = uuid;
  sm_material_manager_read(material, resource);

  atomic_fetch_add(&material->ref_count, 1);

  return (sm_material_handler_s){handle};
}

void sm_material_manager_set_name(sm_material_handler_s handler, SM_STRING name)
{

  SM_ASSERT(MATERIAL_MANAGER);
  SM_ASSERT(sm_handle_valid(MATERIAL_MANAGER->handle_pool, handler.handle));

  u32 index = sm_handle_index(handler.handle);
  SM_ASSERT(index < SM_ARRAY_LEN(MATERIAL_MANAGER->materials));

  sm_material_s *material = &MATERIAL_MANAGER->materials[index];

  if (material->name != NULL) SM_STRING_FREE(material->name);
  material->name = SM_STRING_DUP(name);
}

void sm_material_manager_set_diffuse_color(sm_material_handler_s handler, sm_vec4 diffuse_color)
{

  SM_ASSERT(MATERIAL_MANAGER);
  SM_ASSERT(sm_handle_valid(MATERIAL_MANAGER->handle_pool, handler.handle));

  u32 index = sm_handle_index(handler.handle);
  SM_ASSERT(index < SM_ARRAY_LEN(MATERIAL_MANAGER->materials));

  sm_material_s *material = &MATERIAL_MANAGER->materials[index];

  glm_vec4_copy(diffuse_color.data, material->diffuse_color.data);
}

void sm_material_manager_set_diffuse_map(sm_material_handler_s handler, sm_texture_handler_s diffuse_map)
{
  SM_ASSERT(MATERIAL_MANAGER);
  SM_ASSERT(sm_handle_valid(MATERIAL_MANAGER->handle_pool, handler.handle));

  u32 index = sm_handle_index(handler.handle);
  SM_ASSERT(index < SM_ARRAY_LEN(MATERIAL_MANAGER->materials));

  sm_material_s *material = &MATERIAL_MANAGER->materials[index];

  if (material->diffuse_map.handle != SM_INVALID_HANDLE) sm_texture_manager_dtor(diffuse_map);
  material->diffuse_map = diffuse_map;
}

void sm_material_manager_set_shader(sm_material_handler_s handler, sm_shader_handler_s shader)
{

  SM_ASSERT(MATERIAL_MANAGER);
  SM_ASSERT(sm_handle_valid(MATERIAL_MANAGER->handle_pool, handler.handle));

  u32 index = sm_handle_index(handler.handle);
  SM_ASSERT(index < SM_ARRAY_LEN(MATERIAL_MANAGER->materials));

  sm_material_s *material = &MATERIAL_MANAGER->materials[index];

  if (material->shader.handle != SM_INVALID_HANDLE) sm_shader_manager_dtor(shader);
  material->shader = shader;
}

SM_STRING sm_material_manager_get_name(sm_material_handler_s handler)
{

  SM_ASSERT(MATERIAL_MANAGER);
  SM_ASSERT(sm_handle_valid(MATERIAL_MANAGER->handle_pool, handler.handle));

  u32 index = sm_handle_index(handler.handle);
  SM_ASSERT(index < SM_ARRAY_LEN(MATERIAL_MANAGER->materials));

  sm_material_s *material = &MATERIAL_MANAGER->materials[index];

  return material->name;
}

u32 sm_material_manager_get_uuid(sm_material_handler_s handler)
{

  SM_ASSERT(MATERIAL_MANAGER);
  SM_ASSERT(sm_handle_valid(MATERIAL_MANAGER->handle_pool, handler.handle));

  u32 index = sm_handle_index(handler.handle);
  SM_ASSERT(index < SM_ARRAY_LEN(MATERIAL_MANAGER->materials));

  sm_material_s *material = &MATERIAL_MANAGER->materials[index];

  return material->uuid;
}

sm_vec4 sm_material_manager_get_diffuse_color(sm_material_handler_s handler)
{

  SM_ASSERT(MATERIAL_MANAGER);
  SM_ASSERT(sm_handle_valid(MATERIAL_MANAGER->handle_pool, handler.handle));

  u32 index = sm_handle_index(handler.handle);
  SM_ASSERT(index < SM_ARRAY_LEN(MATERIAL_MANAGER->materials));

  sm_material_s *material = &MATERIAL_MANAGER->materials[index];

  return material->diffuse_color;
}

sm_texture_handler_s sm_material_manager_get_diffuse_map(sm_material_handler_s handler)
{

  SM_ASSERT(MATERIAL_MANAGER);
  SM_ASSERT(sm_handle_valid(MATERIAL_MANAGER->handle_pool, handler.handle));

  u32 index = sm_handle_index(handler.handle);
  SM_ASSERT(index < SM_ARRAY_LEN(MATERIAL_MANAGER->materials));

  sm_material_s *material = &MATERIAL_MANAGER->materials[index];

  return material->diffuse_map;
}

sm_shader_handler_s sm_material_manager_get_shader(sm_material_handler_s handler)
{

  SM_ASSERT(MATERIAL_MANAGER);
  SM_ASSERT(sm_handle_valid(MATERIAL_MANAGER->handle_pool, handler.handle));

  u32 index = sm_handle_index(handler.handle);
  SM_ASSERT(index < SM_ARRAY_LEN(MATERIAL_MANAGER->materials));

  sm_material_s *material = &MATERIAL_MANAGER->materials[index];

  return material->shader;
}

b8 sm_material_manager_write(sm_material_s *material)
{

  SM_STRING path_name = sm_resource_manager_get_path(material->name, SM_RESOURCE_TYPE_MATERIAL);

  sm_resource_s *resource = sm_resource_manager_get(path_name);
  if (resource) {
    SM_LOG_WARN("[%s] file already exists", path_name);
    return true;
  }

  sm_file_handle_s fhandle;
  smr_file_header_s header = smr_file_header_new(SMR_RESOURCE_MATERIAL, material->uuid);

  if (!sm_filesystem_open(path_name, SM_FILE_MODE_WRITE, true, &fhandle)) {
    SM_LOG_ERROR("[%s] failed to open/creat material file", material->name);
    return false;
  }

  SM_LOG_TRACE("writing material %s into %s", material->name, path_name);
  SM_STRING_FREE(path_name);
  if (!smr_write_header(&header, &fhandle)) {
    SM_LOG_ERROR("[%s] failed to write material header", material->name);
    return false;
  }

  b8 has_texture = (material->diffuse_map.handle != SM_INVALID_HANDLE);
  if (!sm_filesystem_write_bytes(&fhandle, &has_texture, sizeof(b8))) {
    SM_LOG_ERROR("failed to write has texure");
    return false;
  }

  if (has_texture) {
    sm_pixel_format_e format = sm_texture_manager_get_pixel_format(material->diffuse_map);
    if (!sm_filesystem_write_bytes(&fhandle, &format, sizeof(sm_pixel_format_e))) {
      SM_LOG_ERROR("failed to write texture pixel format");
      return false;
    }

    u32 WH[2] = {
        sm_texture_manager_get_width(material->diffuse_map),
        sm_texture_manager_get_height(material->diffuse_map),
    };
    if (!sm_filesystem_write_bytes(&fhandle, WH, sizeof(WH))) {
      SM_LOG_ERROR("failed to write texture width height");
      return false;
    }

    const void *data = sm_texture_manager_cpu_get_data(material->diffuse_map);
    if (!data) {
      SM_LOG_ERROR("[%s] failed to retrieve texture data", material->name);
      return false;
    }

    size_t size = sm_gl_get_pixel_data_size(WH[0], WH[1], format);
    SM_LOG_TRACE("(%dx%d) | %d aka %zu bytes", WH[0], WH[1], format, size);
    if (!sm_filesystem_write_bytes(&fhandle, &size, sizeof(size_t))) {
      SM_LOG_ERROR("failed to write texture data length");
      return false;
    }

    if (!sm_filesystem_write_bytes(&fhandle, data, size)) {
      SM_LOG_ERROR("failed to write texture data");
      return false;
    }
  }

  b8 has_diffuse = !glm_vec4_eq((float *)material->diffuse_color.data, 0.0f);
  if (!sm_filesystem_write_bytes(&fhandle, &has_diffuse, sizeof(b8))) {
    SM_LOG_ERROR("failed to write has diffuse");
    return false;
  }

  if (has_diffuse) {

    if (!sm_filesystem_write_bytes(&fhandle, &material->diffuse_color, sizeof(sm_vec4))) {
      SM_LOG_ERROR("failed to write diffuse");
      return false;
    }
  }

  SM_ASSERT((has_texture || has_diffuse));

  return true;
}

b8 sm_material_manager_read(sm_material_s *material, sm_resource_s *resource)
{

  SM_STRING path_name = SM_STRING_MALLOC(strlen(resource->filename) + strlen("assets/materials/") + 1);
  strcpy(path_name, "assets/materials/");
  strcat(path_name, resource->filename);

  sm_file_handle_s fhandle;
  if (!sm_filesystem_open(path_name, SM_FILE_MODE_READ, true, &fhandle)) {
    SM_LOG_ERROR("[%s] failed to open material file", path_name);
    return false;
  }

  smr_file_header_s header;
  if (!smr_read_header(&header, &fhandle)) {
    SM_LOG_ERROR("[%s] failed to read header", path_name);
    return false;
  }
  SM_STRING_FREE(path_name);

  SM_ASSERT(header.resouce_type == SMR_RESOURCE_MATERIAL);
  SM_ASSERT(header.uuid == resource->uuid);

  b8 has_texture = false;
  if (!sm_filesystem_read_bytes(&fhandle, &has_texture, sizeof(b8))) {
    SM_LOG_ERROR("failed to read has texture");
    return false;
  }

  if (has_texture) {
    sm_pixel_format_e format = 0;
    if (!sm_filesystem_read_bytes(&fhandle, &format, sizeof(sm_pixel_format_e))) {
      SM_LOG_ERROR("failed to read texture pixel format");
      return false;
    }

    u32 WH[2] = {0, 0};
    if (!sm_filesystem_read_bytes(&fhandle, WH, sizeof(WH))) {
      SM_LOG_ERROR("failed to read texture width height");
      return false;
    }

    size_t size = 0;
    if (!sm_filesystem_read_bytes(&fhandle, &size, sizeof(size_t)) || size == 0) {
      SM_LOG_ERROR("failed to read texture data length");
      return false;
    }

    SM_LOG_TRACE("(%dx%d) | %d aka %zu bytes", WH[0], WH[1], format, size);
    void *data = SM_MALLOC(size);
    if (!sm_filesystem_read_bytes(&fhandle, data, size)) {
      SM_LOG_ERROR("failed to write texture data");
      return false;
    }

    material->diffuse_map = sm_texture_manager_new_from_mem(data, WH[0], WH[1], format);
  }

  b8 has_diffuse = false;
  if (!sm_filesystem_read_bytes(&fhandle, &has_diffuse, sizeof(b8))) {
    SM_LOG_ERROR("failed to read has diffuse");
    return false;
  }

  if (has_diffuse) {
    sm_vec4 diffuse;
    if (!sm_filesystem_read_bytes(&fhandle, &diffuse, sizeof(sm_vec4))) {
      SM_LOG_ERROR("failed to read diffuse");
      return false;
    }

    material->diffuse_color.r = diffuse.r;
    material->diffuse_color.g = diffuse.g;
    material->diffuse_color.b = diffuse.b;
    material->diffuse_color.a = diffuse.a;
  }

  return true;
}

void sm_material_manager_teardown(void)
{

  SM_ALIGNED_FREE(MATERIAL_MANAGER->materials);

  sm_handle_pool_dtor(MATERIAL_MANAGER->handle_pool);

  SM_FREE(MATERIAL_MANAGER);

  MATERIAL_MANAGER = NULL;
}
