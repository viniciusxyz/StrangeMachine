#ifndef SM_STACK_LAYER_H
#define SM_STACK_LAYER_H

#include "smpch.h"

#include "core/smLayer.h"

struct sm_stack_layer_s;

/* Allocate memory for a new stack layer */
struct sm_stack_layer_s *sm_stack_layer_new(void);

/* Constructor */
b8 sm_stack_layer_ctor(struct sm_stack_layer_s *stack_layer);

/* Destructor */
void sm_stack_layer_dtor(struct sm_stack_layer_s *stack_layer);

void sm_stack_layer_push(struct sm_stack_layer_s *stack_layer, struct sm_layer_s *layer);
void sm_stack_layer_push_overlay(struct sm_stack_layer_s *stack_layer, struct sm_layer_s *layer);
void sm_stack_layer_pop(struct sm_stack_layer_s *stack_layer);
void sm_stack_layer_pop_overlay(struct sm_stack_layer_s *stack_layer);
size_t sm_stack_layer_get_size(struct sm_stack_layer_s *stack_layer);
struct sm_layer_s *sm_stack_layer_get_layer(struct sm_stack_layer_s *stack_layer, size_t index);

#endif /* SM_STACK_LAYER_H */
