#ifndef SM_GL_H
#define SM_GL_H

#include "smpch.h"

#include "math/smMath.h"
#include "renderer/smTextureManager.h"
#include "resource/smShaderResource.h"

typedef enum {
  SM_F32 = 0x1406,       // GL_FLOAT,
  SM_I32 = 0x1404,       // GL_INT,
  SM_IVEC2 = 0x8B53,     // GL_INT_VEC2,
  SM_IVEC3 = 0x8B54,     // GL_INT_VEC3,
  SM_IVEC4 = 0x8B55,     // GL_INT_VEC4,
  SM_VEC2 = 0x8B50,      // GL_FLOAT_VEC2
  SM_VEC3 = 0x8B51,      // GL_FLOAT_VEC3
  SM_VEC4 = 0x8B52,      // GL_FLOAT_VEC4
  SM_MAT2 = 0x8B5A,      // GL_FLOAT_MAT2
  SM_MAT3 = 0x8B5B,      // GL_FLOAT_MAT3
  SM_MAT4 = 0x8B5C,      // GL_FLOAT_MAT4
  SM_SAMPLER2D = 0x8B5E, // GL_SAMPLER_2D

} sm_type_e;

#define SM_TYPE_TO_STR(TYPE)                                                                                                  \
  ((TYPE) == SM_F32         ? "f32"                                                                                           \
   : (TYPE) == SM_I32       ? "i32"                                                                                           \
   : (TYPE) == SM_IVEC2     ? "ivec2"                                                                                         \
   : (TYPE) == SM_IVEC3     ? "ivec3"                                                                                         \
   : (TYPE) == SM_IVEC4     ? "ivec4"                                                                                         \
   : (TYPE) == SM_VEC2      ? "vec2"                                                                                          \
   : (TYPE) == SM_VEC3      ? "vec3"                                                                                          \
   : (TYPE) == SM_VEC4      ? "vec4"                                                                                          \
   : (TYPE) == SM_MAT2      ? "mat2"                                                                                          \
   : (TYPE) == SM_MAT3      ? "mat3"                                                                                          \
   : (TYPE) == SM_MAT4      ? "mat4"                                                                                          \
   : (TYPE) == SM_SAMPLER2D ? "sampler2D"                                                                                     \
                            : "unknown")

typedef enum {

  SM_DEPTH_TEST = 0x0B71,

} enable_flags_e;

typedef enum {

  SM_DEPTH_BUFFER_BIT = 0x00000100,   /* GL_DEPTH_BUFFER_BIT   */
  SM_STENCIL_BUFFER_BIT = 0x00000400, /* GL_STENCIL_BUFFER_BIT */
  SM_COLOR_BUFFER_BIT = 0x00004000,   /* GL_COLOR_BUFFER_BIT */

} buffer_bit_e;

typedef void *(*sm_gl_loadproc_f)(const char *name);
b8 sm_gl_load_proc(sm_gl_loadproc_f load);

u32 sm_gl_shader_new(sm_shader_resource_handler_s handler);
void sm_gl_shader_bind(u32 program);
void sm_gl_shader_unbind(void);
i32 sm_gl_shader_uniform_get_location(u32 program, const char *uniform);
void sm_gl_shader_uniform_set(i32 program, sm_type_e type, void *value);
void sm_gl_shader_uniform_set_array(i32 uniform_loc, sm_type_e type, u32 size, void *value);
void sm_gl_shader_uniform_get_active(u32 program, u32 index, i32 buf_size, i32 *length, i32 *size, u32 *type, char *name);
i32 sm_gl_shader_uniform_get_active_count(u32 program);
i32 sm_gl_shader_attribute_get_location(u32 program, const char *attribute);
void sm_gl_shader_delete(u32 program);

void sm_gl_vertex_attribute_pointer(u32 index, i32 size, sm_type_e type, bool normalized, i32 stride, const void *pointer);
void sm_gl_vertex_attribute_enable(u32 index);
void sm_gl_vertex_attribute_disable(u32 index);

u32 sm_gl_vbo_new(const void *buf, u32 size, bool dyn);
void sm_gl_vbo_sub_data(u32 VBO, const void *data, u32 size, u32 offset);
void sm_gl_vbo_bind(u32 VBO);
void sm_gl_vbo_unbind(void);
void sm_gl_vbo_delete(u32 VBO);

u32 sm_gl_ebo_new(const void *buf, u32 size, bool dyn);
void sm_gl_ebo_sub_data(u32 EBO, const void *data, u32 size, u32 offset);
void sm_gl_ebo_bind(u32 EBO);
void sm_gl_ebo_unbind(void);

u32 sm_gl_vao_new(void);
void sm_gl_vao_bind(unsigned int VAO);
void sm_gl_vao_unbind(void);
void sm_gl_vao_delete(u32 VAO);

u32 sm_gl_texture_ctor(sm_texture_handler_s handler);
void sm_gl_texture_dtor(u32 texture);
void sm_gl_texture_bind(u32 texture, u32 tex_index);
void sm_gl_texture_unbind(u32 texture, u32 tex_index);
u32 sm_gl_get_pixel_data_size(u32 width, u32 height, sm_pixel_format_e format);

void sm_gl_draw(i32 offset, i32 count);
void sm_gl_draw_elements(i32 count, const void *buffer);
void sm_gl_draw_instanced(i32 count, i32 instances);
void sm_gl_draw_elements_instanced(i32 count, const void *buffer, i32 instances);

void sm_gl_viewport(i32 x, i32 y, i32 width, i32 height);
void sm_gl_clear_color(sm_vec4 color);
void sm_gl_clear(u32 mask);
void sm_gl_enable(enable_flags_e flags);

#endif /* SM_GL_H */
