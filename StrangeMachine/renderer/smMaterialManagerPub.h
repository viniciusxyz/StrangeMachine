#ifndef SM_MATERIAL_MANAGER_PUB_H
#define SM_MATERIAL_MANAGER_PUB_H

#include "smpch.h"

#include "core/smCore.h"

#include "renderer/smShaderManager.h"
#include "renderer/smTextureManager.h"

typedef struct sm__material_s {

  u32 uuid;

  SM_STRING name;

  sm_shader_handler_s shader;
  sm_texture_handler_s diffuse_map;
  sm_vec4 diffuse_color;
  b8 twosided;

  atomic_int ref_count;

} sm_material_s;

#endif /*  SM_MATERIAL_MANAGER_PUB_H */
