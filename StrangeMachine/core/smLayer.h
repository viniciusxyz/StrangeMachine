#ifndef SM_LAYER_H
#define SM_LAYER_H

#include "smpch.h"

#include "core/smLayerPub.h"

struct sm_layer_s;

struct sm_layer_s *sm_layer_new(void);

b8 sm_layer_ctor(struct sm_layer_s *layer, char *name, void *user_data, sm_layer_on_attach_f on_attach,
                 sm_layer_on_detach_f on_detach, sm_layer_on_update_f on_update, sm_layer_on_gui_f on_gui,
                 sm_layer_on_event_f on_event);

void sm_layer_dtor(struct sm_layer_s *layer);

void sm_layer_attach(struct sm_layer_s *layer);
void sm_layer_detach(struct sm_layer_s *layer);
void sm_layer_update(struct sm_layer_s *layer, f32 dt);
void sm_layer_gui(struct sm_layer_s *layer);
b8 sm_layer_event(struct sm_layer_s *layer, sm_event_s *event);
void *sm_layer_get_user_data(struct sm_layer_s *layer);

#endif /* SM_LAYER_H */
