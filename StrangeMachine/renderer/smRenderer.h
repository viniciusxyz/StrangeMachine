#ifndef SM_RENDERER_H
#define SM_RENDERER_H

#include "smpch.h"

#include "scene/smScene.h"

void sm_renderer_init(void);
void sm_renderer_teardown(void);

void sm_renderer_clear(sm_vec4 color);
void sm_renderer_on_resize(i32 width, i32 height);
void sm_renderer_draw_scene(sm_scene_s *scene, sm_ecs_s *ecs);

#endif
