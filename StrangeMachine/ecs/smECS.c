#include "smpch.h"

#include "ecs/smECS.h"

#include "core/smCore.h"

#undef SM_MODULE_NAME
#define SM_MODULE_NAME "ECS"

#define BIT (sizeof(u64) * 8)

struct sm__user_data {
  sm_system_s *sys;
  f32 dt;
};

static b8 sm__ecs_inclusive_cb(sm_component_t key, void *value, void *user_data);

sm_ecs_s *sm_ecs_new(void)
{

  sm_ecs_s *ecs = (sm_ecs_s *)SM_CALLOC(1, sizeof(sm_ecs_s));
  SM_ASSERT(ecs);

  return ecs;
}

b8 sm_ecs_ctor(sm_ecs_s *ecs, sm_component_t comp)
{

  SM_ASSERT(ecs);

  memcpy((void *)&ecs->registered_components, &comp, sizeof(comp));

  component_init( );

  ecs->map_archetype = sm_hashmap_new_u64( );
  if (!sm_hashmap_ctor_u64(ecs->map_archetype, 16, NULL, NULL)) {
    SM_LOG_ERROR("Failed to create hashmap");
    return false;
  }

  return true;
}

b8 sm__ecs_dtor_cb(sm_component_t key, void *value, void *user_data)
{

  SM_UNUSED(key);
  SM_UNUSED(user_data);

  sm_chunk_s *chunk = value;
  SM_ASSERT(chunk);

  for (u32 i = 0; i < chunk->length; ++i) {
    for (u32 j = 0; j < SM_ARRAY_LEN(chunk->view); ++j) {

      sm_component_view_s *v = &chunk->view[j];

      if (v->dtor) v->dtor((u8 *)chunk->data + (i * chunk->size) + v->offset);
    }
  }

  sm_handle_pool_dtor(chunk->pool);
  chunk->alligned ? SM_ALIGNED_FREE(chunk->data) : SM_FREE(chunk->data);
  SM_ARRAY_DTOR(chunk->view);
  SM_FREE(chunk);

  return true;
}

void sm_ecs_dtor(sm_ecs_s *ecs)
{

  SM_ASSERT(ecs);

  if (ecs->systems) SM_ARRAY_DTOR(ecs->systems);

  sm_hashmap_for_each_u64(ecs->map_archetype, sm__ecs_dtor_cb, NULL);
  sm_hashmap_dtor_u64(ecs->map_archetype);

  component_teardown( );

  SM_FREE(ecs);
}

void sm_ecs_do(sm_ecs_s *ecs, f32 dt)
{

  SM_ASSERT(ecs);

  for (size_t i = 0; i < SM_ARRAY_LEN(ecs->systems); ++i) {

    sm_system_s *sys = &ecs->systems[i];

    if (SM_MASK_CHK(sys->flags, SM_SYSTEM_EXCLUSIVE_FLAG)) {

      sm_chunk_s *chunk = sm_hashmap_get_u64(ecs->map_archetype, sys->components);

      if (chunk == NULL) continue;

      sm_system_iterator_s iter;
      iter.length = chunk->length;
      iter.size = chunk->size;
      iter.data = chunk->data;
      iter.desc = chunk->view;
      iter.index = 0;

      sys->system(&iter, dt);

    } else sm_hashmap_for_each_u64(ecs->map_archetype, sm__ecs_inclusive_cb, &(struct sm__user_data){sys, dt});
  }
}

sm_entity_s sm_ecs_entity_new(sm_ecs_s *ecs, sm_component_t archetype)
{

  SM_ASSERT(SM_MASK_CHK_EQ(ecs->registered_components, archetype));

  sm_chunk_s *chunk = sm_hashmap_get_u64(ecs->map_archetype, archetype);

  if (chunk == NULL) {
    sm_chunk_s *new_chunk = SM_CALLOC(1, sizeof(sm_chunk_s));

    new_chunk->pool = sm_handle_pool_new( );
    if (!sm_handle_pool_ctor(new_chunk->pool, 64)) {
      SM_LOG_WARN("failed to create pool for archetype %lu", archetype);
      return (sm_entity_s){0};
    }

    for (u32 i = 0; i < BIT; i++) {
      sm_component_t c = archetype & (sm_component_t)(1ULL << i);
      if (c) {
        sm_component_view_s desc = *component_get_desc(c);
        new_chunk->alligned |= desc.alligned;
        desc.offset += new_chunk->size;
        SM_ARRAY_PUSH(new_chunk->view, desc);
        new_chunk->size += desc.size;
      }
    }

    chunk = new_chunk;
    sm_hashmap_put_u64(ecs->map_archetype, archetype, new_chunk);
  }

  sm_handle_t handle = sm_handle_new(chunk->pool);
  SM_ASSERT(handle != SM_INVALID_HANDLE);

  chunk->length++;

  void *data = NULL;
  if (chunk->alligned) {
    data = SM_ALIGNED_ALLOC(16, chunk->size * chunk->length);
    memset(data, 0x0, chunk->size * chunk->length);
    SM_ASSERT(data);
    if (chunk->data) {
      memcpy(data, chunk->data, chunk->size * (chunk->length - 1));
      SM_ALIGNED_FREE(chunk->data);
    }

  } else {
    data = SM_REALLOC(chunk->data, (chunk->size * chunk->length));
    memset((u8 *)data + chunk->size * (chunk->length - 1), 0x0, chunk->size);
    SM_ASSERT(data);
  }
  chunk->data = data;

  sm_entity_s entity = {.handle = handle, .archetype_index = archetype};

  return entity;
}

void sm_ecs_component_set_data(sm_ecs_s *ecs, sm_entity_s entity, sm_component_t components, void *data)
{

  SM_ASSERT(ecs);

  sm_chunk_s *chunk = sm_hashmap_get_u64(ecs->map_archetype, entity.archetype_index);
  if (chunk == NULL) {
    SM_LOG_WARN("no chunk for archetype %lu", entity.archetype_index);
    return;
  }
  SM_ASSERT(chunk->pool);

  u32 index = sm_handle_index(entity.handle);

  for (u32 i = 0; i < SM_ARRAY_LEN(chunk->view); ++i) {
    sm_component_view_s *v = &chunk->view[i];
    if (SM_MASK_CHK(components, v->id)) memcpy((u8 *)chunk->data + (index * chunk->size) + v->offset, data, v->size);
  }
}

void sm_ecs_component_set_all_data(sm_ecs_s *ecs, sm_entity_s entity, void *data)
{

  SM_ASSERT(ecs);

  sm_chunk_s *chunk = sm_hashmap_get_u64(ecs->map_archetype, entity.archetype_index);
  if (chunk == NULL) {
    SM_LOG_WARN("no chunk for archetype %lu", entity.archetype_index);
    return;
  }
  SM_ASSERT(chunk->pool);

  u32 index = sm_handle_index(entity.handle);

  memcpy((u8 *)chunk->data + (index * chunk->size), data, chunk->size);
}

const void *sm_ecs_component_get_data(sm_ecs_s *ecs, sm_entity_s entity, sm_component_t component)
{

  SM_ASSERT(ecs);

  sm_chunk_s *chunk = sm_hashmap_get_u64(ecs->map_archetype, entity.archetype_index);
  if (chunk == NULL) {
    SM_LOG_INFO("no chunk for archetype %lu", entity.archetype_index);
    return NULL;
  }

  SM_ASSERT(chunk->pool);
  SM_ASSERT(sm_handle_valid(chunk->pool, entity.handle));

  u32 index = sm_handle_index(entity.handle);
  SM_ASSERT(index < chunk->length);

  if (SM_MASK_CHK_EQ(component, SM_ALL_COMP)) return (u8 *)chunk->data + (index * chunk->size);

  for (u32 i = 0; i < SM_ARRAY_LEN(chunk->view); ++i) {
    if (SM_MASK_CHK(chunk->view[i].id, component)) return (u8 *)chunk->data + (index * chunk->size) + chunk->view[i].offset;
  }

  return NULL;
}

void sm_ecs_system_register(sm_ecs_s *ecs, sm_component_t comp, system_f system, u32 flags)
{

  SM_ASSERT(ecs);
  SM_ASSERT(SM_MASK_CHK_EQ(ecs->registered_components, comp) && "component not registered");

  sm_system_s sys;
  sys.components = comp;
  sys.system = system;
  sys.flags = flags;

  SM_ARRAY_PUSH(ecs->systems, sys);
}

SM_STRING sm_ecs_entity_to_string(sm_component_t components)
{

  char buf[512];
  buf[0] = '\0';
  b8 first = true;

  for (u32 i = 0; i < BIT; i++) {
    sm_component_t c = components & (sm_component_t)(1ULL << i);
    if (c) {
      sm_component_view_s desc = *component_get_desc(c);
      if (first) {
        first = false;
      } else {
        strcat(buf, "|");
      }
      strcat(buf, desc.name);
    }
  }

  return SM_STRING_DUP(buf);
  /* SM_STRING str_upper = sm_string_to_upper(str); */
  /* SM_STRING_DTOR(str); */

  /* return str; */
}

static b8 sm__ecs_inclusive_cb(sm_component_t key, void *value, void *user_data)
{

  sm_chunk_s *chunk = (sm_chunk_s *)value;
  struct sm__user_data *u_data = (struct sm__user_data *)user_data;

  if (!chunk->pool) {
    SM_LOG_WARN("no pool for archetype %zu", key);
    return false;
  }

  sm_system_iterator_s iter = {0};
  iter.size = chunk->size;
  size_t offset = 0;
  iter.length = chunk->length;
  iter.data = chunk->data;
  iter.index = 0;
  if (SM_MASK_CHK_EQ(key, u_data->sys->components)) {

    for (u32 k = 0; k < BIT; ++k) {

      sm_component_t cmp = key & (sm_component_t)(1ULL << k);
      if (cmp) {
        sm_component_view_s desc = *component_get_desc(cmp);

        if (SM_MASK_CHK(u_data->sys->components, cmp)) {
          desc.offset = offset;
          SM_ARRAY_PUSH(iter.desc, desc);
        }

        offset += desc.size;
      }
    }

    u_data->sys->system(&iter, u_data->dt);
    SM_ARRAY_DTOR(iter.desc);
  }

  return true;
}

#undef SM_MODULE_NAME
