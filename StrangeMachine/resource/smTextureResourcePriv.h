#ifndef SM_TEXTURE_RESOURCE_PRIVATE_H
#define SM_TEXTURE_RESOURCE_PRIVATE_H

#include "smpch.h"

bool sm_texture_manager_init(size_t capacity);
void sm_texture_manager_teardown(void);

#endif /* SM_TEXTURE_RESOURCE_PRIVATE_H */
