#ifndef SM_TEXTURE_RESOURCE_PUBLIC_H
#define SM_TEXTURE_RESOURCE_PUBLIC_H

#include "smpch.h"

#include "core/util/smString.h"

typedef enum {
  SM_PIXELFORMAT_UNCOMPRESSED_GRAYSCALE = 1, // 8 bit per pixel (no alpha)
  SM_PIXELFORMAT_UNCOMPRESSED_GRAY_ALPHA,    // 8*2 bpp (2 channels)
  SM_PIXELFORMAT_UNCOMPRESSED_R5G6B5,        // 16 bpp
  SM_PIXELFORMAT_UNCOMPRESSED_R8G8B8,        // 24 bpp
  SM_PIXELFORMAT_UNCOMPRESSED_R5G5B5A1,      // 16 bpp (1 bit alpha)
  SM_PIXELFORMAT_UNCOMPRESSED_R4G4B4A4,      // 16 bpp (4 bit alpha)
  SM_PIXELFORMAT_UNCOMPRESSED_R8G8B8A8,      // 32 bpp
  SM_PIXELFORMAT_COMPRESSED_DXT1_RGB,        // 4 bpp (no alpha)
  SM_PIXELFORMAT_COMPRESSED_DXT1_RGBA,       // 4 bpp (1 bit alpha)
  SM_PIXELFORMAT_COMPRESSED_DXT3_RGBA,       // 8 bpp
  SM_PIXELFORMAT_COMPRESSED_DXT5_RGBA,       // 8 bpp

} sm_pixel_format_e;

typedef struct {
  u32 handle;

} sm_texture_resource_handler_s;

sm_texture_resource_handler_s sm_texture_manager_new(sm_string file);
void sm_texture_manager_dtor(sm_texture_resource_handler_s handler);
sm_texture_resource_handler_s sm_texture_manager_reference(sm_texture_resource_handler_s handler);

sm_string sm_texture_manager_get_name(sm_texture_resource_handler_s handler);
u32 sm_texture_manager_get_channels(sm_texture_resource_handler_s handler);
sm_pixel_format_e sm_texture_manager_get_pixel_format(sm_texture_resource_handler_s handler);
u32 sm_texture_manager_get_height(sm_texture_resource_handler_s handler);
u32 sm_texture_manager_get_width(sm_texture_resource_handler_s handler);
const void *sm_texture_manager_cpu_get_data(sm_texture_resource_handler_s handler);

b8 sm_texture_manager_load_data(sm_texture_resource_handler_s handler);
b8 sm_texture_manager_cpu_has_data(sm_texture_resource_handler_s handler);

#endif /* SM_TEXTURE_RESOURCE_PUBLIC_H */
