#include "smpch.h"

#include "core/smCore.h"
#include "resource/file_format/smSMR.h"

smr_file_header_s smr_file_header_new(u32 resource_type, u32 uuid)
{

  smr_file_header_s header = {
      .signature = SMR_MAGIC_NUMBER, .version = SMR_VERSION_NUMBER, .resouce_type = resource_type, .uuid = uuid};
  return header;
}

b8 smr_write_header(smr_file_header_s *header, sm_file_handle_s *file)
{

  SM_ASSERT(file);

  if (header->signature != SMR_MAGIC_NUMBER) {
    SM_LOG_ERROR("invalid signature: 0x%x", header->signature);
    return false;
  }

  if (header->version > SMR_VERSION_NUMBER) {
    SM_LOG_ERROR("invalid version: 0x%x", header->version);
    return false;
  }

  if (header->resouce_type == SMR_RESOURCE_NONE) {
    SM_LOG_ERROR("invalid resource type: 0x%x", header->resouce_type);
    return false;
  }

  /* if (sm_filesystem_size(file) != 0) { */
  /*   SM_LOG_ERROR("file is not empty"); */
  /*   return false; */
  /* } */

  if (!sm_filesystem_write_bytes(file, &header->signature, sizeof(u32))) {
    SM_LOG_ERROR("[s] failed to write header");
    return false;
  }

  if (!sm_filesystem_write_bytes(file, &header->version, sizeof(u32))) {
    SM_LOG_ERROR("[s] failed to write header");
    return false;
  }

  if (!sm_filesystem_write_bytes(file, &header->resouce_type, sizeof(u64))) {
    SM_LOG_ERROR("[s] failed to write header");
    return false;
  }

  return true;
}

b8 smr_read_header(smr_file_header_s *header, sm_file_handle_s *file)
{

  SM_ASSERT(file);

  if (sm_filesystem_size(file) < sizeof(smr_file_header_s)) {
    SM_LOG_ERROR("file is too small");
    return false;
  }

  if (!sm_filesystem_read_bytes(file, &header->signature, sizeof(u32))) {
    SM_LOG_ERROR("[s] failed to read header");
    return false;
  }

  if (!sm_filesystem_read_bytes(file, &header->version, sizeof(u32))) {
    SM_LOG_ERROR("[s] failed to read header");
    return false;
  }

  if (!sm_filesystem_read_bytes(file, &header->resouce_type, sizeof(u64))) {
    SM_LOG_ERROR("[s] failed to read header");
    return false;
  }

  if (header->signature != SMR_MAGIC_NUMBER) {
    SM_LOG_ERROR("invalid signature: 0x%x", header->signature);
    return false;
  }

  if (header->version != SMR_VERSION_NUMBER) {
    SM_LOG_ERROR("invalid version: 0x%x", header->version);
    return false;
  }

  if (header->resouce_type == SMR_RESOURCE_NONE) {
    SM_LOG_ERROR("invalid resource type: 0x%x", header->resouce_type);
    return false;
  }

  return true;
}
