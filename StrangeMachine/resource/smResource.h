#ifndef SM_RESOURCE_H
#define SM_RESOURCE_H

#include "smpch.h"

#include "core/smCore.h"

typedef enum {
  SM_RESOURCE_STATUS_NONE = 0,
  SM_RESOURCE_STATUS_FOUND = 1 << 0,
  /* if the file was once found, but sudenly is gone (generally because of a manual delete) */
  SM_RESOURCE_STATUS_NOT_FOUND = 1 << 1,
  SM_RESOURCE_STATUS_RELOADED = 1 << 2, /* if the file was changed on disk this flag is set */
  SM_RESOURCE_STATUS_INVALID = 1 << 3,

  SM_RESOURCE_STATUS_MASK_ALL =
      SM_RESOURCE_STATUS_FOUND | SM_RESOURCE_STATUS_NOT_FOUND | SM_RESOURCE_STATUS_RELOADED | SM_RESOURCE_STATUS_INVALID

} sm_resource_manager_status_e;

typedef enum {
  SM_RESOURCE_TYPE_NONE = 0,
  SM_RESOURCE_TYPE_TEXTURE = 1 << 0,
  SM_RESOURCE_TYPE_AUDIO = 1 << 1,
  SM_RESOURCE_TYPE_SHADER = 1 << 2,
  SM_RESOURCE_TYPE_SCENE = 1 << 3,
  SM_RESOURCE_TYPE_MATERIAL = 1 << 4,
  SM_RESOURCE_TYPE_INVALID = 1 << 5,

  SM_RESOURCE_TYPE_MASK_ALL = SM_RESOURCE_TYPE_TEXTURE | SM_RESOURCE_TYPE_AUDIO | SM_RESOURCE_TYPE_SHADER |
                              SM_RESOURCE_TYPE_SCENE | SM_RESOURCE_TYPE_MATERIAL | SM_RESOURCE_TYPE_INVALID

} sm_resource_manager_type_e;

typedef struct sm__resource_s {

  SM_STRING filename;

  sm_resource_manager_type_e type;
  sm_resource_manager_status_e status;

  u32 handle;
  u32 uuid;

} sm_resource_s;

b8 sm_resource_manager_init(const char *folder);
void sm_resource_manager_teardown( );
sm_resource_s *sm_resource_manager_get(const SM_STRING key);
sm_resource_s *sm_resource_manager_get_by_uuid(u32 uuid);
SM_STRING sm_resource_manager_get_path(const SM_STRING name, sm_resource_manager_type_e type);

#endif /* SM_RESOURCE_H */
