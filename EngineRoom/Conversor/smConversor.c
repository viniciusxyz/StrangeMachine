#include "resource/smResource.h"
#include <unistd.h>
#define SM_DEBUG
#include "smpch.h"

#include "renderer/smMaterialManager.h"
#include "renderer/smMaterialManagerPub.h"
#include "renderer/smTextureManager.h"
#include "smConversorUtil.h"

#include <assimp/cimport.h>
#include <assimp/color4.h>
#include <assimp/material.h>
#include <assimp/postprocess.h>
#include <assimp/scene.h>
#include <assimp/texture.h>
#include <assimp/version.h>

#include "core/data/smHashMap.h"
#include "core/smCore.h"
#include "core/util/smBitMask.h"
#include "core/util/smString.h"
#include "scene/smScene.h"

#include "math/smMath.h"
#include "util/colors.h"

#define STBI_MALLOC(sz)        SM_MALLOC(sz)
#define STBI_REALLOC(p, newsz) SM_REALLOC(p, newsz)
#define STBI_FREE(p)           SM_FREE(p)
#define STBI_ASSERT(x)         SM_ASSERT(x)
#define STB_IMAGE_IMPLEMENTATION
#include "vendor/stbi/stb_image.h"

typedef struct sm__user_data_s {
  sm_scene_s *scene;
  sm_ecs_s *ecs;
  u32 node;
} sm_user_data;

u32 *material_cache = NULL;

sm_string file_path = {0};
sm_string filename = {0};
sm_string path = {0};

void process_node(struct aiNode *node, const struct aiScene *scene, sm_user_data u_data);
sm_entity_s load_mesh(const struct aiScene *scene, const struct aiMesh *ai_mesh, sm_user_data u_data);

sm_scene_s *sc = NULL;

void print_cb(sm_scene_s *scene, u32 index, void *user_data) {

  SM_UNUSED(user_data);

  sm_entity_s *entts = sm_scene_get_entity(scene, index);
  sm_string name = sm_scene_get_name(scene, index);
  printf("%d=>%s\n", index, name.str);

  for (size_t i = 0; i < SM_ARRAY_LEN(entts); ++i) {
    sm_entity_s e = entts[i];

    sm_string s = sm_ecs_entity_to_string(e.archetype_index);
    printf("\t->parent: %d, entity: %d, %lu(%s)\n", sm_scene_get_parent(scene, index), e.handle, e.archetype_index, s.str);
    sm_string_dtor(s);

    if (SM_MASK_CHK(e.archetype_index, SM_MESH_COMP)) {
      const sm_mesh_s *mesh = sm_ecs_component_get_data(user_data, e, SM_MESH_COMP);
      sm_resource_s *resource = sm_resource_manager_get_by_uuid(mesh->material_uuid);
      printf("\t\t->Material uuid: %d (%d) aka: %s\n", mesh->material_uuid, resource->handle, resource->filename.str);
    }

    /* if (SM_MASK_CHK(e.archetype_index, SM_MATERIAL_COMP)) { */
    /**/
    /*   const sm_material_s *material = sm_ecs_component_get_data(user_data, e, SM_MATERIAL_COMP); */
    /*   if (material->diffuse_map.handle != SM_INVALID_HANDLE) */
    /*     printf("\t\t->Material name: %s (%dx%d) | %d\n", material->name.str, */
    /*            sm_texture_manager_get_width(material->diffuse_map), sm_texture_manager_get_height(material->diffuse_map), */
    /*            sm_texture_manager_get_pixel_format(material->diffuse_map)); */
    /*   else */
    /*     printf("\t\t->Material name: %s (%f, %f, %f, %f)\n", material->name.str, material->color.r, material->color.g, */
    /*            material->color.b, material->color.a); */
    /* } */
  }
}

void process_node(struct aiNode *ai_node, const struct aiScene *ai_scene, sm_user_data u_data) {

  struct aiMatrix4x4 ai_mat4 = ai_node->mTransformation;
  sm_mat4 mat;
  SMC_MAT4_ASSIMP_TO_SM(mat, ai_mat4);

  sm_transform_s local_transform = sm_transform_mat4_to_transform(mat);
  sm_scene_set_local_transform(u_data.scene, u_data.node, local_transform);

  sm_string name;
  if (ai_node->mName.length > 0)
    name = sm_string_from(ai_node->mName.data);
  else
    name = sm_string_from("DEFAULT NODE NAME");

  sm_scene_set_name(u_data.scene, u_data.node, name);
  sm_string_dtor(name);

  /* process each mesh located at the current node */
  for (unsigned int i = 0; i < ai_node->mNumMeshes; ++i) {

    /* the node object only contains indices to index the actual objects in the scene. */
    /* the scene contains all the data, node is just to keep stuff organized (like relations between nodes). */
    struct aiMesh *ai_mesh = ai_scene->mMeshes[ai_node->mMeshes[i]];

    sm_entity_s entt = load_mesh(ai_scene, ai_mesh, u_data);
    sm_scene_push_entity(u_data.scene, u_data.node, entt);
  }

  /* after we've processed all of the meshes (if any) we then recursively process each of the children nodes */
  for (unsigned int i = 0; i < ai_node->mNumChildren; ++i) {
    u32 child_node = sm_scene_new_node(u_data.scene);
    sm_scene_add_child(u_data.scene, u_data.node, child_node);

    sm_user_data u_data_child = u_data;
    u_data_child.node = child_node;

    process_node(ai_node->mChildren[i], ai_scene, u_data_child);
  }
}

sm_entity_s load_mesh(const struct aiScene *ai_scene, const struct aiMesh *ai_mesh, sm_user_data u_data) {

  sm_mesh_s mesh = {0};

  if (ai_mesh->mVertices != NULL && ai_mesh->mNumVertices > 0) {
    SM_ARRAY_SET_LEN(mesh.positions, ai_mesh->mNumVertices);
    memcpy(mesh.positions, ai_mesh->mVertices, ai_mesh->mNumVertices * sizeof(float) * 3);
  }

  if (ai_mesh->mNormals != NULL && ai_mesh->mNumVertices > 0) {
    SM_ARRAY_SET_LEN(mesh.normals, ai_mesh->mNumVertices);
    memcpy(mesh.normals, ai_mesh->mNormals, ai_mesh->mNumVertices * sizeof(float) * 3);
  }

  if (ai_mesh->mTextureCoords[0] != NULL && ai_mesh->mNumVertices > 0) {
    SM_ARRAY_SET_LEN(mesh.uvs, ai_mesh->mNumVertices);
    for (u32 uv = 0; uv < ai_mesh->mNumVertices; ++uv) {
      mesh.uvs[uv].x = ai_mesh->mTextureCoords[0][uv].x;
      mesh.uvs[uv].y = ai_mesh->mTextureCoords[0][uv].y;
    }
  } else {
    SM_ARRAY_SET_LEN(mesh.uvs, ai_mesh->mNumVertices);
    for (u32 uv = 0; uv < ai_mesh->mNumVertices; ++uv) {
      mesh.uvs[uv].x = 1.0f;
      mesh.uvs[uv].y = 1.0f;
    }
  }

  /* get the transformation matrix */

  if (ai_mesh->mColors[0] != NULL && ai_mesh->mNumVertices > 0) {
    /* SM_ARRAY_SET_LEN(mesh.colors, ai_mesh->mNumVertices); */
    SM_ALIGNED_ARRAY_NEW(mesh.colors, 16, ai_mesh->mNumVertices);
    for (u32 color = 0; color < ai_mesh->mNumVertices; ++color) {
      mesh.colors[color].r = ai_mesh->mColors[0][color].r;
      mesh.colors[color].g = ai_mesh->mColors[0][color].g;
      mesh.colors[color].b = ai_mesh->mColors[0][color].b;
      mesh.colors[color].a = ai_mesh->mColors[0][color].a;
    }
  } else {
    SM_ARRAY_SET_LEN(mesh.colors, ai_mesh->mNumVertices);
    for (u32 color = 0; color < ai_mesh->mNumVertices; ++color) {
      mesh.colors[color] = WHITE;
    }
  }

  /* load indices */
  if (ai_mesh->mFaces != NULL && ai_mesh->mNumFaces > 0) {
    for (u32 i = 0; i < ai_mesh->mNumFaces; ++i) {
      struct aiFace face = ai_mesh->mFaces[i];
      for (u32 j = 0; j < face.mNumIndices; ++j) {
        SM_ARRAY_PUSH(mesh.indices, face.mIndices[j]);
      }
    }
  }

  sm_mesh_component_set_renderable(&mesh, true);

  bool has_material = false;
  struct aiMaterial *ai_material = ai_scene->mMaterials[ai_mesh->mMaterialIndex];

  if (material_cache[ai_mesh->mMaterialIndex]) {

    mesh.material_uuid = material_cache[ai_mesh->mMaterialIndex];
    printf("Using material from cache: %d\n", mesh.material_uuid);

  } else {
    struct aiString ai_texture_path;
    sm_texture_handler_s diffuse_map = {SM_INVALID_HANDLE};
    if (aiGetMaterialTextureCount(ai_material, aiTextureType_DIFFUSE) > 0 &&
        aiGetMaterialTexture(ai_material, aiTextureType_DIFFUSE, 0, &ai_texture_path, NULL, NULL, NULL, NULL, NULL, NULL) ==
            AI_SUCCESS) {

      void *texture_data = NULL;
      i32 width = 0;
      i32 height = 0;
      i32 channels = 0;

      if (ai_texture_path.data[0] == '*') {
        struct aiTexture *tex = ai_scene->mTextures[atoi(&ai_texture_path.data[1])];
        printf("Texture[%s] (%dx%d) | %s\n", tex->mFilename.data, tex->mWidth, tex->mHeight, tex->achFormatHint);

        if (tex->mHeight == 0) {
          texture_data = stbi_load_from_memory((void *)tex->pcData, (i32)tex->mWidth, &width, &height, &channels, 0);
          printf("Texture stbi memory (%dx%d) | %d\n", width, height, channels);
        }

      } else {

        /* sm_string texture_file_path = sm_string_append_c_str(path, ai_texture_path.data); */
        char texture_file_path[512];
        texture_file_path[0] = '\0';
        strcat(texture_file_path, path.str);
        strcat(texture_file_path, ai_texture_path.data);
        texture_data = stbi_load(texture_file_path, &width, &height, &channels, 0);
        if (!texture_data) {
          printf("[%s] invalid texture data\n", texture_file_path);
          exit(1);
        }
        printf("Texture stbi file [%s] (%dx%d) | %d\n", texture_file_path, width, height, channels);

        /* sm_string_dtor(texture_file_path); */
      }

      sm_pixel_format_e format = SM_PIXELFORMAT_UNCOMPRESSED_GRAYSCALE;

      if (channels == 1)
        format = SM_PIXELFORMAT_UNCOMPRESSED_GRAYSCALE;
      else if (channels == 2)
        format = SM_PIXELFORMAT_UNCOMPRESSED_GRAY_ALPHA;
      else if (channels == 3)
        format = SM_PIXELFORMAT_UNCOMPRESSED_R8G8B8;
      else if (channels == 4)
        format = SM_PIXELFORMAT_UNCOMPRESSED_R8G8B8A8;

      diffuse_map = sm_texture_manager_new_from_mem(texture_data, (u32)width, (u32)height, format);

      /* stbi_image_free(texture_data); */

      has_material = true;
    }

    struct aiColor4D ai_diffuse_color = {0};
    sm_vec4 diffuse_color = sm_vec4_zero( );
    if (aiGetMaterialColor(ai_material, AI_MATKEY_COLOR_DIFFUSE, &ai_diffuse_color) == AI_SUCCESS) {

      diffuse_color.r = ai_diffuse_color.r;
      diffuse_color.g = ai_diffuse_color.g;
      diffuse_color.b = ai_diffuse_color.b;
      diffuse_color.a = ai_diffuse_color.a;
      has_material = true;
    }

    if (has_material) {

      static char buf[512];
      static u32 idx = 0;

      struct aiString ai_material_name;
      sm_string material_name;
      if (aiGetMaterialString(ai_material, AI_MATKEY_NAME, &ai_material_name) == AI_SUCCESS)
        material_name = sm_string_from(ai_material_name.data);
      else {
        snprintf(buf, 512, "material%d", idx++); // puts string into buffer
        material_name = sm_string_from(buf);
      }

      sm_string full_path = sm_resource_manager_get_path(material_name, SM_RESOURCE_TYPE_MATERIAL);
      sm_resource_s *resource = sm_resource_manager_get(full_path);
      if (!resource) {
        printf("Material not found in Resource... Creating one\n");
        sm_material_s material = {0};
        u32 uuid = (u32)(rand( ) + 1);
        material_cache[ai_mesh->mMaterialIndex] = uuid;
        mesh.material_uuid = uuid;
        material.uuid = uuid;
        glm_vec4_copy(diffuse_color.data, material.diffuse_color.data);
        material.name = sm_string_reference(material_name);
        material.diffuse_map = diffuse_map;
        sm_material_manager_write(&material);
      } else {
        printf("Material found in Resource... copying uuid to the cache \n");
        mesh.material_uuid = resource->uuid;
        material_cache[ai_mesh->mMaterialIndex] = resource->uuid;
      }

      sm_string_dtor(material_name);
    }
  }

  sm_entity_s entity = sm_ecs_entity_new(u_data.ecs, SM_MESH_COMP);
  sm_ecs_component_set_data(u_data.ecs, entity, SM_MESH_COMP, &mesh);

  return entity;
}

int main(int argc, char *argv[]) {

  if (argc < 2) {
    printf("Usage: %s <file.{obj,gltf}>\n", argv[0]);
    return 1;
  }

  file_path = sm_string_from(argv[1]);
  /* file_path = sm_string_from("assets/TexturedCube.gltf"); */

  sm_resource_manager_init("assets/");
  sm_material_manager_init( );
  sm_texture_manager_init( );

  char buf[512] = {0};
  SM_ARRAY(sm_string) strs = sm_string_split(file_path, '/');
  for (size_t i = 0; i < SM_ARRAY_LEN(strs); ++i) {
    if (SM_ARRAY_LEN(strs) - 1 != i) {
      strcat(buf, strs[i].str);
      strcat(buf, "/");
    }
  }

  path = sm_string_from(buf);
  filename = sm_string_from(strs[(SM_ARRAY_LEN(strs) - 1)].str);

  for (size_t i = 0; i < SM_ARRAY_LEN(strs); ++i) sm_string_dtor(strs[i]);
  SM_ARRAY_DTOR(strs);

  /* printf("file_path: %s\n", file_path.str); */
  /* printf("path: %s\n", path.str); */
  /* printf("filename: %s\n", filename.str); */

  const struct aiScene *scene = aiImportFile(file_path.str, aiProcessPreset_TargetRealtime_Fast);
  if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
    SM_LOG_ERROR("ASSIMP: %s\n", aiGetErrorString( ));
    exit(1);
  }

  material_cache = SM_CALLOC(1, sizeof(u32) * scene->mNumMaterials);

  conversor_print_material_info(scene);
  stbi_set_flip_vertically_on_load(true);

  sc = sm_scene_new( );
  if (!sm_scene_ctor(sc)) {
    SM_LOG_ERROR("StrangeMachine: Failed to create scene");
    exit(EXIT_FAILURE);
  }

  sm_ecs_s *ecs = sm_ecs_new( );
  if (!sm_ecs_ctor(ecs, SM_ALL_COMP)) {
    SM_LOG_ERROR("StrangeMachine: Failed to create ECS");
    exit(EXIT_FAILURE);
  }

  sm_string root_name = sm_string_from(scene->mRootNode->mName.data);
  u32 root_node = sm_scene_get_root(sc);

  if (!sm_string_eq_c_str(root_name, "ROOT")) {
    u32 n = sm_scene_new_node(sc);
    if (sm_scene_add_child(sc, root_node, n) == INVALID_NODE) {
      SM_LOG_ERROR("Failed to add child");
      exit(EXIT_FAILURE);
    }
    root_node = n;
  }

  sm_scene_set_name(sc, root_node, root_name);
  sm_string_dtor(root_name);

  sm_user_data u_data;
  u_data.scene = sc;
  u_data.ecs = ecs;
  u_data.node = root_node;

  process_node(scene->mRootNode, scene, u_data);

  sm_string scene_path = sm_string_from("assets/scene/scene.smscene");
  if (!sm_scene_save(sc, scene_path, ecs)) {
    SM_LOG_ERROR("StrangeMachine: Failed to save scene");
    exit(EXIT_FAILURE);
  }

  sm_ecs_s *ecs_dummy = sm_ecs_new( );
  if (!sm_ecs_ctor(ecs_dummy, SM_ALL_COMP)) {
    SM_LOG_ERROR("StrangeMachine: Failed to create ECS");
    exit(EXIT_FAILURE);
  }
  sm_scene_s *scene_dummy = sm_scene_open(scene_path, ecs_dummy);

  printf("==========================\n");
  sm_scene_for_each(sc, 0, print_cb, ecs);
  printf("==========================\n");
  sm_scene_for_each(scene_dummy, 0, print_cb, ecs);
  printf("==========================\n");

  sm_string_dtor(path);
  sm_string_dtor(file_path);
  sm_string_dtor(filename);
  sm_texture_manager_teardown( );
  sm_material_manager_teardown( );
  sm_resource_manager_teardown( );
  sm_scene_dtor(scene_dummy);
  aiReleaseImport(scene);
  SM_FREE(material_cache);
  sm_string_dtor(scene_path);
  sm_scene_dtor(sc);

  return 0;
}
