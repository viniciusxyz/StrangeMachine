#ifndef SM_CORE_WINDOW_H
#define SM_CORE_WINDOW_H

#include "smpch.h"

#include "smWindowPub.h"

struct sm_window_s;

/* acllocate memory for window */
struct sm_window_s *window_new(void);

/* constructor */
b8 sm_window_ctor(struct sm_window_s *win, const char *name, u32 width, u32 height);

/* destructor */
void sm_window_dtor(struct sm_window_s *win);

f32 sm_window_get_aspect_ratio(struct sm_window_s *win);
u32 sm_window_get_width(struct sm_window_s *win);
u32 sm_window_get_height(struct sm_window_s *win);
void sm_window_set_vsync(struct sm_window_s *win, b8 vsync);
b8 sm_window_is_vsync(struct sm_window_s *win);
void sm_window_set_callback(struct sm_window_s *win, event_callback_f callback, void *user_data);
void sm_window_do(struct sm_window_s *win);
void sm_window_swap_buffers(struct sm_window_s *win);
const void *sm_window_get_window_raw(struct sm_window_s *win);
const void *sm_window_get_context_raw(struct sm_window_s *win);

#endif /* SM_CORE_WINDOW_H */
