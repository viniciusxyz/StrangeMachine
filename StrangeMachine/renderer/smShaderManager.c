#include "smpch.h"

#include "core/smCore.h"

#include "renderer/GL/smGL.h"
#include "renderer/smShaderManager.h"

typedef struct sm__shader_active_s {

  i32 size;
  u32 type;
  i32 location;

} sm_shader_active_s;

typedef struct sm__shader_s {

  u32 program;
  atomic_uint ref_count;

  sm_hashmap_str_m *actives;

} sm_shader_s;

typedef struct sm__shader_manager_s {

  struct sm_handle_pool_s *pool;
  SM_ARRAY(sm_shader_s) shaders;

} sm_shader_manager_s;

sm_shader_manager_s *SHADER_MANAGER = NULL;

b8 sm_shader_manager_init(void)
{

  SM_ASSERT(!SHADER_MANAGER && "Shader manager already initialized");

  SHADER_MANAGER = SM_CALLOC(1, sizeof(sm_shader_manager_s));
  SM_ASSERT(SHADER_MANAGER);

  SHADER_MANAGER->pool = sm_handle_pool_new( );
  if (!sm_handle_pool_ctor(SHADER_MANAGER->pool, 16)) {
    SM_LOG_WARN("failed to create pool for shaders");
    return false;
  }

  SM_ARRAY_SET_LEN(SHADER_MANAGER->shaders, 16);
  memset(SHADER_MANAGER->shaders, 0x0, sizeof(sm_shader_s) * 16);

  return true;
}

sm_shader_handler_s sm_shader_manager_new(sm_shader_resource_handler_s shader_resource)
{

  if (sm_handle_full(SHADER_MANAGER->pool)) {
    SM_LOG_ERROR("shader manager pool is full");
    return (sm_shader_handler_s){SM_INVALID_HANDLE};
  }

  u32 handle = sm_handle_new(SHADER_MANAGER->pool);
  if (handle == SM_INVALID_HANDLE) {
    SM_LOG_ERROR("failed to create shader manager handle");
    return (sm_shader_handler_s){SM_INVALID_HANDLE};
  }

  u32 index = sm_handle_index(handle);
  u32 shader_program = sm_gl_shader_new(shader_resource);
  if (!shader_program) {
    SM_LOG_ERROR("failed to create shader");
    return (sm_shader_handler_s){SM_INVALID_HANDLE};
  }

  sm_shader_s shader = {.program = shader_program, .ref_count = 1, .actives = NULL};

  shader.actives = sm_hashmap_new_str( );
  if (!sm_hashmap_ctor_str(shader.actives, 16, NULL, NULL)) {
    SM_LOG_ERROR("Failed to create shader actives map");
    sm_gl_shader_delete(shader.program);
    return (sm_shader_handler_s){SM_INVALID_HANDLE};
  }

  u32 num_uniforms = (u32)sm_gl_shader_uniform_get_active_count(shader_program);
  char name[256];
  i32 length;
  i32 size;
  u32 type;
  i32 location;

  for (u32 i = 0; i < num_uniforms; i++) {
    sm_gl_shader_uniform_get_active(shader_program, i, sizeof(name), &length, &size, &type, name);

    location = sm_gl_shader_uniform_get_location(shader_program, name);

    sm_shader_active_s *active = SM_CALLOC(1, sizeof(sm_shader_active_s));
    active->size = size;
    active->type = type;
    active->location = location;

    sm_hashmap_put_str(shader.actives, SM_STRING_DUP(name), active);
  }

  SHADER_MANAGER->shaders[index] = shader;

  return (sm_shader_handler_s){handle};
}

void sm_shader_manager_bind(sm_shader_handler_s handler)
{

  SM_ASSERT(SHADER_MANAGER);
  SM_ASSERT(sm_handle_valid(SHADER_MANAGER->pool, handler.handle));

  u32 index = sm_handle_index(handler.handle);
  SM_ASSERT(index < SM_ARRAY_LEN(SHADER_MANAGER->shaders));

  sm_shader_s shader = SHADER_MANAGER->shaders[index];

  sm_gl_shader_bind(shader.program);
}

void sm_shader_manager_unbind( )
{

  sm_gl_shader_unbind( );
}

sm_shader_handler_s sm_shader_manager_reference(sm_shader_handler_s handler)
{

  SM_ASSERT(SHADER_MANAGER);
  SM_ASSERT(sm_handle_valid(SHADER_MANAGER->pool, handler.handle));

  u32 index = sm_handle_index(handler.handle);
  SM_ASSERT(index < SM_ARRAY_LEN(SHADER_MANAGER->shaders));

  sm_shader_s shader = SHADER_MANAGER->shaders[index];

  atomic_fetch_add(&shader.ref_count, 1);

  return handler;
}

b8 sm_shader_manager_dtor_cb(const SM_STRING key, void *value, void *user_data)
{
  SM_UNUSED(user_data);
  sm_shader_active_s *active = (sm_shader_active_s *)value;

  SM_STRING_FREE((SM_STRING)key);
  SM_FREE(active);

  return true;
}

void sm_shader_manager_dtor(sm_shader_handler_s handler)
{

  SM_ASSERT(SHADER_MANAGER);
  SM_ASSERT(sm_handle_valid(SHADER_MANAGER->pool, handler.handle));

  u32 index = sm_handle_index(handler.handle);
  SM_ASSERT(index < SM_ARRAY_LEN(SHADER_MANAGER->shaders));

  sm_shader_s shader = SHADER_MANAGER->shaders[index];

  atomic_fetch_sub(&shader.ref_count, 1);
  if (atomic_load(&shader.ref_count) > 0) return;

  sm_handle_del(SHADER_MANAGER->pool, handler.handle);

  sm_gl_shader_delete(shader.program);

  sm_hashmap_for_each_str(shader.actives, sm_shader_manager_dtor_cb, NULL);
  sm_hashmap_dtor_str(shader.actives);

  SHADER_MANAGER->shaders[index] = (sm_shader_s){0};
}

void sm_shader_manager_set_uniform(sm_shader_handler_s handler, SM_STRING name, sm_type_e type, void *value)
{

  SM_ASSERT(SHADER_MANAGER);
  SM_ASSERT(sm_handle_valid(SHADER_MANAGER->pool, handler.handle));

  u32 index = sm_handle_index(handler.handle);
  SM_ASSERT(index < SM_ARRAY_LEN(SHADER_MANAGER->shaders));

  sm_shader_s shader = SHADER_MANAGER->shaders[index];

  sm_shader_active_s *active = sm_hashmap_get_str(shader.actives, name);

  sm_gl_shader_uniform_set(active->location, type, value);
}

void sm_shader_manager_set_uniform_location(i32 location, sm_type_e type, void *value)
{

  SM_ASSERT(SHADER_MANAGER);
  sm_gl_shader_uniform_set(location, type, value);
}

i32 sm_shader_manager_get_attribute_location(sm_shader_handler_s handler, char *attribute)
{

  SM_ASSERT(SHADER_MANAGER);
  SM_ASSERT(sm_handle_valid(SHADER_MANAGER->pool, handler.handle));

  u32 index = sm_handle_index(handler.handle);
  SM_ASSERT(index < SM_ARRAY_LEN(SHADER_MANAGER->shaders));

  sm_shader_s shader = SHADER_MANAGER->shaders[index];

  return sm_gl_shader_attribute_get_location(shader.program, attribute);
}

i32 sm_shader_manager_get_uniform_location(sm_shader_handler_s handler, char *uniform)
{

  SM_ASSERT(SHADER_MANAGER);
  SM_ASSERT(sm_handle_valid(SHADER_MANAGER->pool, handler.handle));

  u32 index = sm_handle_index(handler.handle);
  SM_ASSERT(index < SM_ARRAY_LEN(SHADER_MANAGER->shaders));

  sm_shader_s shader = SHADER_MANAGER->shaders[index];

  return sm_gl_shader_uniform_get_location(shader.program, uniform);
}

void sm_shader_manager_teardown(void)
{

  SM_ASSERT(SHADER_MANAGER && "Shader manager not initialized");

  for (size_t i = 0; i < SM_ARRAY_LEN(SHADER_MANAGER->shaders); ++i) {
    sm_shader_s shader = SHADER_MANAGER->shaders[i];
    if (shader.program > 0) sm_gl_shader_delete(shader.program);
    if (shader.actives) {
      sm_hashmap_for_each_str(shader.actives, sm_shader_manager_dtor_cb, NULL);
      sm_hashmap_dtor_str(shader.actives);
    }
  }
  SM_ARRAY_DTOR(SHADER_MANAGER->shaders);

  sm_handle_pool_dtor(SHADER_MANAGER->pool);

  SM_FREE(SHADER_MANAGER);
  SHADER_MANAGER = NULL;
}
