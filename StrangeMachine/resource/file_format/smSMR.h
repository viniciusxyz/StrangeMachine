#ifndef SM_RESOURCE_FILE_FORMAT_SMR_H
#define SM_RESOURCE_FILE_FORMAT_SMR_H

#include "smpch.h"

#include "core/smCore.h"

#define SMR_RESOURCE_NONE     0x0u
#define SMR_RESOURCE_SCENE    0x1u
#define SMR_RESOURCE_MATERIAL 0x2u

/* magic number 'SMR' */
#define SMR_MAGIC_NUMBER   0xFF524D53
#define SMR_VERSION_NUMBER 0x01ul

typedef struct smr__file_header_s {

  u32 signature;
  u32 version;
  u32 resouce_type;
  u32 uuid;

} smr_file_header_s;

smr_file_header_s smr_file_header_new(u32 resource_type, u32 uuid);
b8 smr_write_header(smr_file_header_s *header, sm_file_handle_s *file);
b8 smr_read_header(smr_file_header_s *header, sm_file_handle_s *file);

#endif /* SM_RESOURCE_FILE_FORMAT_SMR_H */
