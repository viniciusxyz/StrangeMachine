#ifndef SM_SCENE_COMPONENTS_H
#define SM_SCENE_COMPONENTS_H

#include "smpch.h"

#include "core/smCore.h"
#include "math/smMath.h"

#include "renderer/smShaderManager.h"

typedef u64 sm_component_t;

extern const sm_component_t SM_VELOCITY_COMP;
typedef sm_vec3 sm_velocity_s;

extern const sm_component_t SM_FORCE_COMP;
typedef sm_vec3 sm_force_s;

b8 sm_vec3_write(const sm_file_handle_s *handle, const void *ptr);
b8 sm_vec3_read(const sm_file_handle_s *handle, const void *ptr);

extern const sm_component_t SM_SPEED_COMP;

typedef struct {
  f32 speed;
} sm_speed_s;

b8 sm_speed_write(const sm_file_handle_s *handle, const void *ptr);
b8 sm_speed_read(const sm_file_handle_s *handle, const void *ptr);

extern const sm_component_t SM_MESH_COMP;

typedef struct sm__mesh_s {

  /* SM_ARRAY(sm_vertex_s) vertices; */
  SM_ARRAY(sm_vec3) positions;
  SM_ARRAY(sm_vec2) uvs;
  SM_ARRAY(sm_vec4) colors;
  SM_ARRAY(sm_vec3) normals;
  SM_ARRAY(u32) indices;

  u32 vao;     /* OpenGL Vertex Array Object */
  u32 vbos[4]; /* OpenGL Vertex Buffer Objects */
  u32 ebo;     /* OpenGL Vertex Buffer Objects */
  u32 material_uuid;

  enum sm_mesh_component_flags_e {
    SM_MESH_FLAG_NONE = 0,
    SM_MESH_FLAG_RENDERABLE = 1 << 0,
    SM_MESH_FLAG_DIRTY = 1 << 1,
    SM__MESH_ENFORCE_ENUM_SIZE = 0x7fffffff /*  Force 32-bit size enum */
  } flags;

} sm_mesh_s;

b8 sm_mesh_write(const sm_file_handle_s *handle, const void *ptr);
b8 sm_mesh_read(const sm_file_handle_s *handle, const void *ptr);
void sm_mesh_component_copy(sm_mesh_s *dest, const sm_mesh_s *src);
void sm_mesh_dtor(void *ptr);

static inline void sm_mesh_component_set_renderable(sm_mesh_s *mesh, bool renderable)
{

  if (renderable) mesh->flags |= SM_MESH_FLAG_RENDERABLE;
  else mesh->flags &= ~SM_MESH_FLAG_RENDERABLE;
}

static inline b8 sm_mesh_component_is_renderable(const sm_mesh_s *mesh)
{

  return mesh->flags & SM_MESH_FLAG_RENDERABLE;
}

static inline void sm_mesh_component_set_dirty(sm_mesh_s *mesh, bool dirty)
{

  if (dirty) mesh->flags |= SM_MESH_FLAG_DIRTY;
  else mesh->flags &= ~SM_MESH_FLAG_DIRTY;
}

static inline b8 sm_mesh_component_is_dirty(const sm_mesh_s *mesh)
{
  return mesh->flags & SM_MESH_FLAG_DIRTY;
}

/* extern const sm_component_t SM_MATERIAL_COMP; */

/* typedef struct sm__material_s { */
/**/
/*   sm_string name; */
/**/
/*   sm_shader_handler_s shader; */
/*   sm_texture_handler_s diffuse_map; */
/*   sm_vec4 color; */
/**/
/* } sm_material_s; */
/**/
/* b8 sm_material_write(const sm_file_handle_s *handle, const void *ptr); */
/* b8 sm_material_read(const sm_file_handle_s *handle, const void *ptr); */

/* static inline sm_string sm_material_component_get_name(const sm_material_s *material) { */
/**/
/*   return material->name; */
/* } */

extern const sm_component_t SM_CAMERA_COMP;

typedef struct sm__camera_s {

  sm_vec3 position;
  sm_vec3 target, _next_target;
  float target_distance;
  sm_vec3 right;
  sm_vec3 up;

  sm_vec3 angle; // pitch, yaw, roll

  f32 fov;
  f32 aspect_ratio;

  /* Units per second */
  f32 move_speed;
  /* Degres per second */
  f32 sensitive;

  enum sm_camera_component_projection_e {
    SM_CAM_PROJ_PERSPECTIVE,
    SM_CAM_PROJ_ORTHOGONAL,
    SM__CAM_PROJ_ENFORCE_ENUM_SIZE = 0x7fffffff /*  Force 32-bit size enum */
  } projection;

  enum sm_camera_component_mode_e {
    SM_CAM_MODE_FREE,
    SM_CAM_MODE_THIRD_PERSON,
    SM_CAM_MODE_CUSTOM,
    SM__CAM_ENFORCE_ENUM_SIZE = 0x7fffffff /*  Force 32-bit size enum */
  } mode;

} sm_camera_s;

b8 sm_camera_write(const sm_file_handle_s *handle, const void *ptr);
b8 sm_camera_read(const sm_file_handle_s *handle, const void *ptr);

static inline void sm_camera_component_set_aspect_ratio(sm_camera_s *camera, float aspect_ratio)
{

  if (aspect_ratio > 0.0f) camera->aspect_ratio = aspect_ratio;
}

static inline void sm_camera_component_get_projection(const sm_camera_s *camera, mat4 out_projection)
{

  if (camera->projection == SM_CAM_PROJ_PERSPECTIVE)
    glm_perspective(glm_rad(camera->fov), camera->aspect_ratio, 0.01f, 100.0f, out_projection);

  else if (camera->projection == SM_CAM_PROJ_ORTHOGONAL) {

    // https://stackoverflow.com/a/55009832
    float ratio_size_per_depth = atanf(glm_rad(camera->fov / 2.0f)) * 2.0f;
    /* float ratio_size_per_depth = atanf(DEG2RAD * (CAMERA.fovy / 2.0f)) * 2.0f; */
    vec3 dist;
    glm_vec3_sub((float *)camera->target.data, (float *)camera->position.data, dist);
    float distance = glm_vec3_norm(dist);

    /* float distance = vec3_len(vec3_sub(CAMERA.target, CAMERA.position)); */

    float size_y = ratio_size_per_depth * distance;
    float size_x = ratio_size_per_depth * distance * camera->aspect_ratio;

    glm_ortho(-size_x, size_x, -size_y, size_y, 0.01f, 100.0f, out_projection);
    /* projection = mat4_ortho(-size_x, size_x, -size_y, size_y, -50.0f, 100.00f); */
  } else {
    SM_UNREACHABLE( );
  }
}

static inline void sm_camera_component_get_view(const sm_camera_s *camera, mat4 out_view)
{

  if (camera->mode == SM_CAM_MODE_THIRD_PERSON)

    glm_lookat((float *)camera->position.data, (float *)camera->target.data, (float *)camera->up.data, out_view);

  else if (camera->mode == SM_CAM_MODE_FREE) {

    vec3 dist;
    glm_vec3_sub((float *)camera->position.data, (float *)camera->target.data, dist);
    glm_lookat((float *)camera->position.data, dist, (float *)camera->up.data, out_view);
  }
}

extern const sm_component_t SM_ALL_COMP;

typedef void (*sm_component_dtor_f)(void *ptr);
typedef b8 (*sm_component_write_f)(const sm_file_handle_s *handle, const void *ptr);
typedef b8 (*sm_component_read_f)(const sm_file_handle_s *handle, const void *ptr);

typedef struct sm__component_view_s {

  sm_component_t id;
  SM_STRING name;

  /* size in bytes of the component */
  size_t size;
  size_t offset;

  b8 alligned;
  sm_component_write_f write;
  sm_component_read_f read;
  sm_component_dtor_f dtor;

} sm_component_view_s;

void component_init(void);
void component_teardown(void);

void component_register_component(sm_component_view_s *desc);
const sm_component_view_s *component_get_desc(sm_component_t id);

#endif /* SM_SCENE_COMPONENTS_H */
