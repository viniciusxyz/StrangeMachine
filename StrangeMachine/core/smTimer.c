#include "smpch.h"

#include "core/smAssert.h"
#include "core/smMem.h"

#include <SDL2/SDL.h>

typedef struct sm__timer_s {

  u64 start;

} sm_timer_s;

sm_timer_s *sm_timer_new(void)
{

  sm_timer_s *t = SM_MALLOC(sizeof(sm_timer_s));

  return t;
}

void sm_timer_ctor(sm_timer_s *t)
{

  SM_ASSERT(t);

  t->start = SDL_GetPerformanceCounter( );
}

void sm_timer_dtor(sm_timer_s *t)
{

  SM_ASSERT(t);

  SM_FREE(t);
}

void sm_timer_reset(sm_timer_s *t)
{

  SM_ASSERT(t);

  t->start = SDL_GetPerformanceCounter( );
}

double sm_timer_get_elapsed(sm_timer_s *t)
{
  SM_ASSERT(t);

  u64 end = SDL_GetPerformanceCounter( );

  return (double)(end - t->start) / (double)(SDL_GetPerformanceFrequency( ));
}
